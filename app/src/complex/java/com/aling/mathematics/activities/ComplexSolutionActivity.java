package com.aling.mathematics.activities;

import android.os.Bundle;
import android.util.Log;

import com.aling.function.beans.FunctionBean;
import com.aling.function.service.CalculateService;
import com.aling.mathematics.R;
import com.aling.mathematics.analytics.Analytics;
import com.aling.mathematics.dialogs.ProgressDialog;
import com.aling.mathematics.widgets.ComplexGraphView;

import java.util.ArrayList;

import static com.aling.mathematics.MathematicsActivity.KEY_FUNCTION;

/**
 * Created by mcekic on 12/6/17.
 */

public class ComplexSolutionActivity extends BaseAppCompatActivity {
    public static final String TAG = "SolutionActivity";

    private ArrayList<String> mFunctionEntered;
    private FunctionBean mFunctionBean;
    private ComplexGraphView mGraphView;

    private long mTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complex_solution);

        mGraphView = (ComplexGraphView) findViewById(R.id.complex_graph_view);

        if (savedInstanceState == null) {
            // get data from intent
            mFunctionEntered = getIntent().getStringArrayListExtra(KEY_FUNCTION);
            if (mFunctionEntered == null) return;

            // show progress dialog
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.calculating));
            progressDialog.show();

            // calculate function parameters
            new CalculateService().execute(getBaseContext(), mFunctionEntered, new CalculateService.OnCalculateCompleteListener() {
                @Override
                public void function(FunctionBean function) {
                    progressDialog.cancel();

                    mFunctionBean = function;

                    if (mFunctionBean != null && mGraphView != null) {
                        mGraphView.addFunction(mFunctionBean);
                        mGraphView.drawGraphView();
                    }

                    if (mFunctionBean.function != null) {
                        Log.d(TAG, "function: " + mFunctionBean.function.toString());
                    }
                }

                @Override
                public void error(int errorCode) {
                    progressDialog.cancel();
                    Log.d(TAG, "errorCode: " + errorCode);
                }
            });
        } else {
            // restore function parameters
            mFunctionBean = savedInstanceState.getParcelable("FunctionBean");
            if (mFunctionBean != null && mGraphView != null) {
                mGraphView.addFunction(mFunctionBean);
                mGraphView.drawGraphView();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTime = System.currentTimeMillis();
    }

    @Override
    protected void onPause() {
        long time = System.currentTimeMillis() - mTime;

        // logging time past while activity is visible on the screen
        if (mFunctionBean != null && mFunctionBean.function != null) {
            Analytics.logGraphicShowed(mFunctionBean.function.toString(), time);
        } else {
            Analytics.logErrorGraphicNotShowed(mFunctionEntered);
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // saving data
        outState.putParcelable("FunctionBean", mFunctionBean);
        super.onSaveInstanceState(outState);
    }


}
