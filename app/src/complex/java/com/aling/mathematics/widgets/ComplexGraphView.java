package com.aling.mathematics.widgets;

import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.aling.function.beans.FunctionBean;
import com.aling.function.beans.Point;
import com.aling.function.functions.numbers.Complex;
import com.aling.mathematics.R;

import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;

/**
 * Created by Maja Cekic on 2/15/15.
 */
public class ComplexGraphView extends LinearLayout {
    public static final String TAG = "GraphView";

    private static HashMap<Integer, Complex> sHasMapConstants = new HashMap<>();

    static {
        sHasMapConstants.put(1, new Complex(3, 4));
        sHasMapConstants.put(2, new Complex(1, 1));
        sHasMapConstants.put(3, new Complex(1, -1));
        sHasMapConstants.put(4, new Complex(-1, 1));
        sHasMapConstants.put(5, new Complex(-1, -1));
        sHasMapConstants.put(6, new Complex(0, 0));
        sHasMapConstants.put(6, new Complex(0, 1));
    }

    private static Complex getConstant(int key) {
        return sHasMapConstants.get(key);
    }

    private static final int LABELS = 10;
    //top, left, bottom, right
    private static final int[] MARGINS = new int[]{20, 75, 50, 20};

    private ArrayList<FunctionBean> mFunctions = new ArrayList<>();

    private int mTitlesSize;

    public void setChartTitle(int chartTitle) {
        this.mChartTitle = chartTitle;
    }

    private int mChartTitle = R.string.chart_title_solution;

    private int mCurves = 3;

    public ComplexGraphView(Context context) {
        super(context);
    }

    public ComplexGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ComplexGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * @param functionBean FunctionBean
     */
    public void addFunction(FunctionBean functionBean) {
        mFunctions.add(functionBean);
    }

    /**
     *
     */
    public void drawGraphView() {
        GraphPoints graphPoints = new GraphPoints(getContext());
        graphPoints.setMinAndMax(MARGINS);
        Point min = graphPoints.getMin();
        Point max = graphPoints.getMax();

        GraphValues graphValues = new GraphValues(min.x, max.x, min.y, max.y);

        List<double[]> yValues = new ArrayList<>();
        yValues.add(graphValues.getYValues(1));
        // x = 0
        double[] yAxis = new double[]{min.y, max.y};
        yValues.add(yAxis);

        List<double[]> xValues = new ArrayList<>();
        xValues.add(graphValues.getXValues(1));
        // y = 0
        double[] xAxis = new double[]{-0.0001, 0.0001};
        xValues.add(xAxis);

        for (FunctionBean functionBean : mFunctions) {
            int count = 1;
            while (count <= mCurves) {
                xValues.add(graphValues.getXValues(functionBean, getConstant(count)));
                yValues.add(graphValues.getYValues());
                count++;
            }
        }

        setTitleSize();

        int[] colors = setColors();
        XYMultipleSeriesRenderer renderer = setRenderer(colors, min, max);

        String[] titles = setTitles();

        draw(renderer, titles, xValues, yValues);
    }


    /**
     *
     */
    private void setTitleSize() {
        mTitlesSize = 2 + mFunctions.size() * mCurves;
    }

    /**
     * @return titles String[]
     */
    private String[] setTitles() {
        String[] titles = new String[mTitlesSize];
        titles[0] = "";
        titles[1] = "";
        for (int i = 0, j = 2; j < mTitlesSize && i < mFunctions.size(); i++) {
            titles[j] = mFunctions.get(i) != null ? (String) MathTextView.convertText(getContext(),
                    "f(x) = " + mFunctions.get(i).function.toString()) : "";
            j++;
            int count = 1;
            while (count != mCurves) {
                titles[j] = "";
                j++;
                count++;
            }
        }
        return titles;
    }

    private int[] setColors() {
        int[] colors = new int[mTitlesSize];
        colors[0] = ContextCompat.getColor(getContext(), R.color.text_black);
        colors[1] = ContextCompat.getColor(getContext(), R.color.text_black);


        for (int i = 0, j = 2; j < mTitlesSize && i < mFunctions.size(); i++) {
            colors[j] = ContextCompat.getColor(getContext(), R.color.function);
            j++;
            int count = 1;
            while (count != mCurves) {
                colors[j] = ContextCompat.getColor(getContext(), R.color.function);
                j++;
                count++;
            }
        }
        return colors;
    }

    /**
     * @param colors int[]
     * @return renderer XYMultipleSeriesRenderer
     */
    private XYMultipleSeriesRenderer setRenderer(int[] colors, Point min, Point max) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();

        for (int color : colors) {
            XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
            seriesRenderer.setColor(color);
            seriesRenderer.setPointStyle(PointStyle.CIRCLE);
            seriesRenderer.setFillPoints(true);
            renderer.addSeriesRenderer(seriesRenderer);
        }

        // graphic setup
        renderer.setChartTitle(getResources().getString(mChartTitle));
        renderer.setXTitle("");
        renderer.setYTitle("");
        renderer.setXLabels(LABELS);
        renderer.setYLabels(LABELS);
        renderer.setShowGrid(true);
        renderer.setXLabelsAlign(Paint.Align.CENTER);
        renderer.setYLabelsAlign(Paint.Align.RIGHT);
        renderer.setZoomButtonsVisible(true);
        renderer.setApplyBackgroundColor(true);
        renderer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.background));
        renderer.setMarginsColor(ContextCompat.getColor(getContext(), R.color.background));
        renderer.setXLabelsColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setYLabelsColor(0, ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setInScroll(true);
        renderer.setAntialiasing(true);
        renderer.setAxesColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setLabelsColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setAxisTitleTextSize(32);
        renderer.setChartTitleTextSize(40);
        renderer.setLabelsTextSize(30);
        renderer.setLegendTextSize(34);
        renderer.setPointSize(2f);
        renderer.setMargins(MARGINS);
        renderer.setXAxisMin(min.x);
        renderer.setXAxisMax(max.x);
        renderer.setYAxisMin(min.y);
        renderer.setYAxisMax(max.y);
        renderer.setPanLimits(new double[]{min.x, max.x, min.y, max.y});
        renderer.setZoomLimits(new double[]{min.x, max.x, min.y, max.y});
        return renderer;
    }

    /**
     * @param renderer XYMultipleSeriesRenderer
     * @param titles   String[]
     * @param xValues  List<double[]>
     * @param yValues  List<double[]>
     */
    private void draw(XYMultipleSeriesRenderer renderer, String[] titles, List<double[]> xValues, List<double[]> yValues) {
        XYMultipleSeriesDataset dataset = buildDataset(titles, xValues, yValues);
        if (checkParameters(dataset, renderer)) {
            CustomGraphicalView graphicalView = getLineChartView(getContext(), dataset, renderer);
            addView(graphicalView);
        }
    }

    /**
     * @param dataset  XYMultipleSeriesDataset
     * @param renderer XYMultipleSeriesRenderer
     * @return
     */
    private boolean checkParameters(XYMultipleSeriesDataset dataset, XYMultipleSeriesRenderer renderer) {
        return dataset != null && renderer != null && dataset.getSeriesCount() == renderer.getSeriesRendererCount();
    }

    /**
     * @param context  context
     * @param dataset  XYMultipleSeriesDataset
     * @param renderer XYMultipleSeriesRenderer
     * @return
     */
    private CustomGraphicalView getLineChartView(Context context, XYMultipleSeriesDataset dataset,
                                                 XYMultipleSeriesRenderer renderer) {
        LineChart chart = new LineChart(dataset, renderer);
        return new CustomGraphicalView(context, chart);
    }

    /**
     * @param titles  String[]
     * @param xValues List<double[]>
     * @param yValues List<double[]>
     * @return
     */
    private XYMultipleSeriesDataset buildDataset(String[] titles, List<double[]> xValues, List<double[]> yValues) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        addXYSeries(dataset, titles, xValues, yValues);
        return dataset;
    }

    /**
     * @param dataset XYMultipleSeriesDataset
     * @param titles  String[]
     * @param xValues List<double[]>
     * @param yValues List<double[]>
     */
    private void addXYSeries(XYMultipleSeriesDataset dataset, String[] titles, List<double[]> xValues, List<double[]> yValues) {
        int scale = 0;
        for (int i = 0; i < mTitlesSize; i++) {
            XYSeries series = new XYSeries(titles[i], scale);
            double[] xV = xValues.get(i);
            double[] yV = yValues.get(i);
            int seriesLength = xV.length;
            for (int k = 0; k < seriesLength; k++) {
                // if (yValues[k] != MathHelper.NULL_VALUE) {
                series.add(xV[k], yV[k]);
                // }
            }
            dataset.addSeries(series);
        }
    }

    private class GraphPoints {
        public static final String TAG = "GraphPoints";

        private static final int GRAPH_VALUE_RANGE = 5;

        private Point min = new Point(-GRAPH_VALUE_RANGE, -GRAPH_VALUE_RANGE);
        private Point max = new Point(GRAPH_VALUE_RANGE, GRAPH_VALUE_RANGE);

        private int measuredWidth;
        private int measuredHeight;

        public GraphPoints(Context context) {
            Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            android.graphics.Point size = new android.graphics.Point();

            display.getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y;
        }

        private void setMinAndMax(int[] margins) {

            // calculate screen size to set 1:1
            double graphWidth = measuredWidth * 4 / 5 - margins[1] - margins[3];
            double graphHeight = measuredHeight - margins[0] - margins[2];

            double offsetX = (max.x - min.x) / 2;
            double offsetY = (max.y - min.y) / 2;

            if (graphHeight > graphWidth) {
                offsetX *= (graphWidth / graphHeight - 1);
                offsetY *= (graphHeight / graphWidth - 1);
            } else {
                offsetX *= (graphWidth / graphHeight + 1);
                offsetY *= (graphHeight / graphWidth + 1);
            }
            if (offsetX > 0) {
                max.x += offsetX;
                min.x -= offsetX;
            } else if (offsetY > 0) {
                max.y += offsetY;
                min.y -= offsetY;
            }
        }

        public Point getMin() {
            return min;
        }

        public Point getMax() {
            return max;
        }


    }

    private static class GraphValues {
        public static final String TAG = "GraphValues";

        public static final int POINTS_PER_ONE = 100;

        public static final double ONE = 1.0;

        private double xMin, xMax, yMin, yMax;
        private int xPointsSize;

        public GraphValues(double xMin, double xMax, double yMin, double yMax) {
            this.xMin = xMin;
            this.xMax = xMax;
            this.yMin = yMin;
            this.yMax = yMax;
            this.xPointsSize = (int) (Math.abs(xMax - xMin) + 1);
        }

        /**
         * @return xValues from minX to maxX
         */
        public double[] getXValues() {
            return getXValues(POINTS_PER_ONE);
        }

        /**
         * @return xValues from minX to maxX
         */
        public double[] getXValues(int scale) {
            int size = xPointsSize * scale;
            double xMin = this.xMin;
            double[] xValues = new double[size];
            for (int i = 0; i < size; i++) {
                xValues[i] = xMin;
                xMin += ONE / (double) scale;
            }
            return xValues;
        }

        /**
         * @return yValues from minX to maxX for y = 0
         */
        public double[] getYValues(int scale) {
            int size = xPointsSize * scale;
            double[] yValues = new double[size];
            for (int i = 0; i < size; i++) {
                yValues[i] = 0;
            }
            return yValues;
        }

        int size = 100;
        double[] xValues, yValues;

        /**
         * @param functionBean FunctionBean
         * @return xValues
         */
        private double[] calculate(final FunctionBean functionBean, Complex startPoint) {
           /* int size = xPointsSize * POINTS_PER_ONE;*/
            double xMin = startPoint.x;
            double yMin = startPoint.y;
            for (int i = 0; i < size; i++) {
                Complex value = functionBean.getValue(new Complex(xMin, yMin));
                if (value.x != NaN && value.x != POSITIVE_INFINITY && value.x != NEGATIVE_INFINITY
                        && value.y != NaN && value.y != POSITIVE_INFINITY && value.y != NEGATIVE_INFINITY) {
                    xValues[i] = xMin = value.x;
                    yValues[i] = yMin = value.y;
                } else {
                    xValues[i] = xMin = 0;
                    yValues[i] = yMin = 0;
                }

                /*xMin += ONE / (double) POINTS_PER_ONE;*/
            }
            return xValues;
        }

        /**
         * @param functionBean FunctionBean
         * @return xValues
         */
        public double[] getXValues(final FunctionBean functionBean, Complex startPoint) {
            xValues = new double[size];
            yValues = new double[size];
            return calculate(functionBean, startPoint);
        }


        /**
         * @return yValues
         */
        public double[] getYValues() {
            return yValues;
        }
    }
}
