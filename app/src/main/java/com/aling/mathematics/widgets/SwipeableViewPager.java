package com.aling.mathematics.widgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by mcekic on 4/27/17.
 */
public class SwipeableViewPager extends ViewPager {
    public static final String TAG = "SwipeableViewPager";

    private boolean mEnabled;

    private boolean mAlwaysEnable, mAlwaysDisable;


    public SwipeableViewPager(Context context) {
        super(context);
        init();
    }

    public SwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        mAlwaysEnable = sharedPreferences.getBoolean("pref_swiping_enabled", false);
        mAlwaysDisable = sharedPreferences.getBoolean("pref_swiping_disabled", false);
        mEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mAlwaysEnable) {
            return super.onTouchEvent(event);
        } else if (mAlwaysDisable) {
            return false;
        } else if (mEnabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (mAlwaysEnable) {
            return super.onInterceptTouchEvent(event);
        } else if (mAlwaysDisable) {
            return false;
        } else if (mEnabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    public boolean isPagingEnabled() {
        return mEnabled;
    }
}
