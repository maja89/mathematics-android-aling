
package com.aling.mathematics.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.AttributeSet;

import com.aling.font.FontTypefaceSpan;
import com.aling.font.widgets.FTextView;
import com.aling.mathematics.R;
import com.aling.mathematics.mathutiles.Constants;

import java.util.regex.Pattern;

/**
 * Created by Maja Cekic on 6/13/15.
 */
public class MathTextView extends FTextView {
    public static final String TAG = "MathTextView";

    private static final Pattern PATTERN_ABS = Pattern.compile(
            "[\\abs]{3,3}" + "[\\(]{1,1}" + "(.*)" + "[\\)]{1,1}"
    );
    private static final Pattern PATTERN_ABS_REPLACE = Pattern.compile(
            "[\\|]{1,1}" + "(.*)" + "[\\|]{1,1}"
    );

    private CharSequence mText;

    public MathTextView(Context context) {
        super(context);
    }

    public MathTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MathTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setTextM(CharSequence text) {
        mText = text;
        setText();
    }

    public void appendM(SpannableString text) {
        mText = mText.toString() + text.toString();
        String str = text.toString();
        super.append(convertText(getContext(), str));
    }

    public void appendM(CharSequence text, Typeface typeface) {
        mText = mText.toString() + text.toString();
        CharSequence textToShow = convertText(getContext(), text);
        SpannableString spannable = new SpannableString(textToShow);
        spannable.setSpan(new FontTypefaceSpan("", typeface), 0, textToShow.length(),
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        super.append(spannable);
    }

    public void appendM(CharSequence text) {
        mText = mText.toString() + text.toString();
        super.append(convertText(getContext(), text));
    }

    private void setText() {
        super.setText(convertText(getContext(), mText));
    }

    @Override
    public CharSequence getText() {
        return mText;
    }

    public static CharSequence convertText(Context context, CharSequence textToCovert) {
        String text = (String) textToCovert;
        // replace neg(x) with -(x)
        text = text.replaceAll(Pattern.quote(Constants.FUNCTION_NEG + Constants.BRACKET_OPEN),
                "-" + Constants.BRACKET_OPEN);

       /* // replace abs(x) with |x)
        text = text.replaceAll(Pattern.quote(Constants.FUNCTION_ABS + Constants.BRACKET_OPEN), "|");
        // replace |x) with |x|
        for (int i = 0; i < text.length(); i++) {
            // finds first index of |
            int first = text.indexOf('|');
            // none found
            if (first == -1) break;
            int second = -1;
            // finds first index of ) after |
            second = text.indexOf(')', first);
            if (second == -1) break;
            Log.d(TAG, "first: " + first);
            Log.d(TAG, "second: " + second);

            // replace ) with |
            text = text.replace(')', '|');
            i = second + 1;
        }*/

        // replace sqrt(x)
        text = text.replaceAll(Pattern.quote(Constants.FUNCTION_SQRT + Constants.BRACKET_OPEN),
                context.getString(R.string.uni_sqrt) + Constants.BRACKET_OPEN);

        return text;
    }
}
