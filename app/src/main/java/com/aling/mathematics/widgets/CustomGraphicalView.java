package com.aling.mathematics.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import androidx.core.content.ContextCompat;

import com.aling.mathematics.R;

import org.achartengine.GraphicalView;
import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.RoundChart;
import org.achartengine.chart.XYChart;
import org.achartengine.renderer.DefaultRenderer;

/**
 * Created by mcekic on 5/4/17.
 */
public class CustomGraphicalView extends GraphicalView {
    public static final String TAG = "CustomGraphicalView";

    private AbstractChart mChart;
    private DefaultRenderer mRenderer;
    private Rect mRect = new Rect();
    private RectF mZoomR = new RectF();
    private Bitmap zoomInImage;
    private Bitmap zoomOutImage;
    private Bitmap fitZoomImage;
    private int zoomSize = 50;
    private Paint mPaint = new Paint();
    private boolean mDrawn;

    public CustomGraphicalView(Context context, AbstractChart chart) {
        super(context, chart);

        this.mChart = chart;

        if (this.mChart instanceof XYChart) {
            this.mRenderer = ((XYChart) this.mChart).getRenderer();
        } else {
            this.mRenderer = ((RoundChart) this.mChart).getRenderer();
        }

        if (this.mRenderer.isZoomButtonsVisible()) {
            this.zoomInImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_zoom_in_white);
            this.zoomOutImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_zoom_out_white);
            this.fitZoomImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_zoom_1_white);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.getClipBounds(this.mRect);
        int top = this.mRect.top;
        int left = this.mRect.left;
        int width = this.mRect.width();
        int height = this.mRect.height();
        if (this.mRenderer.isInScroll()) {
            top = 0;
            left = 0;
            width = this.getMeasuredWidth();
            height = this.getMeasuredHeight();
        }

        this.mChart.draw(canvas, left, top, width, height, this.mPaint);
        if (this.mRenderer != null && this.mRenderer.isZoomEnabled() && this.mRenderer.isZoomButtonsVisible()) {
            this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.primary));
            this.zoomSize = Math.max(this.zoomSize, Math.min(width, height) / 7);

            this.mZoomR.set((float) (left + width - this.zoomSize * 3), (float) (top + height) - (float) this.zoomSize * 0.775F, (float) (left + width), (float) (top + height));
            canvas.drawRoundRect(this.mZoomR, (float) (this.zoomSize / 3), (float) (this.zoomSize / 3), this.mPaint);
            float buttonY = (float) (top + height) - (float) this.zoomSize * 0.625F;
            canvas.drawBitmap(this.zoomInImage, (float) (left + width) - (float) this.zoomSize * 2.75F, buttonY, (Paint) null);
            canvas.drawBitmap(this.zoomOutImage, (float) (left + width) - (float) this.zoomSize * 1.75F, buttonY, (Paint) null);
            canvas.drawBitmap(this.fitZoomImage, (float) (left + width) - (float) this.zoomSize * 0.75F, buttonY, (Paint) null);
        }

        this.mDrawn = true;
    }
}
