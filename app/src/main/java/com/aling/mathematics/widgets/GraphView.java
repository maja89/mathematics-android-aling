package com.aling.mathematics.widgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.aling.function.beans.FunctionBean;
import com.aling.mathematics.R;
import com.aling.function.beans.Point;

import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maja Cekic on 2/15/15.
 */
public class GraphView extends LinearLayout {
    public static final String TAG = "GraphView";

    private static final int LABELS = 10;
    //top, left, bottom, right
    private static final int[] MARGINS = new int[]{20, 75, 50, 20};

    private ArrayList<FunctionBean> mFunctions = new ArrayList<>();

    private int mTitlesSize;

    public void setChartTitle(int chartTitle) {
        this.mChartTitle = chartTitle;
    }

    private int mChartTitle = R.string.chart_title_solution;

    private boolean drawFirstDerivative;
    private boolean drawSecondDerivative;

    public GraphView(Context context) {
        super(context);
        init();
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        // res/xml/settings.xml
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        drawFirstDerivative = sharedPreferences.getBoolean("pref_first_derivative", false);
        drawSecondDerivative = sharedPreferences.getBoolean("pref_second_derivative", false);
    }

    /**
     * @param functionBean FunctionBean
     */
    public void addFunction(FunctionBean functionBean) {
        mFunctions.add(functionBean);
    }

    /**
     * @param functionBeans ArrayList<FunctionBean>
     */
    public void addAllFunctions(ArrayList<FunctionBean> functionBeans) {
        mFunctions.addAll(functionBeans);
    }

    /**
     *
     */
    public void drawGraphView() {
        GraphPoints graphPoints = new GraphPoints(getContext());
        graphPoints.setMinAndMax(MARGINS);
        Point min = graphPoints.getMin();
        Point max = graphPoints.getMax();

        GraphValues graphValues = new GraphValues(min.x, max.x, min.y, max.y);

        List<double[]> yValues = new ArrayList<>();
        yValues.add(graphValues.getYValues(1));
        // x = 0
        double[] yAxis = new double[]{min.y, max.y};
        yValues.add(yAxis);

        List<double[]> xValues = new ArrayList<>();
        xValues.add(graphValues.getXValues(1));
        // y = 0
        double[] xAxis = new double[]{-0.0001, 0.0001};
        xValues.add(xAxis);

        for (FunctionBean functionBean : mFunctions) {
            xValues.add(graphValues.getXValues());
            yValues.add(graphValues.getValues(functionBean));

            if (drawFirstDerivative) {
                xValues.add(graphValues.getXValues());
                yValues.add(graphValues.getValueFirstDerivative(functionBean));
            }
            if (drawSecondDerivative) {
                xValues.add(graphValues.getXValues());
                yValues.add(graphValues.getValueSecondDerivative(functionBean));
            }
        }

        setTitleSize();

        int[] colors = setColors();
        XYMultipleSeriesRenderer renderer = setRenderer(colors, min, max);

        String[] titles = setTitles();

        draw(renderer, titles, xValues, yValues);
    }


    /**
     *
     */
    private void setTitleSize() {
        int coefficient = 1;
        if (drawFirstDerivative) coefficient++;
        if (drawSecondDerivative) coefficient++;
        mTitlesSize = 2 + mFunctions.size() * coefficient;
    }

    /**
     * @return titles String[]
     */
    private String[] setTitles() {
        String[] titles = new String[mTitlesSize];
        titles[0] = "";
        titles[1] = "";
        for (int i = 0, j = 2; j < mTitlesSize && i < mFunctions.size(); i++) {
            titles[j] = mFunctions.get(i) != null ? (String) MathTextView.convertText(getContext(),
                    "f(x) = " + mFunctions.get(i).function.toString()) : "";
            j++;
            if (drawFirstDerivative) {
                titles[j] = mFunctions.get(i) != null ? (String) MathTextView.convertText(getContext(),
                        "f(x)' = " + mFunctions.get(i).firstDerivative.toString()) : "";
                j++;
            }
            if (drawSecondDerivative) {
                titles[j] = mFunctions.get(i) != null ? (String) MathTextView.convertText(getContext(),
                        "f(x)'' = " + mFunctions.get(i).secondDerivative.toString()) : "";
                j++;
            }
        }
        return titles;
    }

    private int[] setColors() {
        int[] colors = new int[mTitlesSize];
        colors[0] = ContextCompat.getColor(getContext(), R.color.text_black);
        colors[1] = ContextCompat.getColor(getContext(), R.color.text_black);


        for (int i = 0, j = 2; j < mTitlesSize && i < mFunctions.size(); i++) {
            colors[j] = ContextCompat.getColor(getContext(), R.color.function);
            j++;
            if (drawFirstDerivative) {
                colors[j] = ContextCompat.getColor(getContext(), R.color.first_derivative);
                j++;
            }
            if (drawSecondDerivative) {
                colors[j] = ContextCompat.getColor(getContext(), R.color.second_derivative);
                j++;
            }
        }
        return colors;
    }

    /**
     * @param colors int[]
     * @return renderer XYMultipleSeriesRenderer
     */
    private XYMultipleSeriesRenderer setRenderer(int[] colors, Point min, Point max) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();

        for (int color : colors) {
            XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
            seriesRenderer.setColor(color);
            seriesRenderer.setPointStyle(PointStyle.CIRCLE);
            seriesRenderer.setFillPoints(true);
            renderer.addSeriesRenderer(seriesRenderer);
        }

        // graphic setup
        renderer.setChartTitle(getResources().getString(mChartTitle));
        renderer.setXTitle("");
        renderer.setYTitle("");
        renderer.setXLabels(LABELS);
        renderer.setYLabels(LABELS);
        renderer.setShowGrid(true);
        renderer.setXLabelsAlign(Paint.Align.CENTER);
        renderer.setYLabelsAlign(Paint.Align.RIGHT);
        renderer.setZoomButtonsVisible(true);
        renderer.setApplyBackgroundColor(true);
        renderer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.background));
        renderer.setMarginsColor(ContextCompat.getColor(getContext(), R.color.background));
        renderer.setXLabelsColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setYLabelsColor(0, ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setInScroll(true);
        renderer.setAntialiasing(true);
        renderer.setAxesColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setLabelsColor(ContextCompat.getColor(getContext(), R.color.text_black));
        renderer.setAxisTitleTextSize(32);
        renderer.setChartTitleTextSize(40);
        renderer.setLabelsTextSize(30);
        renderer.setLegendTextSize(34);
        renderer.setPointSize(2f);
        renderer.setMargins(MARGINS);
        renderer.setXAxisMin(min.x);
        renderer.setXAxisMax(max.x);
        renderer.setYAxisMin(min.y);
        renderer.setYAxisMax(max.y);
        renderer.setPanLimits(new double[]{min.x, max.x, min.y, max.y});
        renderer.setZoomLimits(new double[]{min.x, max.x, min.y, max.y});
        return renderer;
    }

    /**
     * @param renderer XYMultipleSeriesRenderer
     * @param titles   String[]
     * @param xValues  List<double[]>
     * @param yValues  List<double[]>
     */
    private void draw(XYMultipleSeriesRenderer renderer, String[] titles, List<double[]> xValues, List<double[]> yValues) {
        XYMultipleSeriesDataset dataset = buildDataset(titles, xValues, yValues);
        if (checkParameters(dataset, renderer)) {
            CustomGraphicalView graphicalView = getLineChartView(getContext(), dataset, renderer);
            addView(graphicalView);
        }
    }

    /**
     * @param dataset  XYMultipleSeriesDataset
     * @param renderer XYMultipleSeriesRenderer
     * @return
     */
    private boolean checkParameters(XYMultipleSeriesDataset dataset, XYMultipleSeriesRenderer renderer) {
        return dataset != null && renderer != null && dataset.getSeriesCount() == renderer.getSeriesRendererCount();
    }

    /**
     * @param context  context
     * @param dataset  XYMultipleSeriesDataset
     * @param renderer XYMultipleSeriesRenderer
     * @return
     */
    private CustomGraphicalView getLineChartView(Context context, XYMultipleSeriesDataset dataset,
                                                 XYMultipleSeriesRenderer renderer) {
        LineChart chart = new LineChart(dataset, renderer);
        return new CustomGraphicalView(context, chart);
    }

    /**
     * @param titles  String[]
     * @param xValues List<double[]>
     * @param yValues List<double[]>
     * @return
     */
    private XYMultipleSeriesDataset buildDataset(String[] titles, List<double[]> xValues, List<double[]> yValues) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        addXYSeries(dataset, titles, xValues, yValues);
        return dataset;
    }

    /**
     * @param dataset XYMultipleSeriesDataset
     * @param titles  String[]
     * @param xValues List<double[]>
     * @param yValues List<double[]>
     */
    private void addXYSeries(XYMultipleSeriesDataset dataset, String[] titles, List<double[]> xValues, List<double[]> yValues) {
        int scale = 0;
        for (int i = 0; i < mTitlesSize; i++) {
            XYSeries series = new XYSeries(titles[i], scale);
            double[] xV = xValues.get(i);
            double[] yV = yValues.get(i);
            int seriesLength = xV.length;
            for (int k = 0; k < seriesLength; k++) {
                // if (yValues[k] != MathHelper.NULL_VALUE) {
                series.add(xV[k], yV[k]);
                // }
            }
            dataset.addSeries(series);
        }
    }

    private class GraphPoints {
        public static final String TAG = "GraphPoints";

        private static final int GRAPH_VALUE_RANGE = 5;

        private Point min = new Point(-GRAPH_VALUE_RANGE, -GRAPH_VALUE_RANGE);
        private Point max = new Point(GRAPH_VALUE_RANGE, GRAPH_VALUE_RANGE);

        private int measuredWidth;
        private int measuredHeight;

        public GraphPoints(Context context) {
            Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            android.graphics.Point size = new android.graphics.Point();

            display.getSize(size);
            measuredWidth = size.x;
            measuredHeight = size.y;
        }

        private void setMinAndMax(int[] margins) {
            /*ArrayList<Point> points = setPoints();

            ArrayList<Point> minPoints = setMin(points);
            ArrayList<Point> maxPoints = setMax(points);*/

            // calculate screen size to set 1:1
            double graphWidth = measuredWidth * 4 / 5 - margins[1] - margins[3];
            double graphHeight = measuredHeight - margins[0] - margins[2];

            double offsetX = (max.x - min.x) / 2;
            double offsetY = (max.y - min.y) / 2;

            if (graphHeight > graphWidth) {
                offsetX *= (graphWidth / graphHeight - 1);
                offsetY *= (graphHeight / graphWidth - 1);
            } else {
                offsetX *= (graphWidth / graphHeight + 1);
                offsetY *= (graphHeight / graphWidth + 1);
            }
            if (offsetX > 0) {
                max.x += offsetX;
                min.x -= offsetX;
            } else if (offsetY > 0) {
                max.y += offsetY;
                min.y -= offsetY;
            }
        }

        public Point getMin() {
            return min;
        }

        public Point getMax() {
            return max;
        }

        /*private ArrayList<Point> setMin(ArrayList<Point> points) {
            ArrayList<Point> minPoints = new ArrayList<>();
            return minPoints;
        }

        private ArrayList<Point> setMax(ArrayList<Point> points) {
            ArrayList<Point> maxPoints = new ArrayList<>();
            return maxPoints;
        }

        private ArrayList<Point> setPoints() {
            ArrayList<Point> points = new ArrayList<>();
            for (FunctionBean functionBean : mFunctions) {
                findMinAndMax(functionBean, points);
            }
            return points;
        }

        private void findMinAndMax(FunctionBean functionBean, ArrayList<Point> points) {

            for (Point breakpoint : functionBean.breakpoints) {

            }

            for (Point zero : functionBean.zeroes) {

            }

            for (Point pointMax : functionBean.pointMax) {

            }

            for (Point pointMin : functionBean.pointMin) {

            }

            for (Point saddlePoint : functionBean.saddlePoints) {

            }
        }*/

    }

    private static class GraphValues {
        public static final String TAG = "GraphValues";

        /* public static final double NULL_VALUE = 1.7976931348623157E308D;*/
        public static final int POINTS_PER_ONE = 100;

        public static final double ONE = 1.0;

        private double xMin, xMax, yMin, yMax;
        private int xPointsSize;

        public GraphValues(double xMin, double xMax, double yMin, double yMax) {
            this.xMin = xMin;
            this.xMax = xMax;
            this.yMin = yMin;
            this.yMax = yMax;
            this.xPointsSize = (int) (Math.abs(xMax - xMin) + 1);
        }

        /**
         * @return xValues from minX to maxX
         */
        public double[] getXValues() {
            return getXValues(POINTS_PER_ONE);
        }

        /**
         * @return xValues from minX to maxX
         */
        public double[] getXValues(int scale) {
            int size = xPointsSize * scale;
            double xMin = this.xMin;
            double[] xValues = new double[size];
            for (int i = 0; i < size; i++) {
                xValues[i] = xMin;
                xMin += ONE / (double) scale;
            }
            return xValues;
        }

        /**
         * @return yValues from minX to maxX for y = 0
         */
        public double[] getYValues(int scale) {
            int size = xPointsSize * scale;
            double[] yValues = new double[size];
            for (int i = 0; i < size; i++) {
                yValues[i] = 0;
            }
            return yValues;
        }

        /**
         * @param functionBean FunctionBean
         * @return yValues from minX to maxX for y
         */
        public double[] getValues(final FunctionBean functionBean) {
            int size = xPointsSize * POINTS_PER_ONE;
            double xMin = this.xMin;
            double[] yValues = new double[size];
            for (int i = 0; i < size; i++) {
                double value = functionBean.getValue(xMin);
               /* if (value > yMax || value < yMin) {
                    yValues[i] = NULL_VALUE;
                } else {*/
                yValues[i] = value;
               /* }*/
                xMin += ONE / (double) POINTS_PER_ONE;
            }
            return yValues;
        }

        /**
         * @param functionBean FunctionBean
         * @return yValues from minX to maxX for y'
         */
        public double[] getValueFirstDerivative(final FunctionBean functionBean) {
            int size = xPointsSize * POINTS_PER_ONE;
            double xMin = this.xMin;
            double[] yValues = new double[size];
            for (int i = 0; i < size; i++) {
                double value = functionBean.getValueFirstDerivative(xMin);
                /*if (value > yMax || value < yMin) {
                    yValues[i] = NULL_VALUE;
                } else {*/
                yValues[i] = value;
                /*}*/
                xMin += ONE / (double) POINTS_PER_ONE;
            }
            return yValues;
        }

        /**
         * @param functionBean FunctionBean
         * @return yValues from minX to maxX for y''
         */
        public double[] getValueSecondDerivative(final FunctionBean functionBean) {
            int size = xPointsSize * POINTS_PER_ONE;
            double xMin = this.xMin;
            double[] yValues = new double[size];
            for (int i = 0; i < size; i++) {
                double value = functionBean.getValueSecondDerivative(xMin);
               /* if (value > yMax || value < yMin) {
                    yValues[i] = NULL_VALUE;
                } else {*/
                yValues[i] = value;
                /*}*/
                xMin += ONE / (double) POINTS_PER_ONE;
            }
            return yValues;
        }

    }
}
