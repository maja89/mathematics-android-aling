package com.aling.mathematics.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.aling.font.Font;
import com.aling.mathematics.R;

/**
 * Created by mcekic on 5/29/17.
 */
public class StyledPreference extends Preference {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StyledPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public StyledPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StyledPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StyledPreference(Context context) {
        super(context);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        int padding = getContext().getResources().getDimensionPixelOffset(R.dimen.preferences_row_padding);
        View view = super.onCreateView(parent);
        view.setPadding(padding, padding / 2, padding, padding / 2);
        return view;
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(Font.getSpannableString(getContext(), (String) title, Font.FONT_REGULAR));
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(Font.getSpannableString(getContext(), getContext().getString(titleId), Font.FONT_REGULAR));
    }


    @Override
    public void setSummary(CharSequence summary) {
        super.setSummary(Font.getSpannableString(getContext(), (String) summary, Font.FONT_REGULAR));
    }

    @Override
    public void setSummary(int summaryId) {
        super.setSummary(Font.getSpannableString(getContext(), getContext().getString(summaryId), Font.FONT_REGULAR));
    }
}
