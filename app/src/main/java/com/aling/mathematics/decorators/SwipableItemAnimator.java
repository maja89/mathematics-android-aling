package com.aling.mathematics.decorators;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.View;

import com.aling.mathematics.R;

/**
 * Created by mcekic on 11/4/16.
 */
public class SwipableItemAnimator extends ItemTouchHelper.SimpleCallback {
    public static final String TAG = "SwipableItemAnimator";

    private Drawable mBackground;
    private Drawable mIconDrawable;
    private int mIconMargin, mSwipeDirs;
    private OnSwipeListener mOnSwipeListener;
    private boolean mEnabled = true;

    public SwipableItemAnimator(Context context, int dragDirs, int swipeDirs, int iconId, int backgroundId) {
        super(dragDirs, swipeDirs);
        mSwipeDirs = swipeDirs;

        mBackground = new ColorDrawable(ContextCompat.getColor(context, backgroundId));
        mIconDrawable = ContextCompat.getDrawable(context, iconId);
        mIconDrawable.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_ATOP);
        mIconMargin = (int) context.getResources().getDimension(R.dimen.delete_icon);
    }

    public void setOnSwipeListener(OnSwipeListener listener) {
        mOnSwipeListener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        if (!mEnabled) return;

        if (mSwipeDirs != direction) return;

        if (viewHolder.getItemViewType() != 0) return;

        int position = viewHolder.getAdapterPosition();

        if (ItemTouchHelper.LEFT == direction) {
            mOnSwipeListener.onSwipeLeft(position);

        } else if (ItemTouchHelper.RIGHT == direction) {
            mOnSwipeListener.onSwipeRight(position);
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View itemView = viewHolder.itemView;

        if (!mEnabled) return;

        if (viewHolder.getItemViewType() != 0) return;

        // not sure why, but this method get's called for viewholder that are already swiped away
        if (viewHolder.getAdapterPosition() == -1) {
            // not interested in those
            return;
        }

        // draw background
        mBackground.setBounds(ItemTouchHelper.LEFT == mSwipeDirs ? itemView.getRight() + (int) dX : itemView.getLeft(),
                itemView.getTop(), ItemTouchHelper.LEFT == mSwipeDirs ? itemView.getRight() : itemView.getLeft() + (int) dX, itemView.getBottom());
        mBackground.draw(c);

        // draw icon
        int itemHeight = itemView.getBottom() - itemView.getTop();
        int intrinsicWidth = mIconDrawable.getIntrinsicWidth();
        int intrinsicHeight = mIconDrawable.getIntrinsicWidth();

        int iconLeft = ItemTouchHelper.LEFT == mSwipeDirs ? itemView.getRight() - mIconMargin - intrinsicWidth : itemView.getLeft() + mIconMargin;
        int iconRight = ItemTouchHelper.LEFT == mSwipeDirs ? itemView.getRight() - mIconMargin : itemView.getLeft() + mIconMargin + intrinsicWidth;
        int iconTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
        int iconBottom = iconTop + intrinsicHeight;
        mIconDrawable.setBounds(iconLeft, iconTop, iconRight, iconBottom);

        mIconDrawable.draw(c);

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    public interface OnSwipeListener {

        void onSwipeLeft(int position);

        void onSwipeRight(int position);
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

}
