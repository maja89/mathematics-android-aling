package com.aling.mathematics.analytics;

import com.aling.mathematics.utils.TimeUtils;
import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mcekic on 4/19/17.
 */
public class Analytics {
    public static final String TAG = "Analytics";

    public static final String EVENT_APPLICATION_STARTED = "APPLICATION_STARTED";

    public static final String EVENT_SEARCH_FUNCTION_SHOWED = "SEARCH_FUNCTION_SHOWED";
    public static final String EVENT_SEARCH_FUNCTION_SELECTED = "SEARCH_FUNCTION_SELECTED";
    public static final String EVENT_SEARCH_FUNCTION_DELETED = "SEARCH_FUNCTION_DELETED";
    public static final String EVENT_FUNCTION_ENTERED = "FUNCTION_ENTERED";
    public static final String EVENT_GRAPHIC_SHOWED = "GRAPHIC_SHOWED";
    public static final String ERROR_GRAPHIC_NOT_SHOWED = "GRAPHIC_NOT_SHOWED";
    public static final String PARAMETER_FUNCTION = "FUNCTION";
    public static final String PARAMETER_TIME = "TIME";

    public static final String EVENT_SETTINGS = "SETTINGS";
    public static final String EVENT_CLEAR_DATA = "CLEAR_DATA";
    public static final String EVENT_RESET_DATA = "RESET_DATA";


    public static void logApplicationStarted() {
        FlurryAgent.logEvent(EVENT_APPLICATION_STARTED);
    }

    public static void logGraphicShowed(String function, long timeInMs) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAMETER_TIME, TimeUtils.getTime(timeInMs));
        params.put(PARAMETER_FUNCTION, function);
        FlurryAgent.logEvent(EVENT_GRAPHIC_SHOWED, params);
    }

    public static void logErrorGraphicNotShowed(ArrayList<String> function) {
        String func = function.toString();
        func = func.replace("[", "");
        func = func.replace(",", "");
        func = func.replace("]", "");

        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAMETER_FUNCTION, func);
        FlurryAgent.logEvent(ERROR_GRAPHIC_NOT_SHOWED, params);
    }

    public static void logFunctionEntered(ArrayList<String> function) {
        String func = function.toString();
        func = func.replace("[", "");
        func = func.replace(",", "");
        func = func.replace("]", "");

        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAMETER_FUNCTION, func);
        FlurryAgent.logEvent(EVENT_FUNCTION_ENTERED, params);
    }

    public static void logSearchFunctionShowed() {
        FlurryAgent.logEvent(EVENT_SEARCH_FUNCTION_SHOWED);
    }

    public static void logSearchFunctionSelected(ArrayList<String> function) {
        String func = function.toString();
        func = func.replace("[", "");
        func = func.replace(",", "");
        func = func.replace("]", "");

        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAMETER_FUNCTION, func);
        FlurryAgent.logEvent(EVENT_SEARCH_FUNCTION_SELECTED, params);
    }

    public static void logSearchFunctionDeleted(ArrayList<String> function) {
        String func = function.toString();
        func = func.replace("[", "");
        func = func.replace(",", "");
        func = func.replace("]", "");

        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAMETER_FUNCTION, func);
        FlurryAgent.logEvent(EVENT_SEARCH_FUNCTION_DELETED, params);
    }

    public static void logSettings() {
        FlurryAgent.logEvent(EVENT_SETTINGS);
    }

    public static void logClearData() {
        FlurryAgent.logEvent(EVENT_CLEAR_DATA);
    }

    public static void logResetData() {
        FlurryAgent.logEvent(EVENT_RESET_DATA);
    }
}
