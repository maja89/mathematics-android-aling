package com.aling.mathematics;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.aling.font.widgets.FTextView;
import com.aling.mathematics.activities.BaseAppCompatActivity;
import com.aling.mathematics.activities.FormulasActivity;
import com.aling.mathematics.activities.SettingsActivity;
import com.aling.mathematics.activities.SolutionActivity;
import com.aling.mathematics.analytics.Analytics;
import com.aling.mathematics.database.Database;
import com.aling.mathematics.activities.FunctionsActivity;
import com.aling.mathematics.mathutiles.Constants;
import com.aling.mathematics.utils.Logger;
import com.aling.mathematics.widgets.MathTextView;

import java.util.ArrayList;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class MathematicsActivity extends BaseAppCompatActivity {
    public static final String TAG = "MathematicsActivity";

    public static final String KEY_FUNCTION = "function";

    public static final int REQUEST_DEFAULT = 1;

    public static final int REQUEST_FUNCTIONS = 2;

    public static final int REQUEST_SETTINGS = 3;

    private ArrayList<String> mFunction = new ArrayList<String>();

    private int mNumberOfOpen = 0;
    private int mNumberOfClosed = 0;

    private String mLast = "";
    private String mLastNumber = "";

    private MathTextView mMathTextView;

    private String mVariable = Constants.X;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setting title with font
        setTitle(getString(R.string.app_name));

        Analytics.logApplicationStarted();

        if (savedInstanceState == null) {
            if (BuildConfig.FLAVOR.equals("complex")) {
                mVariable = Constants.Z;
            }
        } else {
            // restoring data
            mFunction = savedInstanceState.getStringArrayList("mFunction");
            mNumberOfOpen = savedInstanceState.getInt("mNumberOfOpen");
            mNumberOfClosed = savedInstanceState.getInt("mNumberOfClosed");
            mVariable = savedInstanceState.getString("mVariable");
            mLastNumber = savedInstanceState.getString("mLastNumber");
            mLast = savedInstanceState.getString("mLast");
        }

        mMathTextView = (MathTextView) findViewById(R.id.math_function);
        mMathTextView.setTextM(getString(mFunction));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // saving data
        outState.putString("mVariable", mVariable);
        outState.putString("mLastNumber", mLastNumber);
        outState.putString("mLast", mLast);
        outState.putInt("mNumberOfOpen", mNumberOfOpen);
        outState.putInt("mNumberOfClosed", mNumberOfClosed);
        outState.putStringArrayList("mFunction", mFunction);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mathematics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
            startActivityForResult(intent, REQUEST_SETTINGS);
        } else if (id == R.id.action_formulas) {
            Intent intent = new Intent(getBaseContext(), FormulasActivity.class);
            startActivityForResult(intent, REQUEST_DEFAULT);
        } else if (id == R.id.action_search) {
            Intent intent = new Intent(getBaseContext(), FunctionsActivity.class);
            startActivityForResult(intent, REQUEST_FUNCTIONS);
            Analytics.logSearchFunctionShowed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SETTINGS:
                if (resultCode == RESULT_OK) {
//                    mFunctionsFragment.update();
                }
            case REQUEST_FUNCTIONS:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    if (bundle == null) return;
                    setFunction(bundle.getStringArrayList(KEY_FUNCTION));
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void functions(View view) {
        // findViewById(R.id.math_functions).setVisibility(View.VISIBLE);
    }

    public void numbers(View view) {
       // findViewById(R.id.math_functions).setVisibility(View.GONE);
    }

    public void setFunction(View view) {
        String text = (String) ((FTextView) view).getText();
        setFunction((text.substring(0, text.length() - 3)));
    }

    private void constant(String constant) {
        clearDot();

        if (mFunction.size() == 0 || mLast.equals(Constants.BRACKET_OPEN) || isLastOperator()) {
            mFunction.add(constant);
            mLast = constant;
        } else if (isLastANumber() || isMathFunction(mLast) || mLast.equals(Constants.BRACKET_CLOSE)) {
            multiply();
            mFunction.add(constant);
            mLast = constant;
        }

        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, constant);
    }

    private void number(String number) {
        if (Constants.DIGIT_0.equals(mLastNumber) && mFunction.size() > 0) {
            mFunction.remove(mFunction.size() - 1);
            mFunction.add(number);
            mLast = number;
            mLastNumber = number;
        } else if (mFunction.size() == 0 || mLast.equals(Constants.BRACKET_OPEN) || isLastOperator()) {
            mFunction.add(number);
            mLast = number;
            mLastNumber = number;
        } else if (isMathFunction(mLast) || mLast.equals(Constants.BRACKET_CLOSE)) {
            multiply();
            mFunction.add(number);
            mLast = number;
            mLastNumber = number;
        } else if (isLastANumber() || mLast.equals(Constants.POINT)) {
            mLastNumber += number;
            mFunction.remove(mFunction.size() - 1);
            mFunction.add(mLastNumber);
            mLast = mLastNumber;
        }

        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, number);
    }

    private void operator(String operator) {
        clearDot();

        if (mFunction.size() != 0) {
            if (!mLast.equals(Constants.BRACKET_OPEN)) {
                if (isLastOperator()) {
                    mFunction.remove(mFunction.size() - 1);
                    mFunction.add(operator);
                } else {
                    mFunction.add(operator);
                }
                mLast = operator;
            }
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, operator);
    }

    private void function(String function) {
        clearDot();

        if (mFunction.size() == 0 || mLast.equals(Constants.BRACKET_OPEN) || isLastOperator()) {
            mFunction.add(function);
            mFunction.add(Constants.BRACKET_OPEN);
            mFunction.add(mVariable);
            mFunction.add(Constants.BRACKET_CLOSE);
            mLast = Constants.BRACKET_CLOSE;
        } else if (isLastANumber() || isMathFunction(mLast) || mLast.equals(Constants.BRACKET_CLOSE)) {
            multiply();
            mFunction.add(function);
            mFunction.add(Constants.BRACKET_OPEN);
            mFunction.add(mVariable);
            mFunction.add(Constants.BRACKET_CLOSE);
            mLast = Constants.BRACKET_CLOSE;
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, function);
    }

    public void setFunction(String function) {
        Logger.debug(TAG, "Returned MathFunction: " + function);
        function(function);
    }

    public void constantE(View view) {
        constant(Constants.E);
    }

    public void constantPI(View view) {
        constant(Constants.PI);
    }

    public void euler(View view) {
        function(Constants.FUNCTION_EULER);
    }

    public void ln(View view) {
        function(Constants.FUNCTION_LN);
    }

    public void log(View view) {
        function(Constants.FUNCTION_LOG);
    }

    public void sin(View view) {
        function(Constants.FUNCTION_SIN);
    }

    public void cos(View view) {
        function(Constants.FUNCTION_COS);
    }

    public void tan(View view) {
        function(Constants.FUNCTION_TAN);
    }

    public void cot(View view) {
        function(Constants.FUNCTION_COT);
    }

    public void arcsin(View view) {
        function(Constants.FUNCTION_ARC_SIN);
    }

    public void arccos(View view) {
        function(Constants.FUNCTION_ARC_COS);
    }

    public void arctan(View view) {
        function(Constants.FUNCTION_ARC_TAN);
    }

    public void arccot(View view) {
        function(Constants.FUNCTION_ARC_COT);
    }

    public void sinh(View view) {
        function(Constants.FUNCTION_SINH);
    }

    public void cosh(View view) {
        function(Constants.FUNCTION_COSH);
    }

    public void abs(View view) {
        function(Constants.FUNCTION_ABS);
    }

    public void sqrt(View view) {
        function(Constants.FUNCTION_SQRT);
    }

    public void open(View view) {
        if (!(mLast.equals(Constants.BRACKET_CLOSE)) &&
                (mFunction.size() == 0 || mLast.equals(Constants.BRACKET_OPEN) || isLastOperator())) {
            mFunction.add(Constants.BRACKET_OPEN);
            mLast = Constants.BRACKET_OPEN;
            mNumberOfOpen++;
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, Constants.BRACKET_OPEN);
    }

    public void close(View view) {
        if (mNumberOfClosed < mNumberOfOpen &&
                (!(mLast.equals(Constants.BRACKET_OPEN) || isLastOperator()))) {
            mFunction.add(Constants.BRACKET_CLOSE);
            mLast = Constants.BRACKET_CLOSE;
            mNumberOfClosed++;
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, Constants.BRACKET_CLOSE);
    }

    public void delete(View view) {
        if (mFunction.size() > 0) {
            if (mFunction.get(mFunction.size() - 1).equals(Constants.BRACKET_OPEN)) {
                mNumberOfOpen--;
            }
            if (mFunction.get(mFunction.size() - 1).equals(Constants.BRACKET_CLOSE)) {
                mNumberOfClosed--;
            }
            mFunction.remove(mFunction.size() - 1);
        }
        if (mFunction.size() == 0) {
            mLast = "";
        } else {
            mLast = mFunction.get(mFunction.size() - 1);
            if (mFunction.size() != 0 && isMathFunction(mLast)) {
                mFunction.remove(mFunction.size() - 1);
                if (mFunction.size() == 0) {
                    mLast = "";
                } else {
                    mLast = mFunction.get(mFunction.size() - 1);
                }
            }
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, getString(R.string.button_delete));
    }

    public void clear(View view) {
        mFunction.clear();
        mNumberOfOpen = 0;
        mNumberOfClosed = 0;
        mLast = "";
        mLastNumber = "";
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, getString(R.string.button_clear));
    }

    public void variable(View view) {
        constant(mVariable);
    }

    public void dot(View view) {
        if (mFunction.size() == 0 || mLast.equals(Constants.BRACKET_OPEN) || isLastOperator()) {
            mLastNumber = Constants.DIGIT_0 + Constants.POINT;
            mFunction.add(mLastNumber);
            mLast = mLastNumber;
        } else if (isLastANumber() && !mLastNumber.contains(Constants.POINT)) {
            mLastNumber += Constants.POINT;
            mFunction.remove(mFunction.size() - 1);
            mFunction.add(mLastNumber);
            mLast = mLastNumber;
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, Constants.POINT);
    }

    private void clearDot() {
        if (mLastNumber.length() > 0) {
            char c = mLastNumber.charAt(mLastNumber.length() - 1);
            if (c == Constants.CHAR_DOT) {
                mLastNumber = mLastNumber.substring(0, mLastNumber.lastIndexOf(Constants.CHAR_DOT));
                mFunction.remove(mFunction.size() - 1);
                mFunction.add(mLastNumber);
                mLast = mLastNumber;
            }
            mLastNumber = "";
        }
    }

    public void zero(View view) {
        number(Constants.DIGIT_0);
    }

    public void numberOne(View view) {
        number(Constants.DIGIT_1);
    }

    public void numberTwo(View view) {
        number(Constants.DIGIT_2);
    }

    public void numberThree(View view) {
        number(Constants.DIGIT_3);
    }

    public void numberFour(View view) {
        number(Constants.DIGIT_4);
    }

    public void numberFive(View view) {
        number(Constants.DIGIT_5);
    }

    public void numberSix(View view) {
        number(Constants.DIGIT_6);
    }

    public void numberSeven(View view) {
        number(Constants.DIGIT_7);
    }

    public void numberEight(View view) {
        number(Constants.DIGIT_8);
    }

    public void numberNine(View view) {
        number(Constants.DIGIT_9);
    }

    public void plus(View view) {
        clearDot();

        if (mFunction.size() != 0) {
            if (!mLast.equals(Constants.BRACKET_OPEN)) {
                if (isLastOperator()) {
                    mFunction.remove(mFunction.size() - 1);
                    mFunction.add(Constants.OPERATOR_PLUS);
                    mMathTextView.setTextM(getString(mFunction));
                } else {
                    mFunction.add(Constants.OPERATOR_PLUS);
                    mMathTextView.setTextM(getString(mFunction));
                }
                mLast = Constants.OPERATOR_PLUS;
            }
        }
        Logger.debug(TAG, Constants.OPERATOR_PLUS);
    }

    public void minus(View view) {
        clearDot();

        if (mFunction.size() == 0) {
            mFunction.add(Constants.FUNCTION_NEG);
            mFunction.add(Constants.BRACKET_OPEN);
            mNumberOfOpen++;
            mLast = Constants.BRACKET_OPEN;
        } else {
            if (!mLast.equals(Constants.BRACKET_OPEN)) {
                if (isLastOperator()) {
                    mFunction.remove(mFunction.size() - 1);
                    mFunction.add(Constants.OPERATOR_MINUS);
                } else {
                    mFunction.add(Constants.OPERATOR_MINUS);
                }
                mLast = Constants.OPERATOR_MINUS;
            } else {
                if (mFunction.size() > 1) {
                    if (!mFunction.get(mFunction.size() - 2).equals(Constants.FUNCTION_NEG)) {
                        mFunction.add(Constants.FUNCTION_NEG);
                        mFunction.add(Constants.BRACKET_OPEN);
                        mNumberOfOpen++;
                        mLast = Constants.BRACKET_OPEN;
                    }
                } else {
                    mFunction.add(Constants.FUNCTION_NEG);
                    mFunction.add(Constants.BRACKET_OPEN);
                    mNumberOfOpen++;
                    mLast = Constants.BRACKET_OPEN;
                }
            }
        }
        mMathTextView.setTextM(getString(mFunction));
        Logger.debug(TAG, Constants.OPERATOR_MINUS);
    }

    public void multiply(View view) {
        multiply();
    }

    private void multiply() {
        operator(Constants.OPERATOR_MULTIPLY);
    }

    public void divide(View view) {
        operator(Constants.OPERATOR_DIVIDE);
    }

    public void pow(View view) {
        operator(Constants.OPERATOR_POW);
    }

    /**
     * @param view View
     */
    public void done(View view) {
        for (int i = 0; i < mFunction.size(); i++) {
            Logger.debug(TAG, "function: " + mFunction.get(i));
        }
        if (mFunction.size() == 0) {
            showToast(getString(R.string.no_functions));
        } else if (mNumberOfOpen != mNumberOfClosed || isLastOperator()) {
            showToast(getString(R.string.incorrect_input));
        } else if (Constants.X.equals(mVariable)) {
            Analytics.logFunctionEntered(mFunction);

            Intent intent = new Intent(getBaseContext(), SolutionActivity.class);
            intent.putExtra(KEY_FUNCTION, mFunction);
            startActivityForResult(intent, REQUEST_DEFAULT);

            Database.addFunctionToDB(getBaseContext(), mFunction.toString());
        } else {
            try {
                Class<?> c = Class.forName("com.aling.mathematics.activities.ComplexSolutionActivity");
                Intent intent = new Intent(getBaseContext(), c);
                intent.putExtra(KEY_FUNCTION, mFunction);
                startActivityForResult(intent, REQUEST_DEFAULT);

                Database.addFunctionToDB(getBaseContext(), mFunction.toString());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param func ArrayList<String>
     * @return
     */
    private String getString(ArrayList<String> func) {
        String s = "";
        s += Constants.X.equals(mVariable) ? Constants.FUNCTION_OF_X : Constants.FUNCTION_OF_Z;
        for (int i = 0; i < func.size(); i++) {
            s += func.get(i);
        }
        return s;
    }

    /**
     * @param func ArrayList<String>
     */
    private void setFunction(ArrayList<String> func) {
        mFunction.clear();
        for (int i = 0; i < func.size(); i++) {
            String s = func.get(i);
            mFunction.add(i, s);
        }
        mMathTextView.setTextM(getString(mFunction));
    }

    /**
     * @return true, if last added is number.
     */
    private boolean isLastANumber() {
        if ("".equals(mLast)) {
            return false;
        }

        char c = mLast.charAt(0);
        return c == Constants.CHAR_0 || c == Constants.CHAR_1 || c == Constants.CHAR_2 ||
                c == Constants.CHAR_3 || c == Constants.CHAR_4 || c == Constants.CHAR_5 ||
                c == Constants.CHAR_6 || c == Constants.CHAR_7 || c == Constants.CHAR_8 ||
                c == Constants.CHAR_9;
    }

    /**
     * @return true, if last added is operator.
     */
    private boolean isLastOperator() {
        return mLast.equals(Constants.OPERATOR_PLUS) ||
                mLast.equals(Constants.OPERATOR_MINUS) ||
                mLast.equals(Constants.OPERATOR_MULTIPLY) ||
                mLast.equals(Constants.OPERATOR_DIVIDE) ||
                mLast.equals(Constants.OPERATOR_POW);
    }

    /**
     * @param function String
     * @return
     */
    private boolean isMathFunction(String function) {
        return Constants.FUNCTION_NEG.equals(function) ||
                Constants.FUNCTION_LN.equals(function) ||
                Constants.FUNCTION_LOG.equals(function) ||
                Constants.FUNCTION_SIN.equals(function) ||
                Constants.FUNCTION_COS.equals(function) ||
                Constants.FUNCTION_TAN.equals(function) ||
                Constants.FUNCTION_COT.equals(function) ||
                Constants.FUNCTION_ARC_SIN.equals(function) ||
                Constants.FUNCTION_ARC_COS.equals(function) ||
                Constants.FUNCTION_ARC_TAN.equals(function) ||
                Constants.FUNCTION_ARC_COT.equals(function) ||
                Constants.FUNCTION_SINH.equals(function) ||
                Constants.FUNCTION_COSH.equals(function) ||
                Constants.FUNCTION_ABS.equals(function) ||
                Constants.FUNCTION_SQRT.equals(function) ||
                Constants.FUNCTION_EULER.equals(function);
    }

    private void showToast(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
    }
}
