package com.aling.mathematics.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aling.mathematics.R;
import com.aling.mathematics.widgets.MathTextView;

/**
 * Created by Maja Cekic on 2/8/15.
 */
public class FormulaListAdapter extends BaseAdapter<String, FormulaListAdapter.ViewHolder> {
    private static String TAG = "FormulaListAdapter";

    public static class ViewHolder extends RecyclerView.ViewHolder {
        MathTextView mFormula;
        int position;

        public ViewHolder(View itemView) {
            super(itemView);
            mFormula = ((MathTextView) itemView.findViewById(R.id.formula));
        }
    }

    public FormulaListAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_formula_item, parent, false);
        if (mOnItemClickListener != null) {
            view.setOnClickListener(mOnItemClickListener);
        }
        if (mOnItemLongClickListener != null) {
            view.setOnLongClickListener(mOnItemLongClickListener);
        }
        if (mOnItemTouchListener != null) {
            view.setOnTouchListener(mOnItemTouchListener);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final String formula = getItem(position);
        holder.mFormula.setText(formula);
        holder.position = position;
    }
}
