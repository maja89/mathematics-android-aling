package com.aling.mathematics.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.aling.function.beans.FunctionArray;
import com.aling.mathematics.R;
import com.aling.mathematics.widgets.MathTextView;

/**
 * Created by Maja Cekic on 2/7/15.
 */
public class FunctionListAdapter extends BaseAdapter<FunctionArray, FunctionListAdapter.ViewHolder> {
    private static String TAG = "FunctionListAdapter";

    public static class ViewHolder extends RecyclerView.ViewHolder {
        MathTextView mFunction;
        ImageButton mGraphImage;
        //TODO: add preview of the function

        public ViewHolder(View itemView) {
            super(itemView);
            mFunction = ((MathTextView) itemView.findViewById(R.id.function));
            mGraphImage = ((ImageButton) itemView.findViewById(R.id.graph));
        }
    }

    public FunctionListAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_function_item, parent, false);
        if (mOnItemClickListener != null) {
            view.setOnClickListener(mOnItemClickListener);
        }
        if (mOnItemLongClickListener != null) {
            view.setOnLongClickListener(mOnItemLongClickListener);
        }
        if (mOnItemTouchListener != null) {
            view.setOnTouchListener(mOnItemTouchListener);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final FunctionArray function = getItem(position);
        holder.mFunction.setTextM(function.toString());
        holder.mFunction.setTextColor(ContextCompat.getColor(mContext, isSelected(position) ? R.color.text_white : R.color.text_black));
        holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, isSelected(position) ? R.color.primary : R.color.list_background));
    }
}
