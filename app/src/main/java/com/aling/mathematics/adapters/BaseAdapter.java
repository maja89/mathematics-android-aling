package com.aling.mathematics.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.View;

import java.util.ArrayList;

import static android.widget.AdapterView.INVALID_POSITION;

/**
 * Created by mcekic on 11/3/16.
 */
public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    public static final String TAG = "BaseAdapter";

    protected Context mContext;
    protected ArrayList<T> mItems;
    protected View.OnClickListener mOnItemClickListener;
    protected View.OnLongClickListener mOnItemLongClickListener;
    protected View.OnTouchListener mOnItemTouchListener;

    /**
     * Array of selected items
     */
    private SparseBooleanArray mSelectedItems;
    private OnItemSelectionListener mOnItemSelectionListener;

    public void setOnItemSelectionListener(OnItemSelectionListener listener) {
        mOnItemSelectionListener = listener;
    }

    public BaseAdapter(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
        mSelectedItems = new SparseBooleanArray();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateItems(ArrayList<T> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public void add(T item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void add(T item, int position) {
        mItems.add(position, item);
    }

    public T getItem(int position) {
        return mItems.get(position);
    }

    public void remove(final int position) {
        final T item = mItems.get(position);

        if (mItems.contains(item)) {
            // remove data and notify
            mItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void setOnItemClickListener(View.OnClickListener clickListener) {
        mOnItemClickListener = clickListener;
    }

    public void setOnItemLongClickListener(View.OnLongClickListener clickListener) {
        mOnItemLongClickListener = clickListener;
    }

    public void setOnItemTouchListener(View.OnTouchListener touchListener) {
        mOnItemTouchListener = touchListener;
    }

    /**
     * Sets item selected/unselected at the position.
     *
     * @param position int
     */
    public void toggleSelection(int position) {
        if (position == INVALID_POSITION) return;
        boolean selected = false;
        if (mSelectedItems.get(position, false)) {
            mSelectedItems.delete(position);
        } else {
            mSelectedItems.put(position, true);
            selected = true;
        }
        if (mOnItemSelectionListener != null) {
            mOnItemSelectionListener.onItemSelectionStateChanged(position, selected);
        }
        notifyItemChanged(position);
    }

    /**
     * Clears array of selected items.
     */
    public void clearSelections() {
        mSelectedItems.clear();
        notifyDataSetChanged();
    }

    /**
     * Returns list of items selected positions.
     *
     * @return items ArrayList<Integer>
     */
    public ArrayList<Integer> getSelectedItems() {
        ArrayList<Integer> items =
                new ArrayList<Integer>(mSelectedItems.size());
        for (int i = 0; i < mSelectedItems.size(); i++) {
            int position = mSelectedItems.keyAt(i);
            items.add(position);
        }
        return items;
    }

    public boolean isSelected(int position) {
        return mSelectedItems.get(position, false);
    }

    public interface OnItemSelectionListener {
        void onItemSelectionStateChanged(int position, boolean selected);
    }
}
