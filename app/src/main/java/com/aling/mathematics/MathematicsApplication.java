package com.aling.mathematics;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aling.font.Font;
import com.flurry.android.FlurryAgent;

/**
 * Created by Maja Cekic on 2/14/15.
 */
public class MathematicsApplication extends Application {
    public static final String TAG = "MathematicsApplication";

    public static final String FONT_NAME = "JosefinSlab";

    public static final String FLURRY_API_KEY = "MFCRJJ5V747FJ6TWHF9C";

    @Override
    public void onCreate() {
        super.onCreate();
        // logs app version name and version code
        saveVersionName();

        // Needed
        Font.setFontName(FONT_NAME);
        // Only if you want to use BaseAppCompatActivity for easy managing toolbars font
        Font.setToolbarFont(Font.FONT_BOLD);

        new FlurryAgent.Builder().withLogEnabled(false).build(this, FLURRY_API_KEY);
    }

    private void saveVersionName() {
        String versionName = "";
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
            Log.i(TAG, "Current version name: " + info.versionName + " version code: " + info.versionCode);

            versionName = info.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not find requested package");
        }

        // saves version name in prefs
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = prefs.edit();
        // res/xml/settings.xml
        editor.putString("pref_app_version", versionName);
        editor.commit();
    }
}
