package com.aling.mathematics.fragments;

import androidx.fragment.app.Fragment;

import com.flurry.android.FlurryAgent;

/**
 * Created by mcekic on 4/19/17.
 */
public class BaseFragment extends Fragment {
    public static final String TAG = "BaseFragment";

    @Override
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(getContext());
    }
}
