package com.aling.mathematics.fragments;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.aling.function.beans.FunctionBean;
import com.aling.font.Font;
import com.aling.mathematics.widgets.GraphView;
import com.aling.mathematics.R;
import com.aling.mathematics.widgets.MathTextView;

import java.util.ArrayList;

/**
 * Created by maja on 9/26/15.
 */
public class SolutionFragment extends BaseFragment {
    public static final String TAG = "SolutionFragment";

    private static Typeface fontBold, fontRegular;

    private FunctionBean mFunctionBean;
    private ArrayList<ArrayList<Text>> functionText;

    public class Text {
        protected String bold;
        protected String regular;

        public Text(String bold, String regular) {
            this.bold = bold;
            this.regular = regular;
        }
    }

    private MathTextView mMathView;
    private int mPosition;

    private GraphView mGraphView;
    private TextView mSwiping, mText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mPosition = bundle.getInt("position");
            }
        } else {
            mPosition = savedInstanceState.getInt("position");
        }

        if (fontBold == null || fontRegular == null) {
            fontBold = Font.getTypeface(getContext(), Font.FONT_BOLD);
            fontRegular = Font.getTypeface(getContext(), Font.FONT_REGULAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        if (mPosition != 0) {
            rootView = inflater.inflate(R.layout.fragment_swipe, null);
            mMathView = (MathTextView) rootView.findViewById(R.id.math_view);
        } else {
            rootView = inflater.inflate(R.layout.fragment_graph, null);
            mGraphView = (GraphView) rootView.findViewById(R.id.graph_view);
            mSwiping = (TextView) rootView.findViewById(R.id.swiping);
            mText = (TextView) rootView.findViewById(R.id.text);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            FunctionBean functionBean = savedInstanceState.getParcelable("FunctionBean");
            if (functionBean != null) {
                setFragment(functionBean);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("position", mPosition);
        outState.putParcelable("FunctionBean", mFunctionBean);
        super.onSaveInstanceState(outState);
    }

    public void setFragment(FunctionBean functionBean) {
        mFunctionBean = functionBean;
        if (mPosition == 0) {
            if (functionBean != null && mGraphView != null) {
                mGraphView.addFunction(functionBean);
                mGraphView.drawGraphView();
                setText(functionBean);
            }
        } else {
            setCustomFunctionText(functionBean);
            if (functionText != null && mMathView != null) {
                setCustomText(functionText.get(mPosition - 1));
            }
        }
    }

    private void setCustomText(ArrayList<Text> customTextBeans) {
        mMathView.setTextM("");
        for (int i = 0; i < customTextBeans.size(); i++) {
            if (customTextBeans.get(i).bold == null) customTextBeans.get(i).bold = "";
            mMathView.appendM(customTextBeans.get(i).bold, fontBold);
            mMathView.appendM("\n\n");
            if (customTextBeans.get(i).regular == null) customTextBeans.get(i).regular = "";
            mMathView.appendM(customTextBeans.get(i).regular, fontRegular);
            mMathView.appendM("\n\n");
        }
    }

    private void setCustomFunctionText(FunctionBean functionBean) {
        functionText = new ArrayList<ArrayList<Text>>();
        ArrayList<Text> text = new ArrayList<Text>();
        text.add(new Text(getString(R.string.title_domain),
                functionBean.textDomainBreakpoints));
        text.add(new Text(getString(R.string.title_even_odd),
                functionBean.textOddEvenPeriod));
        functionText.add(0, text);
        text = new ArrayList<Text>();
        text.add(new Text(getString(R.string.title_zeroes),
                functionBean.textZeroes));
        text.add(new Text(getString(R.string.title_positive_negative),
                functionBean.textPositiveNegative));
        functionText.add(1, text);
        text = new ArrayList<Text>();
        text.add(new Text(getString(R.string.title_asymptote),
                functionBean.textAsymptote));
        text.add(new Text(getString(R.string.title_behavior),
                functionBean.textBehavior));
        functionText.add(2, text);
        text = new ArrayList<Text>();
        text.add(new Text(getString(R.string.title_first_derivative),
                functionBean.textFirstDerivative));
        text.add(new Text(getString(R.string.title_min_max),
                functionBean.textMinMax));
        text.add(new Text(getString(R.string.title_growth_decay),
                functionBean.textGrowthDecay));
        functionText.add(3, text);
        text = new ArrayList<Text>();
        text.add(new Text(getString(R.string.title_second_derivative),
                functionBean.textSecondDerivative));
        text.add(new Text(getString(R.string.title_saddle_points),
                functionBean.textSaddlePoints));
        text.add(new Text(getString(R.string.title_concavity),
                functionBean.textConcavityConvexity));
        functionText.add(4, text);
    }

    public void setSwiping(boolean enabled) {
        if (mSwiping != null) {
            mSwiping.setText(getString(enabled ? R.string.swiping_on : R.string.swiping_off));

            // hide if set default behaviour
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean alwaysEnable = sharedPreferences.getBoolean("pref_swiping_enabled", false);
            boolean alwaysDisable = sharedPreferences.getBoolean("pref_swiping_disabled", false);
            mSwiping.setVisibility(alwaysEnable || alwaysDisable ? View.GONE : View.VISIBLE);
        }
    }

    public void setText(FunctionBean functionBean) {
        if (mText != null && functionBean != null) {

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean drawFirstDerivative = sharedPreferences.getBoolean("pref_first_derivative", false);
            boolean drawSecondDerivative = sharedPreferences.getBoolean("pref_second_derivative", false);

            Spannable spannableF, spannableFFD = null, spannableFSD = null;

            String textF = "f(x) = " + functionBean.function.toString();
            spannableF = new SpannableString(textF);
            int colorF = ContextCompat.getColor(getContext(), R.color.function);
            spannableF.setSpan(new ForegroundColorSpan(colorF), 0, textF.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (drawFirstDerivative) {
                String textFFD = "\n\nf(x)' = " + functionBean.firstDerivative.toString();
                spannableFFD = new SpannableString(textFFD);
                int colorFFD = ContextCompat.getColor(getContext(), R.color.first_derivative);
                spannableFFD.setSpan(new ForegroundColorSpan(colorFFD), 0, textFFD.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            if (drawSecondDerivative) {
                String textFSD = "\n\nf(x)'' = " + functionBean.secondDerivative.toString();
                spannableFSD = new SpannableString(textFSD);
                int colorFSD = ContextCompat.getColor(getContext(), R.color.second_derivative);
                spannableFSD.setSpan(new ForegroundColorSpan(colorFSD), 0, textFSD.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            mText.setText(spannableF);
            if (drawFirstDerivative)
                mText.append(spannableFFD);
            if (drawSecondDerivative)
                mText.append(spannableFSD);
        }
    }

}
