package com.aling.mathematics.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aling.mathematics.R;
import com.aling.mathematics.activities.ProofActivity;
import com.aling.mathematics.adapters.FormulaListAdapter;
import com.aling.mathematics.mathutiles.FormulasData;

import java.util.Map;

/**
 * Created by mcekic on 5/31/17.
 */
public class FormulasFragment extends BaseFragment {

    public static final int POSITION_LOGARITHM = 0;

    public static final int POSITION_TRIGONOMETRY = 1;

    public static final int POSITION_LIMITS = 2;

    public static final int POSITION_DERIVATIVES = 3;

    public static final int POSITION_INTEGRALS = 4;

    private FormulaListAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private int mPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mPosition = bundle.getInt("position");
            }
        } else {
            mPosition = savedInstanceState.getInt("position");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler_view, null);
        setRecyclerView(rootView);
        return rootView;
    }

    private void setRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new FormulaListAdapter(getContext());
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = mRecyclerView.getChildAdapterPosition(view);
                Intent intent = new Intent(getContext(), ProofActivity.class);
                intent.putExtra(ProofActivity.KEY_FRAGMENT_POSITION, mPosition);
                intent.putExtra(ProofActivity.KEY_LIST_ITEM_POSITION, position);
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    private void setData() {
        Map<String, String> data = getData();
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                mAdapter.add(data.get(Integer.toString(i)));
            }
        }
    }

    private Map<String, String> getData() {
        switch (mPosition) {
            case POSITION_LOGARITHM:
                return FormulasData.logarithm;
            case POSITION_TRIGONOMETRY:
                return FormulasData.trigonometry;
            case POSITION_LIMITS:
                return FormulasData.limits;
            case POSITION_DERIVATIVES:
                return FormulasData.derivatives;
            case POSITION_INTEGRALS:
                return FormulasData.integrals;
        }
        return null;
    }
}
