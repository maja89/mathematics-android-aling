package com.aling.mathematics.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.aling.mathematics.R;
import com.aling.mathematics.analytics.Analytics;
import com.aling.mathematics.database.Database;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {
    public static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        addPreferencesFromResource(R.xml.settings);

        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setting title with font
        setTitle(getString(R.string.activity_settings));

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        setVersionName();

        setReset();

        Analytics.logSettings();
    }

    private void setVersionName() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String versionName = sharedPreferences.getString("pref_app_version", "");
        Preference prefVersion = findPreference("pref_app_version");
        prefVersion.setSummary(versionName);
    }

    private void setReset() {
        Preference prefReset = findPreference("pref_reset");
        prefReset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Database.resetDB(getBaseContext());
                Analytics.logResetData();

                setResult(RESULT_OK);
                onBackPressed();
                return true;
            }
        });
    }

    public void clear(View view) {
        Database.clearDB(getBaseContext());
        Analytics.logClearData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

}
