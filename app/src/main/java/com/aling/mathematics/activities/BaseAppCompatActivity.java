package com.aling.mathematics.activities;

import androidx.appcompat.app.AppCompatActivity;

import com.aling.font.Font;
import com.flurry.android.FlurryAgent;

/**
 * Created by mcekic on 11/4/16.
 */
public class BaseAppCompatActivity extends AppCompatActivity {
    public static final String TAG = "BaseAppCompatActivity";

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(getBaseContext());
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(getBaseContext());
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(Font.getToolbarSpannableString(getBaseContext(), (String) title));
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(Font.getToolbarSpannableString(getBaseContext(), getString(titleId)));
    }
}