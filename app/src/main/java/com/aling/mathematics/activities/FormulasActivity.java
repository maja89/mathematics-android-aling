package com.aling.mathematics.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.aling.mathematics.R;
import com.aling.mathematics.animations.DepthPageTransformer;
import com.aling.mathematics.fragments.FormulasFragment;

/**
 * Created by mcekic on 5/31/17.
 */
public class FormulasActivity extends BaseAppCompatActivity {

    private ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulas);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        // init view pager and adapter
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new DepthPageTransformer());
        mPager.setAdapter(new SwipeAdapter(getSupportFragmentManager()));
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setFragmentTitle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        int position = getLastPosition();
        if (savedInstanceState != null) {
            position = savedInstanceState.getInt("position");
        }
        mPager.setCurrentItem(position);
        setFragmentTitle(position);
    }

    private int getLastPosition() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return sharedPreferences.getInt("pref_formulas_fragment_position", 0);
    }

    private void saveLastPosition() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = prefs.edit();
        // res/xml/settings.xml
        editor.putInt("pref_formulas_fragment_position", mPager.getCurrentItem());
        editor.apply();
    }

    private void setFragmentTitle(int position) {
        switch (position) {
            case FormulasFragment.POSITION_LOGARITHM:
                setTitle(R.string.logarithm);
                break;
            case FormulasFragment.POSITION_TRIGONOMETRY:
                setTitle(R.string.trigonometry);
                break;
            case FormulasFragment.POSITION_LIMITS:
                setTitle(R.string.limits);
                break;
            case FormulasFragment.POSITION_DERIVATIVES:
                setTitle(R.string.derivatives);
                break;
            case FormulasFragment.POSITION_INTEGRALS:
                setTitle(R.string.integrals);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("position", mPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                saveLastPosition();
                finish();
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        saveLastPosition();
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    private class SwipeAdapter extends FragmentPagerAdapter {
        private static final int NUMBER_OF_FRAGMENTS = 5;

        public SwipeAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            FormulasFragment fragment = new FormulasFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUMBER_OF_FRAGMENTS;
        }
    }
}
