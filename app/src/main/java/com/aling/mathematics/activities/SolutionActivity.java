package com.aling.mathematics.activities;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import com.aling.function.beans.FunctionBean;
import com.aling.function.service.CalculateService;
import com.aling.mathematics.R;
import com.aling.mathematics.analytics.Analytics;
import com.aling.mathematics.animations.DepthPageTransformer;
import com.aling.mathematics.dialogs.ProgressDialog;
import com.aling.mathematics.fragments.SolutionFragment;
import com.aling.mathematics.widgets.SwipeableViewPager;

import java.util.ArrayList;

import static com.aling.mathematics.MathematicsActivity.KEY_FUNCTION;

/**
 * Created by Maja Cekic on 9/26/15.
 */
public class SolutionActivity extends BaseAppCompatActivity {
    public static final String TAG = "SolutionActivity";

    private ArrayList<String> mFunctionEntered;

    private SwipeableViewPager mPager;
    private PagerAdapter mSwipeAdapter;
    private FunctionBean mFunctionBean;

    private long mTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);

        // init view pager and adapter
        mPager = (SwipeableViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new DepthPageTransformer());
        mSwipeAdapter = new SwipeAdapter(getSupportFragmentManager());
        mPager.setAdapter(mSwipeAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setFragment(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (savedInstanceState == null) {
            // get data from intent
            mFunctionEntered = getIntent().getStringArrayListExtra(KEY_FUNCTION);
            if (mFunctionEntered == null) return;

            // show progress dialog
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.calculating));
            progressDialog.show();

            // calculate function parameters
            new CalculateService().execute(getBaseContext(), mFunctionEntered, new CalculateService.OnCalculateCompleteListener() {
                @Override
                public void function(FunctionBean function) {
                    progressDialog.cancel();

                    mFunctionBean = function;
                    int position = mPager.getCurrentItem();
                    setFragment(position);

                    if (mFunctionBean.function != null) {
                        Log.d(TAG, "function: " + mFunctionBean.function.toString());
                    }
                }

                @Override
                public void error(int errorCode) {
                    progressDialog.cancel();
                    Log.d(TAG, "errorCode: " + errorCode);
                }
            });
        } else {
            // restore function parameters and set first fragment
            mFunctionBean = savedInstanceState.getParcelable("FunctionBean");
            int position = savedInstanceState.getInt("position");
            mPager.setCurrentItem(position);
            setFragment(position);
        }
    }

    /**
     * Use this method to set fragment
     *
     * @param position int, position of the fragment to be set
     */
    private void setFragment(int position) {
        SolutionFragment fragment = (SolutionFragment) findFragmentByPosition(position);
        if (fragment != null) {
            fragment.setFragment(mFunctionBean);
            fragment.setSwiping(mPager.isPagingEnabled());
        }
    }

    /**
     * Use this method to get fragment
     *
     * @param position int, position of the fragment
     * @return fragment found on that position
     */
    public Fragment findFragmentByPosition(int position) {
        if (mSwipeAdapter == null || mPager == null) return null;
        FragmentPagerAdapter adapter = (FragmentPagerAdapter) mSwipeAdapter;
        return getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + mPager.getId() + ":"
                        + adapter.getItemId(position));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTime = System.currentTimeMillis();
    }

    @Override
    protected void onPause() {
        long time = System.currentTimeMillis() - mTime;

        // logging time past while activity is visible on the screen
        if (mFunctionBean != null && mFunctionBean.function != null) {
            Analytics.logGraphicShowed(mFunctionBean.function.toString(), time);
        } else {
            Analytics.logErrorGraphicNotShowed(mFunctionEntered);
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // saving data
        outState.putInt("position", mPager.getCurrentItem());
        outState.putBoolean("enabled", mPager.isPagingEnabled());
        outState.putParcelable("FunctionBean", mFunctionBean);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // restoring data and setting last visible fragment
        super.onRestoreInstanceState(savedInstanceState);
        int position = mPager.getCurrentItem();
        if (position == 0) {
            boolean enabled = savedInstanceState.getBoolean("enabled");
            mPager.setPagingEnabled(enabled);
            SolutionFragment fragment = (SolutionFragment) findFragmentByPosition(position);
            if (fragment != null) {
                fragment.setSwiping(enabled);
            }
        }
    }

    /**
     * onClick public method
     *
     * @param view View
     */
    public void swiping(View view) {
        boolean enabled = !mPager.isPagingEnabled();
        mPager.setPagingEnabled(enabled);
        SolutionFragment fragment = (SolutionFragment) findFragmentByPosition(0);
        if (fragment != null) {
            fragment.setSwiping(enabled);
        }
    }

    private class SwipeAdapter extends FragmentPagerAdapter {
        private static final int NUMBER_OF_FRAGMENTS = 6;

        public SwipeAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            SolutionFragment fragment = new SolutionFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUMBER_OF_FRAGMENTS;
        }
    }
}
