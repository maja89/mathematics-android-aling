package com.aling.mathematics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.aling.mathematics.R;
import com.aling.mathematics.fragments.FormulasFragment;
import com.aling.mathematics.mathutiles.FormulasData;
import com.aling.mathematics.widgets.MathTextView;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class ProofActivity extends BaseAppCompatActivity {
    public static final String TAG = "ProofActivity";

    public static final String KEY_FRAGMENT_POSITION = "fragment_position";
    public static final String KEY_LIST_ITEM_POSITION = "list_item_position";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proof);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        int fragmentPosition = bundle.getInt(KEY_FRAGMENT_POSITION, 0);
        int position = bundle.getInt(KEY_LIST_ITEM_POSITION, 0);

        MathTextView proof = (MathTextView) findViewById(R.id.proof);

        switch (fragmentPosition) {
            case FormulasFragment.POSITION_LOGARITHM:
                setTitle(FormulasData.logarithm.get(Integer.toString(position)));
                if (FormulasData.logarithmFormula.get(position) != null) {
                    proof.setText(FormulasData.logarithmFormula.get(position));
                } else {
                    proof.setText(getString(R.string.to_be_done));
                }
                break;
            case FormulasFragment.POSITION_TRIGONOMETRY:
                setTitle(FormulasData.trigonometry.get(Integer.toString(position)));
                if (FormulasData.trigonometryFormula.get(position) != null) {
                    proof.setText(FormulasData.trigonometryFormula.get(position));
                } else {
                    proof.setText(getString(R.string.to_be_done));
                }
                break;
            case FormulasFragment.POSITION_LIMITS:
                setTitle(FormulasData.limits.get(Integer.toString(position)));
                if (FormulasData.limitsFormula.get(position) != null) {
                    proof.setText(FormulasData.limitsFormula.get(position));
                } else {
                    proof.setText(getString(R.string.to_be_done));
                }
                break;
            case FormulasFragment.POSITION_DERIVATIVES:
                setTitle(FormulasData.derivatives.get(Integer.toString(position)));
                if (FormulasData.derivativesFormula.get(position) != null) {
                    proof.setText(FormulasData.derivativesFormula.get(position));
                } else {
                    proof.setText(getString(R.string.to_be_done));
                }
                break;
            case FormulasFragment.POSITION_INTEGRALS:
                setTitle(FormulasData.integrals.get(Integer.toString(position)));
                if (FormulasData.integralsFormula.get(position) != null) {
                    proof.setText(FormulasData.integralsFormula.get(position));
                } else {
                    proof.setText(getString(R.string.to_be_done));
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }
}
