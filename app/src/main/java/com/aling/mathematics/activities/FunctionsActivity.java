package com.aling.mathematics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aling.mathematics.R;
import com.aling.mathematics.adapters.BaseAdapter;
import com.aling.mathematics.adapters.FunctionListAdapter;
import com.aling.mathematics.analytics.Analytics;
import com.aling.mathematics.database.Database;
import com.aling.mathematics.decorators.SwipableItemAnimator;

import java.util.ArrayList;

import static com.aling.mathematics.MathematicsActivity.KEY_FUNCTION;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class FunctionsActivity extends BaseAppCompatActivity implements SwipableItemAnimator.OnSwipeListener,
        BaseAdapter.OnItemSelectionListener {
    public static final String TAG = "FunctionsActivity";

    private FunctionListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private MenuItem mSelectItem;
    private boolean mSelect = false;

    private SwipableItemAnimator mSwipeableDeleteAnimator, mSwipeableEditAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_functions);

        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        initUI();
    }

    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setting title with font
        setTitle(getString(R.string.activity_functions));

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        // add swipable animator so we can support swipe to delete
        mSwipeableDeleteAnimator = new SwipableItemAnimator(getBaseContext(), 0,
                ItemTouchHelper.RIGHT, R.drawable.ic_delete_white_24dp, R.color.light_red);
        ItemTouchHelper deleteTouchHelper = new ItemTouchHelper(mSwipeableDeleteAnimator);
        deleteTouchHelper.attachToRecyclerView(mRecyclerView);
        mSwipeableDeleteAnimator.setOnSwipeListener(this);

        // add swipable animator so we can support swipe to edit
        mSwipeableEditAnimator = new SwipableItemAnimator(getBaseContext(), 0,
                ItemTouchHelper.LEFT, R.drawable.ic_edit_white_24dp, R.color.primary);
        ItemTouchHelper editTouchHelper = new ItemTouchHelper(mSwipeableEditAnimator);
        editTouchHelper.attachToRecyclerView(mRecyclerView);
        mSwipeableEditAnimator.setOnSwipeListener(this);

        mAdapter = new FunctionListAdapter(getBaseContext());
        mAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelect) {
                    // select more items on touch
                    int itemPosition = mRecyclerView.getChildAdapterPosition(view);
                    mAdapter.toggleSelection(itemPosition);

                    // when selected items size is zero
                    if (mAdapter.getSelectedItems().size() == 0) {
                        unSelectAll();
                    }
                } else {
                    int position = mRecyclerView.getChildAdapterPosition(view);
                    ArrayList<String> function = mAdapter.getItem(position).list;
                    Intent intent = new Intent(getBaseContext(), SolutionActivity.class);
                    intent.putStringArrayListExtra(KEY_FUNCTION, function);
                    startActivity(intent);

                    Analytics.logSearchFunctionSelected(function);
                }
            }
        });
        mAdapter.setOnItemLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mSelect) return false;

                select();

                // selects first item
                int itemPosition = mRecyclerView.getChildAdapterPosition(view);
                mAdapter.toggleSelection(itemPosition);
                return true;
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemSelectionListener(FunctionsActivity.this);
        mAdapter.updateItems(Database.getFunctionsFromDB(getBaseContext()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.functions, menu);
        mSelectItem = menu.findItem(R.id.action_graph);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_graph:
                if (mSelect) {
                    Intent intent = new Intent(getBaseContext(), GraphActivity.class);
                    intent.putIntegerArrayListExtra(GraphActivity.KEY_POSITIONS, mAdapter.getSelectedItems());
                    startActivity(intent);
                    return true;
                }
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelectionStateChanged(int position, boolean selected) {
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void onSwipeLeft(int position) {
        editFunction(position);
    }

    private void editFunction(int position) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_FUNCTION, mAdapter.getItem(position).list);

        setResult(RESULT_OK, new Intent().putExtras(bundle));
        onBackPressed();
    }

    @Override
    public void onSwipeRight(int position) {
        removeFunction(position);
    }

    private void removeFunction(int position) {
        ArrayList<String> function = mAdapter.getItem(position).list;

        try {
            if (Database.deleteFunctionFromDB(getBaseContext(), function.toString())) {
                mAdapter.remove(position);

                Analytics.logSearchFunctionDeleted(function);
            }
        } catch (Exception e) {
            Log.e(TAG, "Function not deleted: " + function.toString());
        }
    }

    @Override
    public void onBackPressed() {
        if (mSelect) {
            unSelectAll();
            return;
        }

        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        super.onBackPressed();
    }

    private void select() {
        mSelect = true;
        mSelectItem.setIcon(R.drawable.ic_done_all_white_24dp);

        // disables swiping
        mSwipeableDeleteAnimator.setEnabled(false);
        mSwipeableEditAnimator.setEnabled(false);
    }

    private void unSelectAll() {
        mSelect = false;
        mSelectItem.setIcon(R.drawable.ic_done_all_white_24dp_disabled);
        mAdapter.clearSelections();

        // enables swiping
        mSwipeableDeleteAnimator.setEnabled(true);
        mSwipeableEditAnimator.setEnabled(true);
    }
}
