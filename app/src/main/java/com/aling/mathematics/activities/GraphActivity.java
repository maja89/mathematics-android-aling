package com.aling.mathematics.activities;

import android.os.Bundle;
import android.util.Log;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.FunctionBean;
import com.aling.function.service.CalculateService;
import com.aling.mathematics.R;
import com.aling.mathematics.database.Database;
import com.aling.mathematics.dialogs.ProgressDialog;
import com.aling.mathematics.widgets.GraphView;

import java.util.ArrayList;

/**
 * Created by mcekic on 9/13/17.
 */

public class GraphActivity extends BaseAppCompatActivity {
    public static final String TAG = "GraphActivity";

    public static final String KEY_POSITIONS = "positions";

    private ArrayList<FunctionArray> mFunctions = new ArrayList<>();
    private ArrayList<FunctionBean> mFunctionBeans = new ArrayList<>();

    private GraphView mGraphView;
    private int mCounter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        mGraphView = (GraphView) findViewById(R.id.graph_view);
        mGraphView.setChartTitle(R.string.chart_title_graph);

        if (savedInstanceState == null) {
            // get data from intent

            ArrayList<Integer> positions = getIntent().getIntegerArrayListExtra(KEY_POSITIONS);
            if (positions.size() == 0) {
                return;
            } else {
                ArrayList<FunctionArray> functions = Database.getFunctionsFromDB(getBaseContext());
                for (int position : positions) {
                    mFunctions.add(functions.get(position));
                    Log.d(TAG, "function: " + functions.get(position).toString());
                }
            }
            Log.d(TAG, "size: " + mFunctions.size());
            if (mFunctions.size() == 0) return;

            mCounter = 0;
            calculate();
        } else {
            // restore function parameters and set first fragment
            mFunctionBeans = savedInstanceState.getParcelableArrayList("FunctionBeans");
            mGraphView.addAllFunctions(mFunctionBeans);
            mGraphView.drawGraphView();
        }
    }

    /**
     * Recursive execution.
     */
    private void calculate() {
        if (mCounter == 0) {
            // show progress dialog
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.calculating));
            mProgressDialog.show();
        } else if (mCounter == mFunctions.size()) {
            mProgressDialog.cancel();

            mGraphView.addAllFunctions(mFunctionBeans);
            mGraphView.drawGraphView();
            return;
        }

        // calculate function parameters
        new CalculateService().execute(getBaseContext(), mFunctions.get(mCounter).list, new CalculateService.OnCalculateCompleteListener() {
            @Override
            public void function(FunctionBean function) {
                mFunctionBeans.add(function);

                if (function != null) {
                    Log.d(TAG, "function: " + function.toString());
                }
                mCounter++;
                calculate();
            }

            @Override
            public void error(int errorCode) {
                Log.d(TAG, "errorCode: " + errorCode);
                mCounter++;
                calculate();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // saving data
        outState.putParcelableArrayList("FunctionBeans", mFunctionBeans);
        super.onSaveInstanceState(outState);
    }
}
