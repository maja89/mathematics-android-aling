package com.aling.mathematics.database;

import android.content.Context;
import android.util.Log;

import com.aling.function.beans.FunctionArray;
import com.aling.mathematics.mathutiles.FormulasData;
import com.aling.mathematics.utils.Logger;

import java.util.ArrayList;

/**
 * Created by mcekic on 9/12/17.
 */

public class Database {
    public static final String TAG = "Database";

    /**
     * Returns all functions from DB.
     *
     * @param context Context
     * @return functions ArrayList<FunctionArray>
     */
    public static ArrayList<FunctionArray> getFunctionsFromDB(Context context) {
        ArrayList<FunctionArray> functions = new ArrayList<>();
        try {
            ArrayList<String> functionsFromDB = DatabaseAdapter.getAllFunctions(context);
            for (int i = functionsFromDB.size() - 1; i >= 0; i--) {
                ArrayList<String> function = split(functionsFromDB.get(i));
                functions.add(new FunctionArray(function));
            }
        } catch (Exception e) {
            Log.d(TAG, "Copying function from database failed!");
        }
        return functions;
    }

    /**
     * Adds function at the beginning of table and removes if there is same function in DB.
     *
     * @param context  Context
     * @param function String
     */
    public static void addFunctionToDB(Context context, String function) {
        // delete the existing function
        DatabaseAdapter.removeFunction(context, function);

        // add the function at the beginning
        DatabaseAdapter.insertFunction(context, function);
        Logger.debug(TAG, "saving function to db: " + function);
    }

    /**
     * Deletes function from DB.
     *
     * @param context  Context
     * @param function String
     * @return
     */
    public static boolean deleteFunctionFromDB(Context context, String function) {
        return DatabaseAdapter.removeFunction(context, function);
    }

    /**
     * Splits function from String into ArrayList<String>.
     *
     * @param func String
     * @return function ArrayList<String>
     */
    private static ArrayList<String> split(String func) {
        ArrayList<String> function = new ArrayList<String>();
        int beginning = 1;
        int ending = 1;
        for (int i = 1; i < func.length() - 1; i++) {
            if (func.charAt(i) == ',') {
                ending = i - 1;
                function.add(func.substring(beginning, ending + 1));
                beginning = i + 2;
            }
        }
        ending = func.length() - 1;
        function.add(func.substring(beginning, ending));
        return function;
    }

    /**
     * Deletes all functions from DB.
     *
     * @param context Context
     */
    public static void clearDB(Context context) {
        DatabaseAdapter.removeAll(context);
    }

    /**
     * Resets DB to default values.
     *
     * @param context Context
     */
    public static void resetDB(Context context) {
        DatabaseAdapter.removeAll(context);
        DatabaseAdapter.insertAll(context, FormulasData.functions);
    }
}
