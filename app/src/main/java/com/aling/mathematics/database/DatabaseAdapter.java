package com.aling.mathematics.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

class DatabaseAdapter {
    public static final String TAG = "DatabaseAdapter";

    // Tables
    private static final String TABLE_FUNCTIONS = "table_functions";

    // Columns
    private static final String COLUMN_ID = BaseColumns._ID;
    private static final String COLUMN_FUNCTIONS = "function";

    static final String CREATE_TABLE_FUNCTION =
            "CREATE TABLE " + TABLE_FUNCTIONS + " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_FUNCTIONS + " TEXT NOT NULL" + " )";

    private static final String[] COLUMNS_TABLE_FUNCTIONS =
            new String[]{COLUMN_ID, COLUMN_FUNCTIONS};


    /**
     * Inserts function to DB.
     *
     * @param context  Context
     * @param function String
     */
    static void insertFunction(Context context, String function) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_FUNCTIONS, function);
        DatabaseFactory.getDb(context).insert(TABLE_FUNCTIONS, null, values);
    }

    /**
     * Inserts all functions to DB.
     *
     * @param context   Context
     * @param functions ArrayList<String>
     */
    static void insertAll(Context context, ArrayList<String> functions) {
        for (String func : functions) {
            insertFunction(context, func);
        }
    }

    /**
     * Removes function from DB.
     *
     * @param context  Context
     * @param function String
     * @return
     */
    static boolean removeFunction(Context context, String function) {
        String whereClause = COLUMN_FUNCTIONS + "=?";
        String[] whereArgs = new String[]{function};
        return DatabaseFactory.getDb(context).delete(TABLE_FUNCTIONS, whereClause, whereArgs) > 0;
    }

    /**
     * Removes all functions from DB.
     *
     * @param context Context
     */
    static void removeAll(Context context) {
        DatabaseFactory.getDb(context).delete(TABLE_FUNCTIONS, null, null);
    }

    /**
     * Returns all functions from DB.
     *
     * @param context Context
     * @return functions ArrayList<String>
     */
    static ArrayList<String> getAllFunctions(Context context) {
        Cursor c = DatabaseFactory.getDb(context).query(TABLE_FUNCTIONS, COLUMNS_TABLE_FUNCTIONS,
                null, null, null, null, null);
        ArrayList<String> function = new ArrayList<String>();
        try {
            if (c != null) {
                int func = c.getColumnIndexOrThrow(COLUMN_FUNCTIONS);
                while (c.moveToNext()) {
                    function.add(c.getString(func));
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e);
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return function;
    }
}
