package com.aling.mathematics.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

class DatabaseFactory {

    private static DatabaseHelper sDbHelper;

    /**
     * @param context Context
     * @return
     */
    static SQLiteDatabase getDb(Context context) {
        if (sDbHelper == null) {
            sDbHelper = new DatabaseHelper(context);
        }
        return sDbHelper.getWritableDatabase();
    }

}
