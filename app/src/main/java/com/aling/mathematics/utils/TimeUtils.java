package com.aling.mathematics.utils;

/**
 * Created by maja on 5/4/17.
 */
public class TimeUtils {

    public static final int ONE_SECOND = 1000;

    public static final int ONE_MIN = ONE_SECOND * 60;

    public static final int ONE_HOUR = ONE_MIN * 60;

    public static final int ONE_DAY = ONE_HOUR * 24;

    public static String getTime(long time) {

        int timeHour = (int) (time / ONE_HOUR);
        int timeMin = (int) (time / ONE_MIN);
        int timeSec = (int) (time / ONE_SECOND);

        return "[h, m, s]: [" + timeHour + ", " + timeMin + ", " + timeSec + "]";
    }
}
