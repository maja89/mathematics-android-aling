
package com.aling.mathematics.mathutiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class FormulasData {
    public static final String TAG = "FormulasData";

    public static final String INTEGRAL = "\u222b";
    public static final String INFINITY = "\u221e";
    public static final String PLUS_MINUS = "\u00b1";
    public static final String PLUS_MINUS_INFINITY = PLUS_MINUS + INFINITY;
    public static final String X_GOES_PLUS_MINUS_INFINITY = "(x->" + PLUS_MINUS_INFINITY + ")";
    public static final String DX_EQUALS = "dx = ";

    public static Map<String, String> logarithm = new HashMap<String, String>();

    public static Map<Integer, String> logarithmFormula = new HashMap<>();

    public static Map<String, String> trigonometry = new HashMap<String, String>();

    public static Map<Integer, String> trigonometryFormula = new HashMap<>();

    public static Map<String, String> limits = new HashMap<String, String>();

    public static Map<Integer, String> limitsFormula = new HashMap<>();

    public static Map<String, String> derivatives = new HashMap<String, String>();

    public static Map<Integer, String> derivativesFormula = new HashMap<>();

    public static Map<String, String> integrals = new HashMap<String, String>();

    public static Map<Integer, String> integralsFormula = new HashMap<>();

    public static ArrayList<String> functions = new ArrayList<String>();

    static {
        logarithm.put("0", "log(c, a*b) = log(c, a)+log(c, b)");
        logarithm.put("1", "log(c, a/b) = log(c, a)-log(c, b)");
        logarithm.put("2", "ln(e) = 1");
        logarithm.put("3", "log(a, b^c) = c*log(a, b)");
        logarithm.put("4", "log(a^c, b) = (c^(-1))*log(a,b)");
        logarithm.put("5", "log(a, b) = ln(a)/ln(b)");
        logarithm.put("6", "log(a, b) = 1/log(b, a)");
        logarithm.put("7", "a^(log(a, b)) = b");
        logarithm.put("8", "log(a, a^b) = b");
        logarithm.put("9", "a^log(b, c) = c^log(b, a)");
    }

    /*static {
        logarithmFormula.put(0, "log(c, a*b) = log(c, a)+log(c, b)");
        logarithmFormula.put(1, "log(c, a/b) = log(c, a)-log(c, b)");
        logarithmFormula.put(2, "ln(e) = 1");
        logarithmFormula.put(3, "log(a, b^c) = c*log(a, b)");
        logarithmFormula.put(4, "log(a^c, b) = (c^(-1))*log(a,b)");
        logarithmFormula.put(5, "log(a, b) = ln(a)/ln(b)");
        logarithmFormula.put(6, "log(a, b) = 1/log(b, a)");
        logarithmFormula.put(7, "a^(log(a, b)) = b");
        logarithmFormula.put(8, "log(a, a^b) = b");
        logarithmFormula.put(9, "a^log(b, c) = c^log(b, a)");
    }*/

    static {
        trigonometry.put("0", "(sin(x))^2+(cos(x))^2 = 1");
        trigonometry.put("1", "tan(x) = (sin(x))/(cos(x))");
        trigonometry.put("2", "cot(x) = (cos(x))/(sin(x))");
        trigonometry.put("3", "sin(x) = tan(x)/sqrt(1+tan(x)^2)");
        trigonometry.put("4", "cos(x) = 1/sqrt(1+tan(x)^2)");
        trigonometry.put("5", "tan(x) = sin(x)/sqrt(1-sin(x)^2)");
        trigonometry.put("6", "cot(x) = sqrt(1-sin(x)^2)/sin(x)");
        trigonometry.put("7", "sin(a+-b) = sin(a)*cos(b)+-cos(a)*sin(b)");
        trigonometry.put("8", "cos(a+-b) = cos(a)*cos(b)-+sin(a)*sin(b)");
        trigonometry.put("9", "tan(a+-b) = (tan(a)+-tan(b))/(1-+tan(a)*tan(b))");
        trigonometry.put("10", "cot(a+-b) = (cot(a)*cot(b)-+1)/(cot(b)+-cot(a))");
        trigonometry.put("11", "sin(2*x) = 2*sin(x)*cos(x)");
        trigonometry.put("12", "cos(2*x) = cos(x)^2-sin(x)^2");
        trigonometry.put("13", "tan(2*x) = (2*tan(x))/(1-tan(x)^2)");
        trigonometry.put("14", "cot(2*x) = (cot(x)^2-1)/(2*cot(x))");
        trigonometry.put("15", "sin(a)+sin(b) = 2*sin((a+b)/2)*cos((a-b)/2)");
        trigonometry.put("16", "sin(a)-sin(b) = 2*cos((a+b)/2)*sin((a-b)/2)");
        trigonometry.put("17", "cos(a)+cos(b) = 2*cos((a+b)/2)*cos((a-b)/2)");
        trigonometry.put("18", "cos(a)-cos(b) = -2*sin((a+b)/2)*sin((a-b)/2)");
        trigonometry.put("19", "tan(a)+-tan(b) = sin(a+-b)/(cos(a)*cos(b))");
        trigonometry.put("20", "cot(a)+-cot(b) = +-sin(a+-b)/(sin(a)*sin(b))");
        trigonometry.put("21", "sin(a)*sin(b) = -1/2*(cos(a+b)-cos(a-b))");
        trigonometry.put("22", "cos(a)*cos(b) = 1/2*(cos(a+b)+cos(a-b))");
        trigonometry.put("23", "sin(a)*cos(b) = 1/2*(sin(a+b)+sin(a-b))");
        trigonometry.put("24", "sin(x/2) = sqrt((1-cos(x))/2)");
        trigonometry.put("25", "cos(x/2) = sqrt((1+cos(x))/2)");
        trigonometry.put("26", "tan(x/2) = sqrt((1-cos(x))/(1+cos(x)))");
        trigonometry.put("27", "cot(x/2) = sqrt((1+cos(x))/(1-cos(x)))");
    }

    static {
        limits.put("0", "lim" + X_GOES_PLUS_MINUS_INFINITY + " (1+1/x)^x = e");
        limits.put("1", "lim" + X_GOES_PLUS_MINUS_INFINITY + " (1-1/x)^x = e^(-1)");
        limits.put("2", "lim, x->0, (e^x-1)/x = 1");
        limits.put("3", "lim, x->0, ((ln(x+1))/x = 1");
        limits.put("4", "lim, x->0, (sin(x)/x = 1");
        limits.put("5", "0/0");
        limits.put("6", INFINITY + "/" + INFINITY);
        limits.put("7", "0*" + INFINITY);
        limits.put("8", INFINITY + "-" + INFINITY);
        limits.put("9", "1^" + INFINITY);
        limits.put("10", "0^0");
        limits.put("11", "0^" + INFINITY);

    }

    static {
        derivatives.put("0", "(x^a)' = a*x^(a-1)");
        derivatives.put("1", "(a^x)' = a^x*ln(a)");
        derivatives.put("2", "(e^x)' = e^x");
        derivatives.put("3", "(log(a, x))' = 1/(x*ln(a))");
        derivatives.put("4", "(ln(|x|))' = 1/x");
        derivatives.put("5", "(sin(x))' = cos(x)");
        derivatives.put("6", "(cos(x))' = -sin(x)");
        derivatives.put("7", "(tg(x))' = 1/(cos(x))^2");
        derivatives.put("8", "(ctg(x))' = -1/(sin(x))^2");
        derivatives.put("9", "(arcsin(x))' = 1/sqrt(1-x^2)");
        derivatives.put("10", "(arccos(x))' = -1/sqrt(1-x^2)");
        derivatives.put("11", "(arctg(x))' = 1/(1+x^2)");
        derivatives.put("12", "(arcctg(x))' = -1/(1+x^2)");
        derivatives.put("13", "(sinh(x))' = cosh(x)");
        derivatives.put("14", "(cosh(x))' = sinh(x)");
        derivatives.put("15", "(tgh(x))' = 1/(cosh(x))^2");
        derivatives.put("16", "(ctgh(x))' = -1/(sinh(x))^2");
        derivatives.put("17", "(arcsinh(x))' = 1/sqrt(1+x^2)");
        derivatives.put("18", "(arccosh(x))' = -1/sqrt(x^2-1)");
        derivatives.put("19", "(arctgh(x))' = 1/(1-x^2)");
        derivatives.put("20", "(arcctgh(x))' = 1/(1-x^2)");
        derivatives.put("21", "(x^x)' = x^x*(1+ln(x))");
        derivatives.put("22", "(x^f(x))' =  x^f(x)*(f(x)*ln(x))'");
        derivatives.put("23", "(f(x)^x)' = f(x)^x*(x*ln(f(x)))'");
        derivatives.put("24", "(f(x)^g(x))' = f(x)^g(x)*(g(x)*ln(f(x)))'");
        derivatives.put("25", "(f(x)*g(x))' = (f(x))'*g(x) +f(x)*(g(x))'");
        derivatives.put("26", "(f(x)/g(x))' = ((f(x))'*g(x) -f(x)*(g(x))')/g(x)^2");
    }

    static {
        integrals.put("0", INTEGRAL + "x^a" + DX_EQUALS + "x^(a+1)/(a+1)+C");
        integrals.put("1", INTEGRAL + "x^(-1)" + DX_EQUALS + "ln(x)+C");
        integrals.put("2", INTEGRAL + "e^x" + DX_EQUALS + "e^x+C");
        integrals.put("3", INTEGRAL + "a^x" + DX_EQUALS + "a^x/ln(a)+C");
        integrals.put("4", INTEGRAL + "sin(x)" + DX_EQUALS + "-cos(x)+C");
        integrals.put("5", INTEGRAL + "cos(x)" + DX_EQUALS + "sin(x)+C");
        integrals.put("6", INTEGRAL + "1/(cos(x))^2" + DX_EQUALS + "tg(x)+C");
        integrals.put("7", INTEGRAL + "1/(sin(x))^2 " + DX_EQUALS + "-ctg(x)+C");
        integrals.put("8", INTEGRAL + "1/sqrt(1-x^2)" + DX_EQUALS + "arcsin(x)+C");
        integrals.put("9", INTEGRAL + "1/(1+x^2)" + DX_EQUALS + "arctg(x)+C");
        integrals.put("10", INTEGRAL + "1/(1-x^2)" + DX_EQUALS + "1/2*ln((1+x)/(1-x))+C");
        integrals.put("11", INTEGRAL + "1/sqrt(x^2+-a^2)" + DX_EQUALS + "ln(|x+sqrt(x^2+-a^2)|)+C");
        integrals.put("12", INTEGRAL + "x/sqrt(x^2+-a^2)" + DX_EQUALS + "sqrt(x^2+-a^2)+C");
        integrals.put("13", INTEGRAL + "x/(x^2+-a^2)" + DX_EQUALS + "1/2*ln(|x^2+-a^2|)+C");
        integrals.put("14", INTEGRAL + "1/(x^2-a^2)" + DX_EQUALS + "1/(2*a)*ln(|(x-a)/(x+a)|)+C");
        integrals.put("15", INTEGRAL + "sinh(x)" + DX_EQUALS + "cosh(x)+C");
        integrals.put("16", INTEGRAL + "cosh(x)" + DX_EQUALS + "sinh(x)+C");
        integrals.put("17", INTEGRAL + "1/(cosh(x))^2" + DX_EQUALS + "tgh(x)+C");
        integrals.put("18", INTEGRAL + "1/(sinh(x))^2" + DX_EQUALS + "-ctgh(x)+C");

    }

    // sin(x) + arccos(x) + arctan(x)
    // tan( x^5 + x^2)
    static {
        functions.add("[x, ^, ln, (, x, )]");
        functions.add("[ln, (, x, ^, 2, +, x, +, 1, )]");
        functions.add("[x, ^, 2, +, x, +, 1]");
        functions.add("[sin, (, x, ), +, arccos, (, x, ), +, arctan, (, x, )]");
        functions.add("[x, ^, 3]");
        functions.add("[x, ^, x]");
        functions.add("[x, *, ln, (, x, )]");
        functions.add("[ln, (, ln, (, x, ), ), *, ln, (, x, ), )]");
        functions.add("[x, ^, 3, +, x, +, 1]");
        functions.add("[x, ^, 3, +, x, ^, 2, +, 1]");
    }

}
