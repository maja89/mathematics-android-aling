package com.aling.mathematics.mathutiles;

/**
 * Created by Maja Cekic on 10/5/14.
 */
public class Constants {

    /*
      Constants
     */

    public static final Character CHAR_0 = '0';

    public static final Character CHAR_1 = '1';

    public static final Character CHAR_2 = '2';

    public static final Character CHAR_3 = '3';

    public static final Character CHAR_4 = '4';

    public static final Character CHAR_5 = '5';

    public static final Character CHAR_6 = '6';

    public static final Character CHAR_7 = '7';

    public static final Character CHAR_8 = '8';

    public static final Character CHAR_9 = '9';

    public static final Character CHAR_DOT = '.';

    /*
     Digits Constants
     */

    public static final String DIGIT_0 = "0";

    public static final String DIGIT_1 = "1";

    public static final String DIGIT_2 = "2";

    public static final String DIGIT_3 = "3";

    public static final String DIGIT_4 = "4";

    public static final String DIGIT_5 = "5";

    public static final String DIGIT_6 = "6";

    public static final String DIGIT_7 = "7";

    public static final String DIGIT_8 = "8";

    public static final String DIGIT_9 = "9";

    /*
      Constants
     */

    public static final String X = "x";

    public static final String Z = "z";

    public static final String E = "e";

    public static final String PI = "pi";

    public static final String I = "i";

    public static final String BRACKET_OPEN = "(";

    public static final String BRACKET_CLOSE = ")";

    public static final String POINT = ".";

    /*
     Functions Constants
     */

    public static final String FUNCTION_OF_X = "f(x)=";

    public static final String FUNCTION_OF_Z = "f(z)=";

    public static final String FUNCTION_NEG = "neg";

    public static final String FUNCTION_LN = "ln";

    public static final String FUNCTION_LOG = "log";

    public static final String FUNCTION_SIN = "sin";

    public static final String FUNCTION_COS = "cos";

    public static final String FUNCTION_TAN = "tan";

    public static final String FUNCTION_COT = "cot";

    public static final String FUNCTION_ARC_SIN = "arcsin";

    public static final String FUNCTION_ARC_COS = "arccos";

    public static final String FUNCTION_ARC_TAN = "arctan";

    public static final String FUNCTION_ARC_COT = "arccot";

    public static final String FUNCTION_SINH = "sinh";

    public static final String FUNCTION_COSH = "cosh";

    public static final String FUNCTION_ABS = "abs";

    public static final String FUNCTION_SQRT = "sqrt";

    public static final String FUNCTION_EULER = "eu";

    /*
     Operators Constants
     */

    public static final String OPERATOR_PLUS = "+";

    public static final String OPERATOR_MINUS = "-";

    public static final String OPERATOR_MULTIPLY = "*";

    public static final String OPERATOR_DIVIDE = "/";

    public static final String OPERATOR_POW = "^";

}
