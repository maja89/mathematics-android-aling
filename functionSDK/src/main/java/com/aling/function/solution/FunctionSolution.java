package com.aling.function.solution;

import android.content.Context;

import com.aling.function.beans.FunctionBean;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.solution.utils.MinimalFunction;
import com.aling.function.utils.Logger;

/**
 * Created by maja on 5/7/17.
 */
public class FunctionSolution {
    public static final String TAG = "FunctionSolution";
    private FunctionBean mFunctionBean;

    private BaseFunction mBaseFunction, mFirstDerivative, mSecondDerivative;

    public FunctionSolution(BaseFunction baseFunction) {
        mBaseFunction = MinimalFunction.findMinimal(baseFunction);
    }

    public FunctionBean getCalculatedFunction(Context context) {
        mFunctionBean = new FunctionBean();
        mFunctionBean.function = mBaseFunction.getFunctionArray();
        Logger.debug(TAG, "base function: " + mBaseFunction.toString());

        calculate(context);

        mFunctionBean.setFunctions(mBaseFunction, mFirstDerivative, mSecondDerivative);

        return mFunctionBean;
    }

    private void calculate(Context context) {
        setDomain(context);
        setOddEvenPeriod(context);
        setZeroes(context);
        setPositiveNegative(context);
        setAsymptotes(context);
        setBehavior(context);
        setFirstDerivative(context);
        setPointsMinMax(context);
        setGrowthDecay(context);
        setSecondDerivative(context);
        setSaddlePoints(context);
        setConcavityConvexity(context);
    }

    /*
     *   1. Domain and break point
     */
    private void setDomain(Context context) {
        String s = "";
        Interval domain = Description.getDomain(context, mBaseFunction);
        mFunctionBean.domain = domain;
        s += Description.getTextDescription();
        mFunctionBean.breakpoints = Description.getBreakPoints(context, domain, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.textDomainBreakpoints = s;
    }

    /*
     *   2. Even, odd, period
     */
    private void setOddEvenPeriod(Context context) {
        String s = "";
        mFunctionBean.odd = Description.getOdd(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.even = Description.getEven(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.period = Description.getPeriod(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.textOddEvenPeriod = s;
    }

    /*
     *   3. Zeroes of function
     */
    private void setZeroes(Context context) {
        String s = "";
        mFunctionBean.zeroes = Description.getZeroes(context, mBaseFunction);
        mFunctionBean.textZeroes = Description.getTextDescription();
    }

    /*
     *   4. Positive, negative
     */
    private void setPositiveNegative(Context context) {
        String s = "";
        mFunctionBean.positive = Description.getPositive(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.negative = Description.getNegative(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.textPositiveNegative = s;
    }

    /*
     *   5. Asymptotes (vertical, oblique, horizontal)
     */
    private void setAsymptotes(Context context) {
        String s = "";
        mFunctionBean.asymptoteVertical = Description.getAsymptoteVertical(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.asymptoteOblique = Description.getAsymptoteOblique(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.asymptoteHorizontal = Description.getAsymptoteHorizontal(context, mBaseFunction);
        s += Description.getTextDescription();
        mFunctionBean.textAsymptote = s;
    }

    /*
     *   6. Behavior
     */
    private void setBehavior(Context context) {
        mFunctionBean.behavior = Description.getBehavior(context, mBaseFunction);
        mFunctionBean.textBehavior = Description.getTextDescription();
    }

    /*
     *   7. First Derivative
     */
    private void setFirstDerivative(Context context) {
        mFirstDerivative = Description.getFirstDerivative(context, mBaseFunction);
        mFunctionBean.firstDerivative = mFirstDerivative.getFunctionArray();
        mFunctionBean.textFirstDerivative = Description.getTextDescription();
    }

    /*
     *   8. Min and Max
     */
    private void setPointsMinMax(Context context) {
        String s = "";
        mFunctionBean.pointMin = Description.getPointsMin(context, mFirstDerivative);
        s += Description.getTextDescription();
        mFunctionBean.pointMax = Description.getPointsMax(context, mFirstDerivative);
        s += Description.getTextDescription();
        mFunctionBean.textMinMax = s;
    }

    /*
     *   9. Growth and Decay
     */
    private void setGrowthDecay(Context context) {
        String s = "";
        mFunctionBean.growth = Description.setGrowth(context, mFirstDerivative);
        s += Description.getTextDescription();
        mFunctionBean.decay = Description.setDecay(context, mFirstDerivative);
        s += Description.getTextDescription();
        mFunctionBean.textGrowthDecay = s;
    }

    /*
     *   10. Second derivative
     */
    private void setSecondDerivative(Context context) {
        mSecondDerivative = Description.getSecondDerivative(context, mFirstDerivative);
        mFunctionBean.secondDerivative = mSecondDerivative.getFunctionArray();
        mFunctionBean.textSecondDerivative = Description.getTextDescription();
    }

    /*
     *   11. Saddle Points
     */
    private void setSaddlePoints(Context context) {
        mFunctionBean.saddlePoints = Description.getSaddlePoints(context, mSecondDerivative);
        String s = Description.getTextDescription();
        mFunctionBean.textSaddlePoints = s;
    }

    /*
     *   12. Concavity, convexity
     */
    private void setConcavityConvexity(Context context) {
        String s = "";
        mFunctionBean.convexity = Description.getConvexity(context, mSecondDerivative);
        s += Description.getTextDescription();
        mFunctionBean.concavity = Description.getConcavity(context, mSecondDerivative);
        s += Description.getTextDescription();
        mFunctionBean.textConcavityConvexity = s;
    }
}
