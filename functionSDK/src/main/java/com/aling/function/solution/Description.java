
package com.aling.function.solution;

import android.content.Context;

import com.aling.function.R;
import com.aling.function.beans.Period;
import com.aling.function.beans.Idiom;
import com.aling.function.beans.Point;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.utils.Logger;

import java.util.ArrayList;

public class Description {
    public static final String TAG = "Description";

    public static final String STR_LIM_PLUS_MINUS_INFINITY = "lim(x->" + Number.STR_PLUS_MINUS_INFINITY + ")";

    private static ArrayList<String> sTextDescription = new ArrayList<String>();

    /*
     *   1. Domain
     */
    public static Interval getDomain(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        Interval domain = baseFunction.getDomain();
        sTextDescription.add(context.getResources().getString(R.string.helper_domain) + domain.toString());

        Logger.debug(TAG, "domain: " + domain.toString());
        return domain;
    }

    /*
     *   1. Break point
     */
    public static ArrayList<Point> getBreakPoints(Context context, Interval domain, BaseFunction function) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_break_points));
        ArrayList<Double> points = domain.getBreakPoints();
        ArrayList<Point> breakPoints = new ArrayList<>();
        String str = "";
        for (int i = 0; i < points.size(); i++) {
            double x = points.get(i);
            breakPoints.add(new Point(x, function.getValue(x)));
            if (i != 0) {
                str += ", ";
            }
            str += breakPoints.get(i).toString();
        }
        sTextDescription.add(str);

        Logger.debug(TAG, "breakPoints: " + str);
        return breakPoints;
    }

    /*
     *   2. Odd
     */
    public static boolean getOdd(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_odd));
        sTextDescription.add("f(x) = -f(-x)");
        BaseFunction odd = baseFunction.getOdd();
        sTextDescription.add(baseFunction.toString() + " = " + (odd != null ? baseFunction.getOdd() : ""));
        boolean isOdd = baseFunction.isOdd();
        if (isOdd) {
            sTextDescription.add(context.getResources().getString(R.string.helper_odd_text));
        } else {
            sTextDescription.add(context.getResources().getString(R.string.helper_odd_not));
        }

        Logger.debug(TAG, "isOdd: " + isOdd);
        return isOdd;
    }


    /*
     *   2. Even
     */
    public static boolean getEven(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_even));
        sTextDescription.add("f(x) = f(-x)");
        BaseFunction even = baseFunction.getEven();
        sTextDescription.add(baseFunction.toString() + " = " + (even != null ? baseFunction.getEven() : ""));
        boolean isEven = baseFunction.isEven();
        if (isEven) {
            sTextDescription.add(context.getResources().getString(R.string.helper_even_text));
        } else {
            sTextDescription.add(context.getResources().getString(R.string.helper_even_not));
        }

        Logger.debug(TAG, "isEven: " + isEven);
        return isEven;
    }

    /*
     *   2. Period
     */
    public static Period getPeriod(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_period));
        Period period = new Period();
        return period;
    }

    /*
     *   3. Zeroes of function
     */
    public static ArrayList<Point> getZeroes(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add("f(x) = " + baseFunction.toString() + " = 0");
        ArrayList<Point> zeroes = baseFunction.getZeroesOfFunction();

        Logger.debug(TAG, "zeroes: " + zeroes.size());
        return zeroes;
    }

    /*
     *   4. Positive
     */
    public static Interval getPositive(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add("f(x) = " + baseFunction.toString() + " > 0");
        Interval positive = baseFunction.getPositive();
        sTextDescription.add(context.getResources().getString(R.string.helper_positive) + positive.toString());

        Logger.debug(TAG, "positive: " + positive.toString());
        return positive;
    }

    /*
     *   4. Negative
     */
    public static Interval getNegative(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add("f(x) = " + baseFunction.toString() + " < 0");
        Interval negative = baseFunction.getNegative();
        sTextDescription.add(context.getResources().getString(R.string.helper_negative) + negative.toString());

        Logger.debug(TAG, "negative: " + negative.toString());
        return negative;
    }

    /*
     *   5. Asymptotes (vertical)
     */
    public static ArrayList<Double> getAsymptoteVertical(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_asymptote_vertical));
        sTextDescription.add(STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() + ") = a");
        sTextDescription.add("x = a");
        ArrayList<Double> vertical = new ArrayList<Double>();
        BaseFunction newBaseFunction = baseFunction.replaceXWithValue(Number.PLUS_INFINITY);
        sTextDescription.add(STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() + ") = " +
                newBaseFunction.toString());
        BaseFunction newBaseFunction2 = baseFunction.replaceXWithValue(Number.MINUS_INFINITY);
        sTextDescription.add(STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() + ") = " +
                newBaseFunction2.toString());
        return vertical;
    }

    /*
     *   5. Asymptotes (oblique)
     */
    public static ArrayList<Double> getAsymptoteOblique(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_asymptote_oblique));
        sTextDescription.add("k = " + STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() +
                ") / x)");
        sTextDescription.add("n =" + STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() +
                " - k*x)");
        sTextDescription.add("y = k*x + n");
        ArrayList<Double> oblique = new ArrayList<Double>();
        return oblique;
    }

    /*
     *   5. Asymptotes (horizontal)
     */
    public static ArrayList<Double> getAsymptoteHorizontal(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(context.getResources().getString(R.string.helper_asymptote_horizontal));
        sTextDescription.add("k = 0");
        sTextDescription.add("n =" + STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() + ")");
        sTextDescription.add("y = n");
        ArrayList<Double> horizontal = new ArrayList<Double>();
        return horizontal;
    }

    /*
    *   6. Behavior
    */
    public static ArrayList<Idiom> getBehavior(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add(STR_LIM_PLUS_MINUS_INFINITY + " (" + baseFunction.toString() + ")");
        ArrayList<Idiom> behavior = new ArrayList<Idiom>();
        return behavior;
    }

    /*
     *   7. First Derivative
     */
    public static BaseFunction getFirstDerivative(Context context, BaseFunction baseFunction) {
        sTextDescription.clear();
        sTextDescription.add("f'(x) = (" + baseFunction.toString() + ")'");
        BaseFunction firstDerivative = baseFunction.getDerivative();
        sTextDescription.add("f'(x) = " + firstDerivative.toString());

        Logger.debug(TAG, "first derivative: " + firstDerivative.toString());
        return firstDerivative;
    }

    /*
     *   8. Min
     */
    public static ArrayList<Point> getPointsMin(Context context, BaseFunction firstDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f'(x) = " + firstDerivative + " = 0 (MIN)");
        ArrayList<Point> pointsMin = firstDerivative.getZeroesOfFunction();

        Logger.debug(TAG, "points Min: " + pointsMin.size());
        return pointsMin;
    }

    /*
     *   8. Max
     */
    public static ArrayList<Point> getPointsMax(Context context, BaseFunction firstDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f'(x) = " + firstDerivative + " = 0 (MAX)");
        ArrayList<Point> pointsMax = firstDerivative.getZeroesOfFunction();

        Logger.debug(TAG, "points Max: " + pointsMax.size());
        return pointsMax;
    }

    /*
     *   9. Growth
     */
    public static Interval setGrowth(Context context, BaseFunction firstDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f'(x) = " + firstDerivative + " > 0");
        Interval growth = firstDerivative.getPositive();
        sTextDescription.add(context.getResources().getString(R.string.helper_growth) + growth.toString());

        Logger.debug(TAG, "growth: " + growth.toString());
        return growth;
    }

    /*
     *   9. Decay
     */
    public static Interval setDecay(Context context, BaseFunction firstDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f'(x) = " + firstDerivative + " < 0");
        Interval decay = firstDerivative.getNegative();
        sTextDescription.add(context.getResources().getString(R.string.helper_decay) + decay.toString());

        Logger.debug(TAG, "decay: " + decay.toString());
        return decay;
    }

    /*
     *   10. Second derivative
     */
    public static BaseFunction getSecondDerivative(Context context, BaseFunction firstDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f''(x) = (" + firstDerivative.toString() + ")'");
        BaseFunction secondDerivative = firstDerivative.getDerivative();
        sTextDescription.add("f''(x) = " + secondDerivative.toString());

        Logger.debug(TAG, "second derivative: " + secondDerivative.toString());
        return secondDerivative;
    }

    /*
     *   11. Saddle Points
     */
    public static ArrayList<Point> getSaddlePoints(Context context, BaseFunction secondDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f''(x) = " + secondDerivative + " = 0");
        ArrayList<Point> saddlePoints = secondDerivative.getZeroesOfFunction();

        Logger.debug(TAG, "saddle points: " + saddlePoints.size());
        return saddlePoints;
    }

    /*
     *   12. Convexity
     */
    public static Interval getConvexity(Context context, BaseFunction secondDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f''(x) = " + secondDerivative + " > 0");
        Interval convexity = secondDerivative.getPositive();
        sTextDescription.add(context.getResources().getString(R.string.helper_convexity) + convexity.toString());

        Logger.debug(TAG, "convexity: " + convexity.toString());
        return convexity;
    }

    /*
     *   12. Concavity
     */
    public static Interval getConcavity(Context context, BaseFunction secondDerivative) {
        sTextDescription.clear();
        sTextDescription.add("f''(x) = " + secondDerivative + " < 0");
        Interval concavity = secondDerivative.getNegative();
        sTextDescription.add(context.getResources().getString(R.string.helper_concavity) + concavity.toString());

        Logger.debug(TAG, "concavity: " + concavity.toString());
        return concavity;
    }

    public static String getTextDescription() {
        String s = "";
        for (int i = 0; i < sTextDescription.size(); i++) {
            s += " " + sTextDescription.get(i) + "\n";
        }
        return s += "\n";
    }
}
