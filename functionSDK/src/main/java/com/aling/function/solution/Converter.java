package com.aling.function.solution;

import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.math.Eu;
import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;
import com.aling.function.functions.math.Abs;
import com.aling.function.functions.math.ArcCos;
import com.aling.function.functions.math.ArcCot;
import com.aling.function.functions.math.ArcSin;
import com.aling.function.functions.math.ArcTan;
import com.aling.function.functions.math.Cos;
import com.aling.function.functions.math.Cosh;
import com.aling.function.functions.math.Cot;
import com.aling.function.functions.math.Ln;
import com.aling.function.functions.math.Log10;
import com.aling.function.functions.math.Neg;
import com.aling.function.functions.math.Sin;
import com.aling.function.functions.math.Sinh;
import com.aling.function.functions.math.Sqrt;
import com.aling.function.functions.math.Tan;
import com.aling.function.utils.Logger;

import java.util.ArrayList;

public class Converter {
    public static final String TAG = "Converter";

    public static final boolean DEBUG = true;

    public static final String CONSTANT_E = "e";
    public static final String CONSTANT_PI = "pi";
    public static final String BRACKET_OPEN = "(";
    public static final String BRACKET_CLOSED = ")";

    public static final Character CHAR_0 = '0';
    public static final Character CHAR_1 = '1';
    public static final Character CHAR_2 = '2';
    public static final Character CHAR_3 = '3';
    public static final Character CHAR_4 = '4';
    public static final Character CHAR_5 = '5';
    public static final Character CHAR_6 = '6';
    public static final Character CHAR_7 = '7';
    public static final Character CHAR_8 = '8';
    public static final Character CHAR_9 = '9';

    public static final int PRIORITY_0 = 0;
    public static final int PRIORITY_2 = 2;
    public static final int PRIORITY_3 = 3;
    public static final int PRIORITY_4 = 4;
    public static final int PRIORITY_5 = 5;
    public static final int PRIORITY_6 = 6;

    private ArrayList<String> stackOperators;
    private ArrayList<BaseFunction> stackFunctions;

    /**
     * @param function ArrayList<String>
     * @return function BaseFunction
     */
    public BaseFunction convertToFunction(ArrayList<String> function) {
        stackOperators = new ArrayList<String>();
        stackFunctions = new ArrayList<BaseFunction>();
        int inPriority = 0;
        // int stackPriority = 0;
        // int rank = 0;

        try {
            for (int i = 0; i < function.size(); i++) {
                String next = function.get(i);
                // +, -, *, /, ^
                if (OperatorFunction.isOperation(next)) {
                    Logger.debug(TAG, i + " " + next);
                    switch (next) {

                        case OperatorFunction.OPERATOR_PLUS:
                        case OperatorFunction.OPERATOR_MINUS:
                            if (inPriority == PRIORITY_2 && stackOperators.size() > 0) {
                                int j = stackOperators.size() - 1;
                                addOperatorFunction(j);
                            } else if (inPriority > PRIORITY_2 && stackOperators.size() > 0) {
                                while (stackOperators.size() > 0) {
                                    int j = stackOperators.size() - 1;
                                    if (stackOperators.get(j).equals(BRACKET_OPEN)) {
                                        break;
                                    }
                                    addOperatorFunction(j);
                                }
                            }
                            inPriority = PRIORITY_2;
                            // stackPriority = 2;
                            break;

                        case OperatorFunction.OPERATOR_MULTIPLY:
                        case OperatorFunction.OPERATOR_DIVIDE:
                            if (inPriority == PRIORITY_3 && stackOperators.size() > 0) {
                                int j = stackOperators.size() - 1;
                                addOperatorFunction(j);
                            } else if (inPriority > PRIORITY_3 && stackOperators.size() > 0) {
                                while (stackOperators.size() > 0) {
                                    int j = stackOperators.size() - 1;
                                    if (stackOperators.get(j).equals(BRACKET_OPEN)
                                            || stackOperators.get(j).equals(OperatorFunction.OPERATOR_PLUS)
                                            || stackOperators.get(j).equals(OperatorFunction.OPERATOR_MINUS)) {
                                        break;
                                    }
                                    addOperatorFunction(j);

                                }
                            }
                            inPriority = PRIORITY_3;
                            // stackPriority = 3;
                            break;

                        case OperatorFunction.OPERATOR_POW:
                            inPriority = PRIORITY_5;
                            // stackPriority = 4;
                            break;
                    }

                    stackOperators.add(stackOperators.size(), next);
                    // Log.d(TAG, i + " " + next);

                    // ln, log, sin, cos, tan, ctn...
                } else if (isMathFunction(next)) {
                    stackOperators.add(stackOperators.size(), next);
                    Logger.debug(TAG, i + " " + next);

                    // (
                } else if (next.equals(BRACKET_OPEN)) {
                    stackOperators.add(stackOperators.size(), next);
                    inPriority = PRIORITY_6;
                    // stackPriority = 0;
                    Logger.debug(TAG, i + " " + next);

                    // )
                } else if (next.equals(BRACKET_CLOSED)) {
                    debug(stackOperators, stackFunctions);

                    while (stackOperators.size() > 0) {
                        int j = stackOperators.size() - 1;
                        // Log.d(TAG, "j: " + j);
                        if (stackOperators.get(j).equals(BRACKET_OPEN)) {
                            stackOperators.remove(j);
                            break;
                        }
                        addOperatorFunction(j);
                    }
                    if (stackOperators.size() > 0) {
                        addMathFunction();

                        if (stackOperators.size() > 0) {
                            int index = stackOperators.size() - 1;
                            inPriority = getInPriority(stackOperators.get(index));
                        }
                        Logger.debug(TAG, i + " " + next);
                    }

                    // number
                } else if (isNumber(next)) {
                    BaseFunction baseFunction = new Number(Double.parseDouble(next));
                    stackFunctions.add(stackFunctions.size(), baseFunction);
                    Logger.debug(TAG, i + " " + next);
                    // rank++;

                    // constant
                } else if (isConstant(next)) {

                    BaseFunction baseFunction = null;
                    if (next.equals(CONSTANT_E)) {
                        baseFunction = Number.getConstant(Number.C_E);
                        Logger.debug(TAG, "math.e: " + baseFunction.toString());
                    } else if (next.equals(CONSTANT_PI)) {
                        baseFunction = Number.getConstant(Number.C_PI);
                        Logger.debug(TAG, "math.e: " + baseFunction.toString());
                    }

                    if (baseFunction != null) {
                        stackFunctions.add(stackFunctions.size(), baseFunction);
                    }
                    Logger.debug(TAG, i + " " + next);

                    // x and z
                } else if (next.equals(VariableFunction.X) || next.equals(VariableFunction.Z)) {
                    VariableFunction.setVariable(next);
                    stackFunctions.add(stackFunctions.size(), VariableFunction.getFunction());
                    Logger.debug(TAG, i + " " + next);
                }
            } // for


            // at the end
            while (stackOperators.size() > 0) {
                int j = stackOperators.size() - 1;
                if (stackOperators.get(j).equals(BRACKET_OPEN)) {
                    break;
                }
                addOperatorFunction(j);
            }
            Logger.debug(TAG, "Function: Done!");
        } catch (Exception e) {
            Logger.debug(TAG, "Function: Failed!");
        }

        if (stackFunctions.size() > 0 && stackFunctions.get(0) != null) {
            Logger.debug(TAG, "Function: " + stackFunctions.get(0).showNode());

            return stackFunctions.get(0);
        } else {
            // size == 0
            return null;
        }
    }

    /**
     * @param j int
     */
    private void addOperatorFunction(int j) {
        String last = stackOperators.get(j);
        OperatorFunction baseFunction = new OperatorFunction(last);
        baseFunction.setBaseFunction1(stackFunctions.get(stackFunctions.size() - 2));
        baseFunction.setBaseFunction2(stackFunctions.get(stackFunctions.size() - 1));
        stackFunctions.add(stackFunctions.size() - 2, baseFunction);
        stackFunctions.remove(stackFunctions.size() - 1);
        stackFunctions.remove(stackFunctions.size() - 1);
        stackOperators.remove(j);

        debug(stackOperators, stackFunctions);
    }

    /**
     *
     */
    private void addMathFunction() {
        String last = stackOperators.get(stackOperators.size() - 1);
        if (isMathFunction(last)) {
            MathFunction baseFunction = new MathFunction(last);
            baseFunction.setBaseFunction(stackFunctions.get(stackFunctions.size() - 1));
            stackFunctions.add(stackFunctions.size() - 1, baseFunction);
            stackFunctions.remove(stackFunctions.size() - 1);
            stackOperators.remove(stackOperators.size() - 1);

            debug(stackOperators, stackFunctions);
        }
    }

    /**
     * @param operator String
     * @return
     */
    private static int getInPriority(String operator) {
        switch (operator) {
            case OperatorFunction.OPERATOR_PLUS:
            case OperatorFunction.OPERATOR_MINUS:
                return PRIORITY_2;
            case OperatorFunction.OPERATOR_MULTIPLY:
            case OperatorFunction.OPERATOR_DIVIDE:
                return PRIORITY_3;
            case OperatorFunction.OPERATOR_POW:
                return PRIORITY_5;
            case BRACKET_OPEN:
                return PRIORITY_6;
            default:
                return PRIORITY_0;
        }
    }

    /**
     * @param s String
     * @return
     */
    private static boolean isNumber(String s) {
        char c = s.charAt(0);
        return c == CHAR_0 || c == CHAR_1 || c == CHAR_2 || c == CHAR_3 || c == CHAR_4 ||
                c == CHAR_5 || c == CHAR_6 || c == CHAR_7 || c == CHAR_8 || c == CHAR_9;
    }

    /**
     * @param s String
     * @return
     */
    private static boolean isConstant(String s) {
        return s.equals(CONSTANT_E) || s.equals(CONSTANT_PI);
    }

    /**
     * @param function String
     * @return
     */
    private static boolean isMathFunction(String function) {
        return Neg.TAG.equals(function) ||
                Ln.TAG.equals(function) || Log10.TAG.equals(function) ||
                Sin.TAG.equals(function) || Cos.TAG.equals(function) ||
                Tan.TAG.equals(function) || Cot.TAG.equals(function) ||
                ArcSin.TAG.equals(function) || ArcCos.TAG.equals(function) ||
                ArcTan.TAG.equals(function) || ArcCot.TAG.equals(function) ||
                Sinh.TAG.equals(function) || Cosh.TAG.equals(function) ||
                Abs.TAG.equals(function) || Sqrt.TAG.equals(function) ||
                Eu.TAG.equals(function);
    }

    /**
     * @param stackOperators ArrayList<String>
     * @param stackFunctions ArrayList<BaseFunction>
     */
    private void debug(ArrayList<String> stackOperators, ArrayList<BaseFunction> stackFunctions) {
        if (Logger.DEBUG && DEBUG) {
            for (int i = 0; i < stackFunctions.size(); i++) {
                Logger.debug(TAG, "stackFunction: " + i + "  "
                        + stackFunctions.get(i).toString());
            }
            for (int i = 0; i < stackOperators.size(); i++) {
                Logger.debug(TAG, "stackOperator: " + i + "  "
                        + stackOperators.get(i).toString());
            }
        }
    }
}
