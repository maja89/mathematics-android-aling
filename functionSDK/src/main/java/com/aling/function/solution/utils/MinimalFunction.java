package com.aling.function.solution.utils;

import android.util.Log;

import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.numbers.Fraction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;
import com.aling.function.functions.math.Abs;
import com.aling.function.functions.math.ArcCos;
import com.aling.function.functions.math.ArcCot;
import com.aling.function.functions.math.ArcSin;
import com.aling.function.functions.math.ArcTan;
import com.aling.function.functions.math.Cos;
import com.aling.function.functions.math.Cosh;
import com.aling.function.functions.math.Cot;
import com.aling.function.functions.math.Ln;
import com.aling.function.functions.math.Log10;
import com.aling.function.functions.math.Neg;
import com.aling.function.functions.math.Sin;
import com.aling.function.functions.math.Sinh;
import com.aling.function.functions.math.Sqrt;
import com.aling.function.functions.math.Tan;
import com.aling.function.utils.Logger;

/**
 * Created by maja on 5/11/17.
 */
public class MinimalFunction {
    public static final String TAG = "MinimalFunction";

    private static final boolean DEBUG = true;

    /**
     * Minimizes baseFunction until last minimalFunc is equal to new minimalFunc
     *
     * @param baseFunction BaseFunction
     * @return minimalFunc found or baseFunction entered if fails
     */
    public static BaseFunction findMinimal(BaseFunction baseFunction) {
        BaseFunction minimalFunc = baseFunction.clone();
        try {
            String func = minimalFunc.toString();
            minimalFunc = findMinimalEqual(minimalFunc);
            while (!func.equals(minimalFunc.toString())) {
                func = minimalFunc.toString();
                minimalFunc = findMinimalEqual(minimalFunc);
            }
            debug(TAG, "minimalFunc " + minimalFunc.showNode());
            return minimalFunc;
        } catch (Exception e) {
            debug(TAG, "findMinimal error: " + e);
        }
        debug(TAG, "baseFunction " + baseFunction.showNode());
        return baseFunction;
    }

    /**
     * Minimizes baseFunction one step
     *
     * @param baseFunction BaseFunction
     * @return minimalFunc found or baseFunction entered if fails
     */
    private static BaseFunction findMinimalEqual(BaseFunction baseFunction) {
        if (baseFunction instanceof OperatorFunction) {
            return findMinimalEqualOperator((OperatorFunction) baseFunction);
        } else if (baseFunction instanceof MathFunction) {
            return findMinimalEqualMathFunction((MathFunction) baseFunction);
        }
        return baseFunction;
    }

    /**
     * Minimizes operatorFunc one step
     *
     * @param operatorFunc OperatorFunction
     * @return minimalFunc found or operatorFunc entered
     */
    private static BaseFunction findMinimalEqualOperator(OperatorFunction operatorFunc) {
        BaseFunction newBaseFunction = null;
        debug(TAG, "findMinimalEqualOperator operator: " + operatorFunc.getBaseFunction1().showNode()
                + operatorFunc.getOperator() + operatorFunc.getBaseFunction2().showNode());

        // 3/4 operator 4/8
        if (operatorFunc.getBaseFunction1() instanceof Number && operatorFunc.getBaseFunction2() instanceof Number) {
            newBaseFunction = operationWithConstants(operatorFunc.getOperator(), (Number) operatorFunc.getBaseFunction1(),
                    (Number) operatorFunc.getBaseFunction2());
            if (newBaseFunction == null && operatorFunc.getBaseFunction1().toString().equals(operatorFunc.getBaseFunction2().toString())) {
                newBaseFunction = operationWithTwoEqualFunctions(operatorFunc.getOperator(), operatorFunc.getBaseFunction1());
            }

            // x^2+2*x+4 operator x^2+2*x+4 = 2*(x^2+2*x+4)
        } else if (operatorFunc.getBaseFunction1().toString().equals(operatorFunc.getBaseFunction2().toString())) {
            newBaseFunction = operationWithTwoEqualFunctions(operatorFunc.getOperator(), operatorFunc.getBaseFunction1());
            // f(x) operator f(x)
        } /*else if (operatorFunc.getBaseFunction1() instanceof OperatorFunction && operatorFunc.getBaseFunction2() instanceof OperatorFunction) {
            newBaseFunction = operationOperatorFunctionAndOperatorFunction((OperatorFunction) operatorFunc.getBaseFunction1(),
                    operatorFunc.getOperator(), (OperatorFunction) operatorFunc.getBaseFunction2());
        } else if (operatorFunc.getBaseFunction1() instanceof OperatorFunction && operatorFunc.getBaseFunction2() instanceof Number) {
            Number constant = (Number) operatorFunc.getBaseFunction2();
            newBaseFunction = operationOperatorFunctionAndConstant((OperatorFunction) operatorFunc.getBaseFunction1(), operatorFunc.getOperator(), constant);
        } else if (operatorFunc.getBaseFunction2() instanceof Number && operatorFunc.getBaseFunction2() instanceof OperatorFunction) {
            double c = ((Number) operatorFunc.getBaseFunction1()).getNumber();
            newBaseFunction = operationConstantAndOperatorFunction(c, operatorFunc.getOperator(), (OperatorFunction) operatorFunc.getBaseFunction2());
        }*/


        if (newBaseFunction == null) {
            operatorFunc.setBaseFunction1(findMinimalEqual(operatorFunc.getBaseFunction1()));
            operatorFunc.setBaseFunction2(findMinimalEqual(operatorFunc.getBaseFunction2()));
        }
        debug(TAG, "findMinimalEqualOperator, newBaseFunction: " + (newBaseFunction != null ? newBaseFunction.showNode() : "null"));
        debug(TAG, "findMinimalEqualOperator, operatorFunc: " + operatorFunc.showNode());
        return newBaseFunction != null ? newBaseFunction : operatorFunc;
    }

    /**
     * Minimizes mathFunc one step
     *
     * @param mathFunc MathFunction
     * @return minimalFunc found or mathFunc entered
     */

    private static BaseFunction findMinimalEqualMathFunction(MathFunction mathFunc) {
        BaseFunction newBaseFunction = null;
        debug(TAG, "findMinimalEqualMathFunction operator: " + mathFunc.getOperator());

        if (mathFunc.getBaseFunction() instanceof Number) {
            double constant = ((Number) mathFunc.getBaseFunction()).getNumber();
            double value = mathFunc.getValue(constant);
            if (value % 1 == 0) {
                // log(1), log(10), log(100), ln(1), ln(e)
                newBaseFunction = new Number(value);
            } else if (Math.PI == constant) {
                //cos(PI), sin(PI), tg(PI)
                newBaseFunction = getConstantForPI(mathFunc.getOperator(), 1);
            }
        } else if (mathFunc.getBaseFunction() instanceof OperatorFunction) {
            OperatorFunction operatorFunc = (OperatorFunction) mathFunc.getBaseFunction();
            if (operatorFunc.getBaseFunction1() instanceof Number && operatorFunc.getBaseFunction2() instanceof Number) {
                double a = ((Number) operatorFunc.getBaseFunction1()).getNumber();
                double b = ((Number) operatorFunc.getBaseFunction2()).getNumber();
                if (Math.PI == a || Math.PI == b) {
                    switch (operatorFunc.getOperator()) {
                        case OperatorFunction.OPERATOR_MULTIPLY:
                        case OperatorFunction.OPERATOR_DIVIDE:
                            // sin(3/2*PI), cos(2*PI)
                            newBaseFunction = getConstantForPI(mathFunc.getOperator(), Math.PI == a ? a : b);
                            break;
                    }
                }
            }
        } else {
            // TODO: code cleaning
            switch (mathFunc.getOperator()) {
                case Neg.TAG:
                    if (mathFunc.getBaseFunction() instanceof MathFunction) {
                        MathFunction neg = (MathFunction) mathFunc.getBaseFunction();
                        switch (neg.getOperator()) {
                            case Neg.TAG:
                                newBaseFunction = findMinimalEqual(neg.getBaseFunction());
                                break;
                            default:
                                mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                        }
                    } else {
                        mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                    }
                    break;
                case Ln.TAG:
                    break;
                case Log10.TAG:
                    break;
                case Sin.TAG:
                    if (mathFunc.getBaseFunction() instanceof MathFunction) {
                        MathFunction neg = (MathFunction) mathFunc.getBaseFunction();
                        switch (neg.getOperator()) {
                            case Neg.TAG:
                                // sin(-x) = - sin(x)
                                MathFunction func = new MathFunction(Sin.TAG);
                                func.setBaseFunction(findMinimalEqual(neg.getBaseFunction()));
                                mathFunc = new MathFunction(Neg.TAG);
                                mathFunc.setBaseFunction(func);
                                break;
                            default:
                                mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                        }
                    } else {
                        mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                    }
                    break;
                case Cos.TAG:
                    if (mathFunc.getBaseFunction() instanceof MathFunction) {
                        MathFunction neg = (MathFunction) mathFunc.getBaseFunction();
                        switch (neg.getOperator()) {
                            case Neg.TAG:
                                // cos(-x) = cos(x)
                                mathFunc.setBaseFunction(findMinimalEqual(neg.getBaseFunction()));
                                break;
                            default:
                                mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                        }
                    } else {
                        mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
                    }
                    break;
                case Tan.TAG:
                case Cot.TAG:
                case ArcSin.TAG:
                case ArcCos.TAG:
                case ArcTan.TAG:
                case ArcCot.TAG:
                case Sinh.TAG:
                case Cosh.TAG:
                case Abs.TAG:
                case Sqrt.TAG:
                case Euler.TAG:
            }
        }

        if (newBaseFunction == null) {
            mathFunc.setBaseFunction(findMinimalEqual(mathFunc.getBaseFunction()));
        }
        debug(TAG, "findMinimalEqualMathFunction, newBaseFunction: " + (newBaseFunction != null ? newBaseFunction.showNode() : "null"));
        debug(TAG, "findMinimalEqualMathFunction, operatorFunc: " + mathFunc.showNode());
        return newBaseFunction != null ? newBaseFunction : mathFunc;
    }

    /**
     * OperatorFunction
     *
     * @param operator String
     * @param numberA  Number
     * @param numberB  Number
     * @return c = a operator b
     */
    private static Number operationWithConstants(String operator, Number numberA, Number numberB) {
        debug(TAG, "operationWithConstants numberA: " + numberA.showNode());
        debug(TAG, "operationWithConstants numberB: " + numberB.showNode());

        if (numberA instanceof Fraction || numberB instanceof Fraction) {
            Fraction fractionC = null;
            Fraction fractionA = numberA instanceof Fraction ? (Fraction) numberA : new Fraction(numberA.getNumber());
            Fraction fractionB = numberB instanceof Fraction ? (Fraction) numberB : new Fraction(numberB.getNumber());
            debug(TAG, "operationWithConstants fractionA: " + fractionA.showNode());
            debug(TAG, "operationWithConstants fractionB: " + fractionB.showNode());
            switch (operator) {
                case OperatorFunction.OPERATOR_PLUS:
                    // a/b + c/d = (ad + bc) /bd
                    fractionC = new Fraction(fractionA.getDividend() * fractionB.getDivisor()
                            + fractionA.getDivisor() * fractionB.getDividend(),
                            fractionA.getDivisor() * fractionB.getDivisor());
                    break;
                case OperatorFunction.OPERATOR_MINUS:
                    // a/b - c/d = (ad - bc) /bd
                    fractionC = new Fraction(fractionA.getDividend() * fractionB.getDivisor()
                            - fractionA.getDivisor() * fractionB.getDividend(),
                            fractionA.getDivisor() * fractionB.getDivisor());
                    break;
                case OperatorFunction.OPERATOR_MULTIPLY:
                    // a/b * c/d = ac /bd
                    fractionC = new Fraction(fractionA.getDividend() * fractionB.getDividend(),
                            fractionA.getDivisor() * fractionB.getDivisor());
                    break;
                case OperatorFunction.OPERATOR_DIVIDE:
                    // (a/b) / (c/d )= ad /bc
                    fractionC = new Fraction(fractionA.getDividend() * fractionB.getDivisor(),
                            fractionA.getDivisor() * fractionB.getDividend());
                    break;
                case OperatorFunction.OPERATOR_POW:
                    // todo
                    break;
            }
            debug(TAG, "fractions: " + fractionA.showNode() + operator + fractionB.showNode()
                    + " = " + (fractionC == null ? "?" : fractionC.showNode()));

            if (fractionC != null) {
                debug(TAG, "isValidFraction: " + fractionC.isValidFraction());
                if (fractionC.isValidFraction()) {
                    return fractionC;
                } else {
                    return new Number(fractionC.getNumber());
                }
            }
        } else {

            double a = numberA.getNumber();
            double b = numberB.getNumber();

            if (Number.isKnownConstant(a) || Number.isKnownConstant(b)) {
                return null;
            }

            Number numberC = null;
            debug(TAG, "operationWithConstants a: " + a);
            debug(TAG, "operationWithConstants b: " + b);
            switch (operator) {
                case OperatorFunction.OPERATOR_PLUS:
                    // a + b = c
                    numberC = new Number(a + b);
                    break;
                case OperatorFunction.OPERATOR_MINUS:
                    // a - b = c
                    numberC = new Number(a - b);
                    break;
                case OperatorFunction.OPERATOR_MULTIPLY:
                    // a * b = c
                    numberC = new Number(a * b);
                    break;
                case OperatorFunction.OPERATOR_DIVIDE:
                    if (b != 0) {
                        Fraction fraction = new Fraction(a, b);
                        if (fraction.isValidFraction()) {
                            // a / b
                            return fraction;
                        } else {
                            // a / b = c
                            numberC = new Number(a / b);
                        }
                    }
                    break;
                case OperatorFunction.OPERATOR_POW:
                    numberC = new Number(Math.pow(a, b));
                    break;
            }
            debug(TAG, "constants " + a + operator + b + " = " + (numberC == null ? "?" : numberC.getNumber()));
            return numberC;
        }
        return null;
    }

    /**
     * OperatorFunction
     *
     * @param operator     String
     * @param baseFunction BaseFunction
     * @return c = baseFunction operator baseFunction
     */
    private static BaseFunction operationWithTwoEqualFunctions(String operator, BaseFunction baseFunction) {
        debug(TAG, "operationWithTwoEqualFunctions " + baseFunction.showNode() + operator + baseFunction.showNode());
        switch (operator) {
            case OperatorFunction.OPERATOR_PLUS:
                return new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY, Number.getConstant(Number.C_2), baseFunction);
            case OperatorFunction.OPERATOR_MINUS:
                return Number.getConstant(Number.C_0);
            case OperatorFunction.OPERATOR_MULTIPLY:
                return new OperatorFunction(OperatorFunction.OPERATOR_POW, baseFunction, Number.getConstant(Number.C_2));
            case OperatorFunction.OPERATOR_DIVIDE:
                return Number.getConstant(Number.C_1);
            case OperatorFunction.OPERATOR_POW:
                return null;
        }
        return null;
    }

    /**
     * @param operatorFunc OperatorFunction
     * @param operator     String
     * @param numberB      Number
     * @return
     */
    private static BaseFunction operationOperatorFunctionAndConstant(OperatorFunction operatorFunc, String operator, Number numberB) {
        BaseFunction newBaseFunction = null;
        if (OperatorFunction.getPriority(operator) == operatorFunc.getPriority()) {
            if (operatorFunc.getBaseFunction1() instanceof Number) {
                Number numberA = (Number) operatorFunc.getBaseFunction1();
                switch (operatorFunc.getPriority()) {
                    // (a + f(x)) - b = (a - b) + f(x)
                    case OperatorFunction.PRIORITY_2:
                        newBaseFunction = new OperatorFunction(operatorFunc.getOperator(),
                                operationWithConstants(operatorFunc.getOperator(), numberA, numberB), operatorFunc.getBaseFunction2());
                        break;
                    // (a * f(x)) / b = (a / b) * f(x)
                    case OperatorFunction.PRIORITY_3:
                        newBaseFunction = new OperatorFunction(operatorFunc.getOperator(),
                                operationWithConstants(operatorFunc.getOperator(), numberA, numberB), operatorFunc.getBaseFunction2());
                        break;
                }

                // (f(x) + a) + b
            } else if (operatorFunc.getBaseFunction2() instanceof Number) {
                Number numberA = (Number) operatorFunc.getBaseFunction2();

                /*if (operatorFunc.getOperator().equals(operatorFunc.getOperator())) {
                    newBaseFunction = new OperatorFunction(operatorFunc.getOperator(), operatorFunc.getBaseFunction1(), operationWithConstants(operatorFunc.getOperator(), a, b));
                } else if (a > b) {
                    //TODO:
                } else {
                    //TODO:
                }*/

            }
        }
        return newBaseFunction;
    }

    /**
     * @param constant     double
     * @param operator     String
     * @param operatorFunc OperatorFunction
     * @return
     */
    private static BaseFunction operationConstantAndOperatorFunction(double constant, String operator, OperatorFunction operatorFunc) {
        BaseFunction newBaseFunction = null;
        //TODO: operationConstantAndOperatorFunction
        return newBaseFunction;
    }

    /**
     * @param operatorFunc1 OperatorFunction
     * @param operator      String
     * @param operatorFunc2 OperatorFunction
     * @return newBaseFunction BaseFunction
     */
    private static BaseFunction operationOperatorFunctionAndOperatorFunction(OperatorFunction operatorFunc1, String operator, OperatorFunction operatorFunc2) {
        BaseFunction newBaseFunction = null;
        //TODO: operationOperatorFunctionAndOperatorFunction
        return newBaseFunction;
    }

    private static Number getConstantForPI(String operator, double constantOfPI) {
        Number number = null;
        switch (operator) {
            case Sin.TAG:
                if (constantOfPI % 2 == 0 || constantOfPI % 2 == 1 || constantOfPI % 2 == -1) {
                    // sin(0) = 0, sin(PI) = 0, sin(-PI) = 0
                    number = Number.getConstant(Number.C_0);
                    ;
                } else if (constantOfPI % 2 == 0.5 || constantOfPI % 2 == -1.5) {
                    // sin(PI/2) = 1, sin(-3/2*PI) = 1
                    number = Number.getConstant(Number.C_1);
                    // sin(3/2*PI) = -1, sin(-PI/2) = -1
                } else if (constantOfPI % 2 == 1.5 || constantOfPI % 2 == -0.5) {
                    number = Number.getConstant(Number.C_NEG_1);
                }
                break;
            case Cos.TAG:
                if (constantOfPI % 2 == 0.5 || constantOfPI % 2 == 1.5 || constantOfPI % 2 == -0.5 || constantOfPI % 2 == -1.5) {
                    // cos(PI/2) = 0, cos(3/2*PI) = 0, cos(-PI/2) = 0, cos(-3/2*PI) = 0
                    number = Number.getConstant(Number.C_0);
                    ;
                } else if (constantOfPI % 2 == 0) {
                    // cos(0) = 1
                    number = Number.getConstant(Number.C_1);
                    // cos(PI) = -1, cos(-PI) = -1
                } else if (constantOfPI % 2 == 1 || constantOfPI % 2 == -1) {
                    number = Number.getConstant(Number.C_NEG_1);
                }
            case Tan.TAG:
            case Cot.TAG:
        }
        return number;
    }

    private static void debug(String tag, String message) {
        if (Logger.DEBUG && DEBUG) {
            Log.d(tag, message);
        }
    }
}
