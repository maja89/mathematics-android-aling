package com.aling.function.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.aling.function.beans.FunctionBean;
import com.aling.function.functions.BaseFunction;
import com.aling.function.solution.Converter;
import com.aling.function.solution.FunctionSolution;
import com.aling.function.utils.Logger;

import java.util.ArrayList;

/**
 * Created by Maja Cekic on 2/15/15.
 */
public class CalculateService extends IntentService {
    public static final String TAG = "CalculateService";

    public static final boolean DEBUG = false;

    public static final String KEY_FUNCTION = "function";
    public static final String KEY_FUNCTION_VALUES = "function_values";
    public static final String KEY_ERROR = "error_code";

    public static final int ERROR_UNKNOWN = 0;
    public static final int ERROR_MISSING_PARAMS = 1;
    public static final int ERROR_CONVERTER_FAILED = 2;

    private NetworkReceiver mBroadcastReceiver;
    private OnCalculateCompleteListener mCallback;
    private Context mContext;

    public CalculateService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        stopSelf();

        String action = intent.getAction();
        Bundle bundle = intent.getExtras();

        if (action != null && bundle != null) {

            ArrayList<String> function = bundle.getStringArrayList(KEY_FUNCTION);
            if (function != null && function.size() != 0) {

                BaseFunction baseFunction = new Converter().convertToFunction(function);
                if (baseFunction != null) {
                    CalculateService.sendResponse(getBaseContext(), new FunctionSolution(baseFunction).getCalculatedFunction(getBaseContext()));
                } else {
                    CalculateService.sendError(getBaseContext(), CalculateService.ERROR_CONVERTER_FAILED);
                }
            } else {
                sendError(getBaseContext(), ERROR_MISSING_PARAMS);
            }
        } else {
            sendError(getBaseContext(), ERROR_MISSING_PARAMS);
        }
    }

    public static void sendResponse(Context context, FunctionBean functionValues) {
        Intent intent = new Intent(TAG);
        intent.putExtra(KEY_FUNCTION_VALUES, functionValues);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void sendError(Context context, int errorCode) {
        Intent intent = new Intent(TAG);
        intent.putExtra(KEY_ERROR, errorCode);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public void execute(Context context, ArrayList<String> function, OnCalculateCompleteListener callback) {
        mCallback = callback;
        mContext = context;
        register(TAG);

        Intent intent = new Intent(context, CalculateService.class);
        intent.setAction(TAG);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_FUNCTION, function);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    private void register(String... filters) {
        IntentFilter filter = new IntentFilter();
        for (String string : filters) {
            filter.addAction(string);
        }
        mBroadcastReceiver = new NetworkReceiver();
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mBroadcastReceiver, filter);
    }

    private void unregister() {
        if (mBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mBroadcastReceiver);
        }
    }

    private class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregister();

            FunctionBean functionBean = intent.getExtras().getParcelable(KEY_FUNCTION_VALUES);
            int errorCode = intent.getExtras().getInt(KEY_ERROR, ERROR_UNKNOWN);
            if (functionBean != null) {
                mCallback.function(functionBean);
            } else {
                mCallback.error(errorCode);
            }
        }
    }

    public interface OnCalculateCompleteListener {
        void function(FunctionBean function);

        void error(int errorCode);
    }

    public static void debug(String message) {
        if (Logger.DEBUG && DEBUG) {
            Log.d(TAG, message);
        }
    }
}
