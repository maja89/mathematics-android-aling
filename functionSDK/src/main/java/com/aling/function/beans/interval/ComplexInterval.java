package com.aling.function.beans.interval;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mcekic on 10/23/16.
 */
public class ComplexInterval extends Interval {
    public static final String TAG = "ComplexInterval";

    public static final String AND = "\u22c0";
    public static final String OR = "\u22c1";

    public static final String INTERVAL = "interval";

    @Expose
    @SerializedName(INTERVAL)
    public ArrayList<DefInterval> list;

    public ComplexInterval() {
    }

    public void add(DefInterval interval) {
        list.add(interval);
    }

    public void remove(DefInterval interval) {
        list.remove(interval);
    }

    @Override
    public Interval clone() {
        //TODO: clone
        return new ComplexInterval();
    }

    @Override
    public boolean equals(Interval interval) {
        if (interval instanceof ComplexInterval) {
            ComplexInterval complexInterval = (ComplexInterval) interval;
            if (complexInterval.list.size() != list.size()) {
                return false;
            }
            for (int i = 0; i < list.size(); i++) {
                if (!complexInterval.list.get(i).equals(((ComplexInterval) interval).list.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Interval operationAND(Interval interval) {
        //TODO: operationAND
        return new ComplexInterval();
    }

    @Override
    public Interval operationOR(Interval interval) {
        //TODO: operationOR
        return new ComplexInterval();
    }

    @Override
    public String toString() {
        if (list.size() > 0) {
            String s = list.get(0).toString();
            for (int i = 1; i < list.size(); i++) {
                s += AND + list.get(i).toString();
            }
            return s;
        }
        return "";
    }

    @Override
    public ArrayList<Double> getBreakPoints() {
        ArrayList<Double> points = new ArrayList<>();
        for (DefInterval defInterval : list) {
            points.addAll(defInterval.getBreakPoints());
        }
        return points;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(list);
    }

    protected ComplexInterval(Parcel in) {
        this.list = in.createTypedArrayList(DefInterval.CREATOR);
    }

    public static final Creator<ComplexInterval> CREATOR = new Creator<ComplexInterval>() {
        public ComplexInterval createFromParcel(Parcel source) {
            return new ComplexInterval(source);
        }

        public ComplexInterval[] newArray(int size) {
            return new ComplexInterval[size];
        }
    };
}
