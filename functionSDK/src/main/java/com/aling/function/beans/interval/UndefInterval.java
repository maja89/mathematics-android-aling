package com.aling.function.beans.interval;

import android.os.Parcel;

import java.util.ArrayList;

/**
 * Created by mcekic on 10/22/16.
 */
public class UndefInterval extends Interval {

    public static final String NOT_ELEMENT = "\u2209";

    public static final String STR_UNDEF_INTERVAL = "x " + NOT_ELEMENT + " R";

    @Override
    public Interval clone() {
        return new UndefInterval();
    }

    @Override
    public boolean equals(Interval interval) {
        return interval instanceof UndefInterval;
    }

    @Override
    public String toString() {
        return STR_UNDEF_INTERVAL;
    }

    @Override
    public ArrayList<Double> getBreakPoints() {
        return new ArrayList<>();
    }

    @Override
    public Interval operationAND(Interval interval) {
        return new UndefInterval();
    }

    @Override
    public Interval operationOR(Interval interval) {
        return interval;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public UndefInterval() {
    }

    protected UndefInterval(Parcel in) {
    }

    public static final Creator<UndefInterval> CREATOR = new Creator<UndefInterval>() {
        public UndefInterval createFromParcel(Parcel source) {
            return new UndefInterval(source);
        }

        public UndefInterval[] newArray(int size) {
            return new UndefInterval[size];
        }
    };
}
