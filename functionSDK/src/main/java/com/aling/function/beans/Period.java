package com.aling.function.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mcekic on 10/22/16.
 */
public class Period implements Parcelable{

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Period() {
    }

    protected Period(Parcel in) {
    }

    public static final Creator<Period> CREATOR = new Creator<Period>() {
        public Period createFromParcel(Parcel source) {
            return new Period(source);
        }

        public Period[] newArray(int size) {
            return new Period[size];
        }
    };
}
