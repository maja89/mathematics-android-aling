package com.aling.function.beans.interval;

import android.os.Parcelable;

import com.aling.function.beans.interval.DefInterval.IntervalType;

import java.util.ArrayList;

/**
 * Created by mcekic on 10/22/16.
 */
public abstract class Interval implements Parcelable {

    public abstract Interval clone();

    public abstract boolean equals(Interval interval);

    public abstract String toString();

    /**
     * @return x0, x1, x2, x3...
     */
    public abstract ArrayList<Double> getBreakPoints();

    public abstract Interval operationAND(Interval interval);

    public abstract Interval operationOR(Interval interval);

    public static Interval operationAND(Interval interval1, Interval interval2) {
        // UndefInterval
        if (interval1 instanceof UndefInterval || interval2 instanceof UndefInterval) {
            return new UndefInterval();

            // DefInterval
        } else if (interval1 instanceof DefInterval && interval2 instanceof DefInterval) {
            DefInterval defInterval1 = (DefInterval) interval1;
            DefInterval defInterval2 = (DefInterval) interval2;
            // (-infinity,+infinity)
            DefInterval defInterval = new DefInterval();
            if (defInterval.equals(defInterval1)) {
                return defInterval2;
            } else if (defInterval.equals(defInterval2)) {
                return defInterval1;
            } else {

                int type1;
                double x1;
                // setting first x
                if (defInterval1.x1 == defInterval2.x1) {
                    x1 = defInterval2.x1;
                    IntervalType type = new IntervalType(IntervalType.CLOSED);
                    if (type.equals(defInterval1.type1) && type.equals(defInterval1.type1)) {
                        type1 = type.type;
                    } else {
                        type1 = IntervalType.OPEN;
                    }
                } else if (defInterval1.x1 < defInterval2.x1) {
                    x1 = defInterval2.x1;
                    type1 = defInterval2.type1.type;
                } else {
                    x1 = defInterval1.x1;
                    type1 = defInterval1.type1.type;
                }
                int type2;
                double x2;
                // setting second x
                if (defInterval1.x2 == defInterval2.x2) {
                    x2 = defInterval2.x2;
                    IntervalType type = new IntervalType(IntervalType.CLOSED);
                    if (type.equals(defInterval1.type2) && type.equals(defInterval1.type2)) {
                        type2 = type.type;
                    } else {
                        type2 = IntervalType.OPEN;
                    }
                } else if (defInterval1.x2 > defInterval2.x2) {
                    x2 = defInterval2.x2;
                    type2 = defInterval2.type2.type;
                } else {
                    x2 = defInterval1.x2;
                    type2 = defInterval1.type2.type;
                }
                if (x1 > x2) return new UndefInterval();
                if (x1 == x2 && !(type1 == IntervalType.CLOSED &&
                        type2 == IntervalType.CLOSED)) {
                    return new UndefInterval();
                }
                return new DefInterval(type1, x1, x2, type2);
            }
            // ComplexInterval
        } else {

        }
        return new DefInterval();
    }

    public static Interval operationOR(Interval interval1, Interval interval2) {
        // UndefInterval
        if (interval1 instanceof UndefInterval) {
            return interval2;
        } else if (interval2 instanceof UndefInterval) {
            return interval1;

            // DefInterval
        } else if (interval1 instanceof DefInterval && interval2 instanceof DefInterval) {
            DefInterval defInterval1 = (DefInterval) interval1;
            DefInterval defInterval2 = (DefInterval) interval2;
            // (-infinity,+infinity)
            DefInterval defInterval = new DefInterval();
            if (defInterval.equals(defInterval1) || defInterval.equals(defInterval2)) {
                return defInterval;
            } else {

            }
            // ComplexInterval
        } else {

        }
        return new DefInterval();
    }
}
