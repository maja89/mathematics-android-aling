package com.aling.function.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Maja Cekic on 2/7/15.
 */
public class FunctionArray implements Parcelable {
    public static final String TAG = "FunctionArray";

    public static final String FUNCTION = "function";

    @Expose
    @SerializedName(FUNCTION)
    public ArrayList<String> list;

    public FunctionArray(ArrayList<String> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        String s = "";
        for (String str : list) {
            s += str;
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FunctionArray) {
            FunctionArray func = (FunctionArray) o;
            return (toString()).equals(func.toString());
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.list);
    }

    private FunctionArray(Parcel in) {
        this.list = (ArrayList<String>) in.readSerializable();
    }

    public static final Creator<FunctionArray> CREATOR = new Creator<FunctionArray>() {
        public FunctionArray createFromParcel(Parcel source) {
            return new FunctionArray(source);
        }

        public FunctionArray[] newArray(int size) {
            return new FunctionArray[size];
        }
    };
}
