package com.aling.function.beans.interval;

import android.os.Parcel;
import android.os.Parcelable;

import com.aling.function.functions.numbers.Number;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DefInterval extends Interval {
    public static final String TAG = "DefInterval";

    public static final String X_1 = "x_1";
    public static final String X_2 = "x_2";
    public static final String TYPE_1 = "type_1";
    public static final String TYPE_2 = "type_2";

    @Expose
    @SerializedName(TYPE_1)
    public IntervalType type1;

    @Expose
    @SerializedName(TYPE_2)
    public IntervalType type2;

    @Expose
    @SerializedName(X_1)
    public double x1;

    @Expose
    @SerializedName(X_2)
    public double x2;


    public DefInterval() {
        this.x1 = Number.MINUS_INFINITY;
        this.x2 = Number.PLUS_INFINITY;
        this.type1 = new IntervalType(IntervalType.OPEN);
        this.type2 = new IntervalType(IntervalType.OPEN);
    }

    public DefInterval(int type1, double x1, double x2, int type2) {
        this.x1 = x1;
        this.x2 = x2;
        this.type1 = new IntervalType(type1);
        this.type2 = new IntervalType(type2);
    }

    @Override
    public Interval clone() {
        return new DefInterval(type1.type, x1, x2, type2.type);
    }

    @Override
    public boolean equals(Interval interval) {
        if (interval instanceof DefInterval) {
            DefInterval defInterval = (DefInterval) interval;
            return this.type1.equals(defInterval.type1) && x1 == defInterval.x1 &&
                    x2 == defInterval.x2 && type2.equals(defInterval.type2);
        }
        return false;
    }

    /**
     * (-infinity,+infinity)
     * [e,pi]
     * [0,+infinity)
     * (0,7]
     */
    @Override
    public String toString() {
        String strPoint1, strPoint2;

        if (x1 == Number.MINUS_INFINITY) {
            strPoint1 = Number.STR_MINUS_INFINITY;
        } else if (x1 == Math.E) {
            strPoint1 = Number.STR_E;
        } else if (x1 == Math.PI) {
            strPoint1 = Number.STR_PI;
        } else if (x1 % 1 == 0) {
            strPoint1 = Integer.toString((int) x1);
        } else {
            strPoint1 = Double.toString(x1);
        }

        if (x2 == Number.PLUS_INFINITY) {
            strPoint2 = Number.STR_PLUS_INFINITY;
        } else if (x2 == Math.E) {
            strPoint2 = Number.STR_E;
        } else if (x2 == Math.PI) {
            strPoint2 = Number.STR_PI;
        } else if (x2 % 1 == 0) {
            strPoint2 = Integer.toString((int) x2);
        } else {
            strPoint2 = Double.toString(x2);
        }
        String strType1 = type1.toString1();
        String strType2 = type2.toString2();
        return strType1 + strPoint1 + ", " + strPoint2 + strType2;
    }

    @Override
    public ArrayList<Double> getBreakPoints() {
        ArrayList<Double> xs = new ArrayList<>();
        if (x1 != Number.MINUS_INFINITY && x1 != Number.PLUS_INFINITY) {
            xs.add(x1);
        }
        if (x2 != Number.MINUS_INFINITY && x2 != Number.PLUS_INFINITY) {
            xs.add(x2);
        }
        return xs;
    }

    @Override
    public Interval operationAND(Interval interval) {
        if (interval instanceof UndefInterval) {
            return new UndefInterval();

            // DefInterval
        } else if (interval instanceof DefInterval) {
            DefInterval defInterval1 = (DefInterval) interval;
            // (-infinity,+infinity)
            DefInterval defInterval = new DefInterval();
            if (defInterval.equals(defInterval1)) {
                return clone();
            } else if (defInterval.equals(this)) {
                return defInterval1;
            } else {

                int type1;
                double x1;
                // setting first x
                if (defInterval1.x1 == this.x1) {
                    x1 = this.x1;
                    IntervalType type = new IntervalType(IntervalType.CLOSED);
                    if (type.equals(defInterval1.type1) && type.equals(defInterval1.type1)) {
                        type1 = type.type;
                    } else {
                        type1 = IntervalType.OPEN;
                    }
                } else if (defInterval1.x1 < this.x1) {
                    x1 = this.x1;
                    type1 = this.type1.type;
                } else {
                    x1 = defInterval1.x1;
                    type1 = defInterval1.type1.type;
                }
                int type2;
                double x2;
                // setting second x
                if (defInterval1.x2 == this.x2) {
                    x2 = this.x2;
                    IntervalType type = new IntervalType(IntervalType.CLOSED);
                    if (type.equals(defInterval1.type2) && type.equals(defInterval1.type2)) {
                        type2 = type.type;
                    } else {
                        type2 = IntervalType.OPEN;
                    }
                } else if (defInterval1.x2 > this.x2) {
                    x2 = this.x2;
                    type2 = this.type2.type;
                } else {
                    x2 = defInterval1.x2;
                    type2 = defInterval1.type2.type;
                }
                if (x1 > x2) return new UndefInterval();
                if (x1 == x2 && !(type1 == IntervalType.CLOSED &&
                        type2 == IntervalType.CLOSED)) {
                    return new UndefInterval();
                }
                return new DefInterval(type1, x1, x2, type2);
            }
            // ComplexInterval
        } else {

        }
        return new DefInterval();
    }

    @Override
    public Interval operationOR(Interval interval) {
        //TODO: operationOR
        return new DefInterval();
    }

    public static class IntervalType implements Parcelable {
        private static final String STR_OPEN_1 = "(";
        private static final String STR_OPEN_2 = ")";
        private static final String STR_CLOSED_1 = "[";
        private static final String STR_CLOSED_2 = "]";

        public static final int OPEN = 0;
        public static final int CLOSED = 1;

        protected int type;

        protected IntervalType(int type) {
            this.type = type;
        }

        protected IntervalType clone() {
            return new IntervalType(type);
        }

        protected boolean equals(IntervalType intervalType) {
            return this.type == intervalType.type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.type);
        }

        protected IntervalType(Parcel in) {
            this.type = in.readInt();
        }

        public static final Creator<IntervalType> CREATOR = new Creator<IntervalType>() {
            public IntervalType createFromParcel(Parcel source) {
                return new IntervalType(source);
            }

            public IntervalType[] newArray(int size) {
                return new IntervalType[size];
            }
        };

        public String toString1() {
            switch (type) {
                case OPEN:
                    return STR_OPEN_1;
                case CLOSED:
                    return STR_CLOSED_1;
            }
            return "";
        }

        public String toString2() {
            switch (type) {
                case OPEN:
                    return STR_OPEN_2;
                case CLOSED:
                    return STR_CLOSED_2;
            }
            return "";
        }

        @Override
        public String toString() {
            return "";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.type1, 0);
        dest.writeParcelable(this.type2, 0);
        dest.writeDouble(this.x1);
        dest.writeDouble(this.x2);
    }

    protected DefInterval(Parcel in) {
        this.type1 = in.readParcelable(IntervalType.class.getClassLoader());
        this.type2 = in.readParcelable(IntervalType.class.getClassLoader());
        this.x1 = in.readDouble();
        this.x2 = in.readDouble();
    }

    public static final Creator<DefInterval> CREATOR = new Creator<DefInterval>() {
        public DefInterval createFromParcel(Parcel source) {
            return new DefInterval(source);
        }

        public DefInterval[] newArray(int size) {
            return new DefInterval[size];
        }
    };
}
