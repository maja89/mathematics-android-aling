package com.aling.function.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Idiom implements Parcelable {
    public static final String TAG = "Idiom";

    public static final String SIGN = "sign";
    public static final String POINT = "point";

    @Expose
    @SerializedName(SIGN)
    public int sign;

    @Expose
    @SerializedName(POINT)
    public double point;

    public Idiom() {
        sign = 1;
        point = 0;
    }

    public Idiom(int sign, double point) {
        if (sign != -1 && sign != 1) {
            sign = 1;
        }
        this.sign = sign;
        this.point = point;
    }

    public int getSign() {
        return sign;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sign);
        dest.writeDouble(this.point);
    }

    private Idiom(Parcel in) {
        this.sign = in.readInt();
        this.point = in.readDouble();
    }

    public static final Creator<Idiom> CREATOR = new Creator<Idiom>() {
        public Idiom createFromParcel(Parcel source) {
            return new Idiom(source);
        }

        public Idiom[] newArray(int size) {
            return new Idiom[size];
        }
    };
}
