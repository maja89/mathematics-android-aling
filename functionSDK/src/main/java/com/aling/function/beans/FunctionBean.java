package com.aling.function.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Complex;
import com.aling.function.functions.numbers.Euler;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Maja Cekic on 3/10/15.
 */
public class FunctionBean implements Parcelable {
    public static final String TAG = "FunctionBean";

    /**
     * @Hide
     */
    private BaseFunction mBaseFunction, mFirstDerivative, mSecondDerivative;

    public static final String BASE_FUNCTION = "base_function";

    public static final String DOMAIN = "domain";

    public static final String BREAKPOINTS = "breakpoints";

    public static final String TEXT_DOMAIN_BREAKPOINTS = "text_domain_breakpoints";

    public static final String ODD = "odd";

    public static final String EVEN = "even";

    public static final String PERIOD = "period";

    public static final String TEXT_ODD_EVEN_PERIOD = "text_odd_even_period";

    public static final String ZEROES = "zeroes";

    public static final String TEXT_ZEROES = "text_zeroes";

    public static final String POSITIVE = "positive";

    public static final String NEGATIVE = "negative";

    public static final String TEXT_POSITIVE_NEGATIVE = "text_positive_negative";

    public static final String ASYMPTOTE_VERTICAL = "asymptote_vertical";

    public static final String ASYMPTOTE_OBLIQUE = "asymptote_oblique";

    public static final String ASYMPTOTE_HORIZONTAL = "asymptote_horizontal";

    public static final String TEXT_ASYMPTOTE = "text_asymptote";

    public static final String BEHAVIOR = "behavior";

    public static final String TEXT_BEHAVIOR = "text_behavior";

    public static final String FIRST_DERIVATIVE = "first_derivative";

    public static final String TEXT_FIRST_DERIVATIVE = "text_first_derivative";

    public static final String POINT_MIN = "point_min";

    public static final String POINT_MAX = "point_max";

    public static final String TEXT_MIN_MAX = "text_min_max";

    public static final String GROWTH = "growth";

    public static final String DECAY = "decay";

    public static final String TEXT_GROWTH_DECAY = "text_growth_decay";

    public static final String SECOND_DERIVATIVE = "second_derivative";

    public static final String TEXT_SECOND_DERIVATIVE = "text_second_derivative";

    public static final String SADDLE_POINTS = "saddle_points";

    public static final String TEXT_SADDLE_POINTS = "text_saddle_points";

    public static final String CONCAVITY = "concavity";

    public static final String CONVEXITY = "convexity";

    public static final String TEXT_CONCAVITY_CONVEXITY = "text_concavity_convexity";

    @Expose
    @SerializedName(BASE_FUNCTION)
    public FunctionArray function;

    @Expose
    @SerializedName(DOMAIN)
    public Interval domain;

    @Expose
    @SerializedName(BREAKPOINTS)
    public ArrayList<Point> breakpoints;

    @Expose
    @SerializedName(TEXT_DOMAIN_BREAKPOINTS)
    public String textDomainBreakpoints;

    @Expose
    @SerializedName(ODD)
    public boolean odd;

    @Expose
    @SerializedName(EVEN)
    public boolean even;

    @Expose
    @SerializedName(PERIOD)
    public Period period;

    @Expose
    @SerializedName(TEXT_ODD_EVEN_PERIOD)
    public String textOddEvenPeriod;

    @Expose
    @SerializedName(ZEROES)
    public ArrayList<Point> zeroes;

    @Expose
    @SerializedName(TEXT_ZEROES)
    public String textZeroes;

    @Expose
    @SerializedName(POSITIVE)
    public Interval positive;

    @Expose
    @SerializedName(NEGATIVE)
    public Interval negative;

    @Expose
    @SerializedName(TEXT_POSITIVE_NEGATIVE)
    public String textPositiveNegative;

    @Expose
    @SerializedName(ASYMPTOTE_VERTICAL)
    public ArrayList<Double> asymptoteVertical;

    @Expose
    @SerializedName(ASYMPTOTE_OBLIQUE)
    public ArrayList<Double> asymptoteOblique;

    @Expose
    @SerializedName(ASYMPTOTE_HORIZONTAL)
    public ArrayList<Double> asymptoteHorizontal;

    @Expose
    @SerializedName(TEXT_ASYMPTOTE)
    public String textAsymptote;

    @Expose
    @SerializedName(BEHAVIOR)
    public ArrayList<Idiom> behavior;

    @Expose
    @SerializedName(TEXT_BEHAVIOR)
    public String textBehavior;

    @Expose
    @SerializedName(FIRST_DERIVATIVE)
    public FunctionArray firstDerivative;

    @Expose
    @SerializedName(TEXT_FIRST_DERIVATIVE)
    public String textFirstDerivative;

    @Expose
    @SerializedName(POINT_MIN)
    public ArrayList<Point> pointMin;

    @Expose
    @SerializedName(POINT_MAX)
    public ArrayList<Point> pointMax;

    @Expose
    @SerializedName(TEXT_MIN_MAX)
    public String textMinMax;

    @Expose
    @SerializedName(GROWTH)
    public Interval growth;

    @Expose
    @SerializedName(DECAY)
    public Interval decay;

    @Expose
    @SerializedName(TEXT_GROWTH_DECAY)
    public String textGrowthDecay;

    @Expose
    @SerializedName(SECOND_DERIVATIVE)
    public FunctionArray secondDerivative;

    @Expose
    @SerializedName(TEXT_SECOND_DERIVATIVE)
    public String textSecondDerivative;

    @Expose
    @SerializedName(SADDLE_POINTS)
    public ArrayList<Point> saddlePoints;

    @Expose
    @SerializedName(TEXT_SADDLE_POINTS)
    public String textSaddlePoints;

    @Expose
    @SerializedName(CONCAVITY)
    public Interval concavity;

    @Expose
    @SerializedName(CONVEXITY)
    public Interval convexity;

    @Expose
    @SerializedName(TEXT_CONCAVITY_CONVEXITY)
    public String textConcavityConvexity;

    public FunctionBean() {
    }

    public FunctionBean(FunctionArray function, Interval domain, ArrayList<Point> breakpoints,
                        String textDomainBreakpoints, boolean odd, boolean even, Period period,
                        String textOddEvenPeriod, ArrayList<Point> zeroes, String textZeroes,
                        Interval positive, Interval negative, String textPositiveNegative,
                        ArrayList<Double> asymptoteVertical, ArrayList<Double> asymptoteOblique,
                        ArrayList<Double> asymptoteHorizontal, String textAsymptote,
                        ArrayList<Idiom> behavior, String textBehavior,
                        FunctionArray firstDerivative, String textFirstDerivative,
                        ArrayList<Point> pointMin, ArrayList<Point> pointMax, String textMinMax,
                        Interval growth, Interval decay, String textGrowthDecay,
                        FunctionArray secondDerivative, String textSecondDerivative,
                        ArrayList<Point> saddlePoints, String textSaddlePoints,
                        Interval concavity, Interval convexity, String textConcavityConvexity) {
        this.function = function;
        this.domain = domain;
        this.breakpoints = breakpoints;
        this.textDomainBreakpoints = textDomainBreakpoints;
        this.odd = odd;
        this.even = even;
        this.period = period;
        this.textOddEvenPeriod = textOddEvenPeriod;
        this.zeroes = zeroes;
        this.textZeroes = textZeroes;
        this.positive = positive;
        this.negative = negative;
        this.textPositiveNegative = textPositiveNegative;
        this.asymptoteVertical = asymptoteVertical;
        this.asymptoteOblique = asymptoteOblique;
        this.asymptoteHorizontal = asymptoteHorizontal;
        this.textAsymptote = textAsymptote;
        this.behavior = behavior;
        this.textBehavior = textBehavior;
        this.firstDerivative = firstDerivative;
        this.textFirstDerivative = textFirstDerivative;
        this.pointMin = pointMin;
        this.pointMax = pointMax;
        this.textMinMax = textMinMax;
        this.growth = growth;
        this.decay = decay;
        this.textGrowthDecay = textGrowthDecay;
        this.secondDerivative = secondDerivative;
        this.textSecondDerivative = textSecondDerivative;
        this.saddlePoints = saddlePoints;
        this.textSaddlePoints = textSaddlePoints;
        this.concavity = concavity;
        this.convexity = convexity;
        this.textConcavityConvexity = textConcavityConvexity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.function, 0);
        dest.writeParcelable(this.domain, 0);
        dest.writeSerializable(this.breakpoints);
        dest.writeString(this.textDomainBreakpoints);
        dest.writeByte(odd ? (byte) 1 : (byte) 0);
        dest.writeByte(even ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.period, 0);
        dest.writeString(this.textOddEvenPeriod);
        dest.writeSerializable(this.zeroes);
        dest.writeString(this.textZeroes);
        dest.writeParcelable(this.positive, 0);
        dest.writeParcelable(this.negative, 0);
        dest.writeString(this.textPositiveNegative);
        dest.writeSerializable(this.asymptoteVertical);
        dest.writeSerializable(this.asymptoteOblique);
        dest.writeSerializable(this.asymptoteHorizontal);
        dest.writeString(this.textAsymptote);
        dest.writeSerializable(this.behavior);
        dest.writeString(this.textBehavior);
        dest.writeParcelable(this.firstDerivative, 0);
        dest.writeString(this.textFirstDerivative);
        dest.writeSerializable(this.pointMin);
        dest.writeSerializable(this.pointMax);
        dest.writeString(this.textMinMax);
        dest.writeParcelable(this.growth, 0);
        dest.writeParcelable(this.decay, 0);
        dest.writeString(this.textGrowthDecay);
        dest.writeParcelable(this.secondDerivative, 0);
        dest.writeString(this.textSecondDerivative);
        dest.writeSerializable(this.saddlePoints);
        dest.writeString(this.textSaddlePoints);
        dest.writeParcelable(this.concavity, 0);
        dest.writeParcelable(this.convexity, 0);
        dest.writeString(this.textConcavityConvexity);
    }

    private FunctionBean(Parcel in) {
        this.function = in.readParcelable(FunctionArray.class.getClassLoader());
        this.domain = in.readParcelable(Interval.class.getClassLoader());
        this.breakpoints = (ArrayList<Point>) in.readSerializable();
        this.textDomainBreakpoints = in.readString();
        this.odd = in.readByte() != 0;
        this.even = in.readByte() != 0;
        this.period = in.readParcelable(Period.class.getClassLoader());
        this.textOddEvenPeriod = in.readString();
        this.zeroes = (ArrayList<Point>) in.readSerializable();
        this.textZeroes = in.readString();
        this.positive = in.readParcelable(Interval.class.getClassLoader());
        this.negative = in.readParcelable(Interval.class.getClassLoader());
        this.textPositiveNegative = in.readString();
        this.asymptoteVertical = (ArrayList<Double>) in.readSerializable();
        this.asymptoteOblique = (ArrayList<Double>) in.readSerializable();
        this.asymptoteHorizontal = (ArrayList<Double>) in.readSerializable();
        this.textAsymptote = in.readString();
        this.behavior = (ArrayList<Idiom>) in.readSerializable();
        this.textBehavior = in.readString();
        this.firstDerivative = in.readParcelable(FunctionArray.class.getClassLoader());
        this.textFirstDerivative = in.readString();
        this.pointMin = (ArrayList<Point>) in.readSerializable();
        this.pointMax = (ArrayList<Point>) in.readSerializable();
        this.textMinMax = in.readString();
        this.growth = in.readParcelable(Interval.class.getClassLoader());
        this.decay = in.readParcelable(Interval.class.getClassLoader());
        this.textGrowthDecay = in.readString();
        this.secondDerivative = in.readParcelable(FunctionArray.class.getClassLoader());
        this.textSecondDerivative = in.readString();
        this.saddlePoints = (ArrayList<Point>) in.readSerializable();
        this.textSaddlePoints = in.readString();
        this.concavity = in.readParcelable(Interval.class.getClassLoader());
        this.convexity = in.readParcelable(Interval.class.getClassLoader());
        this.textConcavityConvexity = in.readString();
    }

    public static final Creator<FunctionBean> CREATOR = new Creator<FunctionBean>() {
        public FunctionBean createFromParcel(Parcel source) {
            return new FunctionBean(source);
        }

        public FunctionBean[] newArray(int size) {
            return new FunctionBean[size];
        }
    };

    /**
     * @Hide
     */
    public void setFunctions(BaseFunction baseFunction, BaseFunction firstDerivative, BaseFunction secondDerivative) {
        mBaseFunction = baseFunction;
        mFirstDerivative = firstDerivative;
        mSecondDerivative = secondDerivative;
    }

    public double getValue(double point) {
        return mBaseFunction.getValue(point);
    }

    public double getValueFirstDerivative(double point) {
        return mFirstDerivative.getValue(point);
    }

    public double getValueSecondDerivative(double point) {
        return mSecondDerivative.getValue(point);
    }

    public Complex getValue(Complex point) {
        return mBaseFunction.getValue(point);
    }

    public Euler getValue(Euler point) {
        return mBaseFunction.getValue(point);
    }
}
