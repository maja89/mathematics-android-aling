package com.aling.function.utils;

import android.util.Log;

/**
 * Created by Maja Cekic on 11/9/14.
 */
public class Logger {

    public static final boolean DEBUG = true;

    public static void debug(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, message);
        }
    }
}
