package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.VariableFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Abs {
    public static final String TAG = "abs";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from abs(f(x))
        // Equals: abs(f(x)')
        if (!(baseFunction instanceof Number)) {
            return new MathFunction(Abs.TAG, baseFunction.getDerivative());
        } else {
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
