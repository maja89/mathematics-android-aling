package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Cos {
    public static final String TAG = "cos";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (cos(x))' = -sin(x)
        // Derivative from (cos(f(x)))' = -sin(x)*f(x)'
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // sin(x)
            MathFunction sin = new MathFunction(Sin.TAG, baseFunction.clone());

            // - sin(x)
            MathFunction neg = new MathFunction(Neg.TAG, sin);

            //-sin(x) * f(x)'
            return new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY, neg,
                    baseFunction.getDerivative());

        } else if (baseFunction instanceof VariableFunction) {
            // sin(x)
            MathFunction sin = new MathFunction(Sin.TAG, baseFunction.clone());

            // -sin(x)
            return new MathFunction(Neg.TAG, sin);
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
