package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;
import com.aling.function.functions.VariableFunction;

import com.aling.function.beans.interval.DefInterval.IntervalType;

/**
 * Created by maja on 10/19/16.
 */
public class Log10 {
    public static final String TAG = "log";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (log(a,x))' = 1/(x*ln(a))
        // Derivative from (log(a,f(x)))' = * f(x)'/(x*ln(a))
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // x * ln(a)
            OperatorFunction funcMul = new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY,
                    baseFunction.clone(), new MathFunction(Ln.TAG, Number.getConstant(Number.C_10)));

            // f(x)'/(x*ln(a))
            return new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE,
                    baseFunction.getDerivative(), funcMul);
        } else if (baseFunction instanceof VariableFunction) {
            // x * ln(a)
            OperatorFunction funcMul = new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY,
                    baseFunction.clone(), new MathFunction(Ln.TAG, Number.getConstant(Number.C_10)));

            //1 / (x*ln(a))
            return new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE,
                    Number.getConstant(Number.C_1), funcMul);
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (0,+infinity)
        return new DefInterval(IntervalType.OPEN, 0, Number.PLUS_INFINITY, IntervalType.OPEN);
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
