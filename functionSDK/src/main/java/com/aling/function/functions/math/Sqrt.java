package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

import com.aling.function.beans.interval.DefInterval.IntervalType;
import com.aling.function.functions.numbers.Fraction;

/**
 * Created by maja on 10/19/16.
 */
public class Sqrt {
    public static final String TAG = "sqrt";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        if (!(baseFunction instanceof Number)) {
            OperatorFunction pow = new OperatorFunction(OperatorFunction.OPERATOR_POW, baseFunction,
                    new Fraction(1, 2));
            return pow.getDerivative();
        } else {
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  [0,+infinity)
        return new DefInterval(IntervalType.CLOSED, 0, Number.PLUS_INFINITY, IntervalType.OPEN);
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
