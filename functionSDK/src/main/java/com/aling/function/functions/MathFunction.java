package com.aling.function.functions;

import android.util.Log;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.math.Eu;
import com.aling.function.functions.numbers.Complex;
import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.math.Abs;
import com.aling.function.functions.math.ArcCos;
import com.aling.function.functions.math.ArcCot;
import com.aling.function.functions.math.ArcSin;
import com.aling.function.functions.math.ArcTan;
import com.aling.function.functions.math.Cos;
import com.aling.function.functions.math.Cosh;
import com.aling.function.functions.math.Cot;
import com.aling.function.functions.math.Ln;
import com.aling.function.functions.math.Log10;
import com.aling.function.functions.math.Neg;
import com.aling.function.functions.math.Sin;
import com.aling.function.functions.math.Sinh;
import com.aling.function.functions.math.Sqrt;
import com.aling.function.functions.math.Tan;
import com.aling.function.utils.Logger;

import java.util.ArrayList;

public class MathFunction extends BaseFunction {
    public static final String TAG = "MathFunction";

    public static final boolean DEBUG = false;

    private String operator; // sin,cos,abs,ln,log...
    private BaseFunction baseFunction;

    public MathFunction(String operator) {
        this.operator = operator;
    }

    public MathFunction(String operator, BaseFunction baseFunction) {
        this.operator = operator;
        this.baseFunction = baseFunction;
    }


    public String getOperator() {
        return this.operator;
    }

    public BaseFunction getBaseFunction() {
        return baseFunction;
    }

    public void setBaseFunction(BaseFunction baseFunction) {
        this.baseFunction = baseFunction;
    }

    @Override
    public BaseFunction clone() {
        return new MathFunction(this.operator, this.baseFunction.clone());
    }

    @Override
    public boolean equals(BaseFunction baseFunction) {
        if (baseFunction instanceof MathFunction) {
            MathFunction mathFunction = (MathFunction) baseFunction;
            return mathFunction.getOperator() != null && mathFunction.getOperator().equals(this.operator)
                    && mathFunction.getBaseFunction().equals(this.getBaseFunction());
        }
        return false;
    }

    @Override
    public String toString() {
        switch (operator) {
//            case Abs.TAG:
//                return "|" + baseFunction.toString() + "|";
//            case Neg.TAG:
//                if (baseFunction instanceof OperatorFunction) {
//                    return operator + "(" + baseFunction.toString() + ")";
//                }
//                return "-" + baseFunction.toString();
            default:
                return operator + "(" + baseFunction.toString() + ")";
        }
    }

    @Override
    public String showNode() {
        return toString();
    }

    @Override
    public FunctionArray getFunctionArray() {
        ArrayList<String> function = new ArrayList<>();
        switch (operator) {
//            case Abs.TAG:
//                function.add("|");
//                function.addAll(baseFunction.getFunctionArray().list);
//                function.add("|");
//                break;
//            case Neg.TAG:
//                if (baseFunction instanceof OperatorFunction) {
//                    function.add(operator);
//                    function.add("(");
//                    function.addAll(baseFunction.getFunctionArray().list);
//                    function.add(")");
//                    break;
//                }
//                function.add("-");
//                function.addAll(baseFunction.getFunctionArray().list);
//                break;
            default:
                function.add(operator);
                function.add("(");
                function.addAll(baseFunction.getFunctionArray().list);
                function.add(")");
                break;
        }
        return new FunctionArray(function);
    }

    @Override
    public double getValue(double x) {
        switch (operator) {
            case Neg.TAG:
                return -baseFunction.getValue(x);
            case Ln.TAG:
                return Math.log(baseFunction.getValue(x));
            case Log10.TAG:
                return Math.log10(baseFunction.getValue(x));
            case Sin.TAG:
                return Math.sin(baseFunction.getValue(x));
            case Cos.TAG:
                return Math.cos(baseFunction.getValue(x));
            case Tan.TAG:
                return Math.tan(baseFunction.getValue(x));
            case Cot.TAG:
                return 1 / Math.tan(baseFunction.getValue(x));
            case ArcSin.TAG:
                return Math.asin(baseFunction.getValue(x));
            case ArcCos.TAG:
                return Math.acos(baseFunction.getValue(x));
            case ArcTan.TAG:
                return Math.atan(baseFunction.getValue(x));
            case ArcCot.TAG:
                return Math.atan(1 / baseFunction.getValue(x));
            case Sinh.TAG:
                return (Math.pow(Math.E, baseFunction.getValue(x)) -
                        Math.pow(Math.E, -baseFunction.getValue(x))) / 2;
            case Cosh.TAG:
                return (Math.pow(Math.E, baseFunction.getValue(x)) +
                        Math.pow(Math.E, -baseFunction.getValue(x))) / 2;
            case Abs.TAG:
                return Math.abs(baseFunction.getValue(x));
            case Sqrt.TAG:
                return Math.sqrt(baseFunction.getValue(x));
            case Eu.TAG:
                return 0;
        }
        return 0;
    }

    @Override
    public Euler getValue(Euler euler) {
        switch (operator) {
            case Neg.TAG:
                return euler.neg();
            case Ln.TAG:
                return euler.ln();
            case Log10.TAG:
                return euler.log10();
            case Sin.TAG:
                return euler.sin();
            case Cos.TAG:
                return euler.cos();
            case Tan.TAG:
                return euler.tan();
            case Cot.TAG:
                return euler.cot();
            case ArcSin.TAG:
                return euler.arcsin();
            case ArcCos.TAG:
                return euler.arccos();
            case ArcTan.TAG:
                return euler.arctan();
            case ArcCot.TAG:
                return euler.arccot();
            case Sinh.TAG:
                return euler.sinh();
            case Cosh.TAG:
                return euler.cosh();
            case Abs.TAG:
                return euler.abs();
            case Sqrt.TAG:
                return euler.sqrt();
            case Eu.TAG:
                return euler.eu();
        }
        return null;
    }

    @Override
    public Complex getValue(Complex complex) {
        switch (operator) {
            case Neg.TAG:
                return complex.neg();
            case Ln.TAG:
                return complex.ln();
            case Log10.TAG:
                return complex.log10();
            case Sin.TAG:
                return complex.sin();
            case Cos.TAG:
                return complex.cos();
            case Tan.TAG:
                return complex.tan();
            case Cot.TAG:
                return complex.cot();
            case ArcSin.TAG:
                return complex.arcsin();
            case ArcCos.TAG:
                return complex.arccos();
            case ArcTan.TAG:
                return complex.arctan();
            case ArcCot.TAG:
                return complex.arccot();
            case Sinh.TAG:
                return complex.sinh();
            case Cosh.TAG:
                return complex.cosh();
            case Abs.TAG:
                return complex.abs();
            case Sqrt.TAG:
                return complex.sqrt();
            case Eu.TAG:
                return complex.eu();
        }
        return null;
    }

    @Override
    public Interval getDomain() {
        switch (operator) {
            case Neg.TAG:
                return Neg.getDomain(baseFunction);
            case Ln.TAG:
                return Ln.getDomain(baseFunction);
            case Log10.TAG:
                return Log10.getDomain(baseFunction);
            case Sin.TAG:
                return Sin.getDomain(baseFunction);
            case Cos.TAG:
                return Cos.getDomain(baseFunction);
            case Tan.TAG:
                return Tan.getDomain(baseFunction);
            case Cot.TAG:
                return Cot.getDomain(baseFunction);
            case ArcSin.TAG:
                return ArcSin.getDomain(baseFunction);
            case ArcCos.TAG:
                return ArcCos.getDomain(baseFunction);
            case ArcTan.TAG:
                return ArcTan.getDomain(baseFunction);
            case ArcCot.TAG:
                return ArcCot.getDomain(baseFunction);
            case Sinh.TAG:
                return Sinh.getDomain(baseFunction);
            case Cosh.TAG:
                return Cosh.getDomain(baseFunction);
            case Abs.TAG:
                return Abs.getDomain(baseFunction);
            case Sqrt.TAG:
                return Sqrt.getDomain(baseFunction);
            case Eu.TAG:
                return Sqrt.getDomain(baseFunction);
        }
        return new DefInterval();
    }

    @Override
    public BaseFunction getDerivative() {
        debug("getDerivative : " + toString());
        BaseFunction derivative = null;

        switch (operator) {
            case Neg.TAG:
                derivative = Neg.getDerivative(baseFunction);
                break;
            case Ln.TAG:
                derivative = Ln.getDerivative(baseFunction);
                break;
            case Log10.TAG:
                derivative = Log10.getDerivative(baseFunction);
                break;
            case Sin.TAG:
                derivative = Sin.getDerivative(baseFunction);
                break;
            case Cos.TAG:
                derivative = Cos.getDerivative(baseFunction);
                break;
            case Tan.TAG:
                derivative = Tan.getDerivative(baseFunction);
                break;
            case Cot.TAG:
                derivative = Cot.getDerivative(baseFunction);
                break;
            case ArcSin.TAG:
                derivative = ArcSin.getDerivative(baseFunction);
                break;
            case ArcCos.TAG:
                derivative = ArcCos.getDerivative(baseFunction);
                break;
            case ArcTan.TAG:
                derivative = ArcTan.getDerivative(baseFunction);
                break;
            case ArcCot.TAG:
                derivative = ArcCot.getDerivative(baseFunction);
                break;
            case Sinh.TAG:
                derivative = Sinh.getDerivative(baseFunction);
                break;
            case Cosh.TAG:
                derivative = Cosh.getDerivative(baseFunction);
                break;
            case Abs.TAG:
                derivative = Abs.getDerivative(baseFunction);
                break;
            case Sqrt.TAG:
                derivative = Sqrt.getDerivative(baseFunction);
                break;
            case Eu.TAG:
                derivative = Eu.getDerivative(baseFunction);
                break;
        }
        if (derivative == null) {
            return Number.getConstant(Number.C_0);
        }

        debug("getDerivative, derivative: " + derivative.toString());
        return derivative;
    }

    public static void debug(String message) {
        if (Logger.DEBUG && DEBUG) {
            Log.d(TAG, message);
        }
    }
}
