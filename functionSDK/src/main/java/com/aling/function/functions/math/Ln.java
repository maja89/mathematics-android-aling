package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

import com.aling.function.beans.interval.DefInterval.IntervalType;

/**
 * Created by maja on 10/19/16.
 */
public class Ln {
    public static final String TAG = "ln";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (ln(x))' = 1/x = x ^(-1)
        // Derivative from (ln(f(x)))' = 1/f(x) * f(x)' =f(x)'/f(x)
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            //f(x)'/f(x)
            return new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE,
                    baseFunction.getDerivative(), baseFunction.clone());
        } else if (baseFunction instanceof VariableFunction) {
            //x^(-1)
            return new OperatorFunction(OperatorFunction.OPERATOR_POW,
                    baseFunction.clone(), Number.getConstant(Number.C_NEG_1));
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (0,+infinity)
        return new DefInterval(IntervalType.OPEN, 0, Number.PLUS_INFINITY, IntervalType.OPEN);
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}

