package com.aling.function.functions.numbers;

import com.aling.function.beans.FunctionArray;
import com.aling.function.functions.OperatorFunction;
import com.aling.function.functions.BaseFunction;

import java.util.ArrayList;

/**
 * Created by maja on 11/16/16.
 */
public class Fraction extends Number {
    public static final String TAG = "Fraction";

    private double dividend;
    private double divisor;

    public Fraction(double dividend) {
        super(dividend);
        this.dividend = dividend;
        this.divisor = 1;
    }

    public Fraction(double dividend, double divisor) {
        super(dividend / divisor);
        this.dividend = dividend;
        this.divisor = divisor;
    }

    public void addDividend(double dividend) {
        super.addDividend(dividend);
        this.dividend *= dividend;
    }

    public void addDivisor(double divisor) {
        super.addDivisor(divisor);
        this.divisor *= divisor;
    }

    public double getDivisor() {
        return divisor;
    }

    public double getDividend() {
        return dividend;
    }

    public boolean isValidFraction() {
        return dividend % 1 == 0 && divisor % 1 == 0 && dividend != 0 && divisor != 1 && getNumber() % 1 != 0;
    }

    @Override
    public BaseFunction clone() {
        return new Fraction(this.dividend, this.divisor);
    }

    @Override
    public boolean equals(BaseFunction baseFunction) {
        if (baseFunction instanceof Fraction) {
            Fraction function = (Fraction) baseFunction;
            return function.dividend == this.dividend && function.divisor == this.divisor;
        }
        return false;
    }

    @Override
    public String toString() {
        if (isValidFraction()) {
            return Integer.toString((int) dividend) + OperatorFunction.OPERATOR_DIVIDE + Integer.toString((int) divisor);
        }
        return super.toString();
    }

    @Override
    public String showNode() {
        if (isValidFraction()) {
            return "{" + Integer.toString((int) dividend) + OperatorFunction.OPERATOR_DIVIDE + Integer.toString((int) divisor) + "}";
        }
        return super.showNode();
    }

    @Override
    public FunctionArray getFunctionArray() {
        ArrayList<String> function = new ArrayList<>();
        function.add(toString());
        return new FunctionArray(function);
    }

    @Override
    public BaseFunction getDerivative() {
        return new Fraction(0);
    }
}
