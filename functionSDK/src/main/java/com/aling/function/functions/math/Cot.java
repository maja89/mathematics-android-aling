package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Cot {
    public static final String TAG = "cot";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (ctg(x))' = -1/(sin(x))^2 = (sin(x))^-2
        // Derivative from (ctg(f(x)))' = -f(x)'/(sin(x))^2
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // (sin(x)) ^ 2
            OperatorFunction func = new OperatorFunction(OperatorFunction.OPERATOR_POW,
                    new MathFunction(Sin.TAG, baseFunction.clone()), Number.getConstant(Number.C_2));

            // 1 / (sin(x))^2
            OperatorFunction operatorFunction = new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE,
                    baseFunction.getDerivative(), func);
            // - 1/(sin(x))^2
            return new MathFunction(Neg.TAG, operatorFunction);
        } else if (baseFunction instanceof VariableFunction) {
            // (sin(x)) ^ -2
            OperatorFunction func = new OperatorFunction(OperatorFunction.OPERATOR_POW,
                    new MathFunction(Sin.TAG, baseFunction.clone()), Number.getConstant(Number.C_NEG_2));

            // - 1/(sin(x))^-2
            return new MathFunction(Neg.TAG, func);
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
