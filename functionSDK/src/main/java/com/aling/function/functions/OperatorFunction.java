package com.aling.function.functions;

import android.util.Log;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.numbers.Complex;
import com.aling.function.functions.numbers.Fraction;
import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.math.Ln;
import com.aling.function.utils.Logger;

import java.util.ArrayList;

public class OperatorFunction extends BaseFunction {
    public static final String TAG = "OperatorFunction";

    public static final boolean DEBUG = false;

    public static final int PRIORITY_0 = 0;
    public static final int PRIORITY_2 = 2;
    public static final int PRIORITY_3 = 3;
    public static final int PRIORITY_4 = 4;
    public static final int PRIORITY_5 = 5;
    public static final int PRIORITY_6 = 6;

    public static final String OPERATOR_PLUS = "+";
    public static final String OPERATOR_MINUS = "-";
    public static final String OPERATOR_MULTIPLY = "*";
    public static final String OPERATOR_DIVIDE = "/";
    public static final String OPERATOR_POW = "^";

    private String operator; // +,-, *, /,^..
    private BaseFunction baseFunction1;
    private BaseFunction baseFunction2;

    public OperatorFunction(String operator) {
        this.operator = operator;
    }

    public OperatorFunction(String operator, BaseFunction baseFunction1, BaseFunction baseFunction2) {
        this.operator = operator;
        this.baseFunction1 = baseFunction1;
        this.baseFunction2 = baseFunction2;
    }

    public String getOperator() {
        return this.operator;
    }

    public BaseFunction getBaseFunction1() {
        return baseFunction1;
    }

    public void setBaseFunction1(BaseFunction baseFunction1) {
        this.baseFunction1 = baseFunction1;
    }

    public BaseFunction getBaseFunction2() {
        return baseFunction2;
    }

    public void setBaseFunction2(BaseFunction baseFunction2) {
        this.baseFunction2 = baseFunction2;
    }

    @Override
    public BaseFunction clone() {
        return new OperatorFunction(this.operator, this.baseFunction1.clone(), this.baseFunction2.clone());
    }

    @Override
    public boolean equals(BaseFunction baseFunction) {
        if (baseFunction instanceof OperatorFunction) {
            OperatorFunction operatorFunction = (OperatorFunction) baseFunction;
            return operatorFunction.getOperator() != null
                    && operatorFunction.getOperator().equals(this.operator)
                    && operatorFunction.getBaseFunction1().equals(this.getBaseFunction1())
                    && operatorFunction.getBaseFunction2().equals(this.getBaseFunction2());
        }
        return false;
    }

    @Override
    public String toString() {
        String s = "";
        if (operator.equals(OPERATOR_MULTIPLY) || operator.equals(OPERATOR_DIVIDE) ||
                operator.equals(OPERATOR_POW)) {
            if (baseFunction1 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction1).getPriority()) {
                s += "(" + baseFunction1.toString() + ") ";
            } else {
                s += baseFunction1.toString() + " ";
            }
            s += operator;
            if (baseFunction2 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction2).getPriority()) {
                s += " (" + baseFunction2.toString() + ")";
            } else {
                s += " " + baseFunction2.toString();
            }
        } else {
            s += baseFunction1.toString() + " " + operator + " " + baseFunction2.toString();
        }
        return s;
    }

    @Override
    public String showNode() {
        String s = "";
        if (operator.equals(OPERATOR_MULTIPLY) || operator.equals(OPERATOR_DIVIDE) ||
                operator.equals(OPERATOR_POW)) {
            if (baseFunction1 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction1).getPriority()) {
                s += "(" + baseFunction1.showNode() + ") ";
            } else {
                s += baseFunction1.showNode() + " ";
            }
            s += operator;
            if (baseFunction2 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction2).getPriority()) {
                s += " (" + baseFunction2.showNode() + ")";
            } else {
                s += " " + baseFunction2.showNode();
            }
        } else {
            s += baseFunction1.showNode() + " " + operator + " " + baseFunction2.showNode();
        }
        return "{" + s + "}";
    }

    public int getPriority() {
        switch (operator) {
            case OPERATOR_PLUS:
            case OPERATOR_MINUS:
                return PRIORITY_2;
            case OPERATOR_MULTIPLY:
            case OPERATOR_DIVIDE:
                return PRIORITY_3;
            case OPERATOR_POW:
                return PRIORITY_5;
            case "(":
                return PRIORITY_6;
            default:
                return PRIORITY_0;
        }
    }

    public static int getPriority(String operator) {
        switch (operator) {
            case OPERATOR_PLUS:
            case OPERATOR_MINUS:
                return PRIORITY_2;
            case OPERATOR_MULTIPLY:
            case OPERATOR_DIVIDE:
                return PRIORITY_3;
            case OPERATOR_POW:
                return PRIORITY_5;
            case "(":
                return PRIORITY_6;
            default:
                return PRIORITY_0;
        }
    }

    @Override
    public FunctionArray getFunctionArray() {
        ArrayList<String> function = new ArrayList<>();
        if (operator.equals(OPERATOR_MULTIPLY) || operator.equals(OPERATOR_DIVIDE) ||
                operator.equals(OPERATOR_POW)) {
            if (baseFunction1 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction1).getPriority()) {
                function.add("(");
                function.addAll(baseFunction1.getFunctionArray().list);
                function.add(")");
            } else {
                function.addAll(baseFunction1.getFunctionArray().list);
            }
            function.add(operator);
            if (baseFunction2 instanceof OperatorFunction &&
                    getPriority() != ((OperatorFunction) baseFunction2).getPriority()) {
                function.add("(");
                function.addAll(baseFunction2.getFunctionArray().list);
                function.add(")");
            } else {
                function.addAll(baseFunction2.getFunctionArray().list);
            }
        } else {
            function.addAll(baseFunction1.getFunctionArray().list);
            function.add(operator);
            function.addAll(baseFunction2.getFunctionArray().list);
        }
        return new FunctionArray(function);
    }

    @Override
    public double getValue(double x) {
        switch (operator) {
            case OPERATOR_PLUS:
                return baseFunction1.getValue(x) + baseFunction2.getValue(x);
            case OPERATOR_MINUS:
                return baseFunction1.getValue(x) - baseFunction2.getValue(x);
            case OPERATOR_MULTIPLY:
                return baseFunction1.getValue(x) * baseFunction2.getValue(x);
            case OPERATOR_DIVIDE:
                return baseFunction1.getValue(x) / baseFunction2.getValue(x);
            case OPERATOR_POW:
                return Math.pow(baseFunction1.getValue(x), baseFunction2.getValue(x));
        }
        return 0;
    }

    @Override
    public Euler getValue(Euler euler) {
        Euler euler1 = baseFunction1.getValue(euler);
        Euler euler2 = baseFunction2.getValue(euler);

        switch (operator) {
            case OPERATOR_PLUS:
                return euler1.plus(euler2);
            case OPERATOR_MINUS:
                return euler1.minus(euler2);
            case OPERATOR_MULTIPLY:
                return euler1.multiply(euler2);
            case OPERATOR_DIVIDE:
                return euler1.divide(euler2);
            case OPERATOR_POW:
                return euler1.pow(euler2);
        }
        return null;
    }

    @Override
    public Complex getValue(Complex complex) {
        double x1 = baseFunction1.getValue(complex).x;
        double y1 = baseFunction1.getValue(complex).y;
        double x2 = baseFunction2.getValue(complex).x;
        double y2 = baseFunction2.getValue(complex).y;

        Euler euler;
        switch (operator) {
            case OPERATOR_PLUS:
                return new Complex(x1 + x2, y1 + y2);
            case OPERATOR_MINUS:
                return new Complex(x1 - x2, y1 - y2);
            case OPERATOR_MULTIPLY:
                return new Complex(x1 * x2 + y1 * y2, x1 * y2 + x2 * y1);
            case OPERATOR_DIVIDE:
                euler = getValue(complex.toEuler());
                return euler.toComplex();
            case OPERATOR_POW:
                euler = getValue(complex.toEuler());
                return euler.toComplex();
        }
        return null;
    }

    @Override
    public Interval getDomain() {
        switch (operator) {
            case OPERATOR_PLUS:
            case OPERATOR_MINUS:
            case OPERATOR_MULTIPLY:
            case OPERATOR_POW:
            case OPERATOR_DIVIDE:
        }
        return new DefInterval();
    }

    @Override
    public BaseFunction getDerivative() {
        debug("getDerivative : " + toString());

        // Derivative from C operator C
        // Equals: 0
        if (baseFunction1 instanceof Number && baseFunction2 instanceof Number) {
            return Number.getConstant(Number.C_0);
        }
        BaseFunction derivative = null;
        switch (operator) {
            case OPERATOR_PLUS:
            case OPERATOR_MINUS:
                // Derivative from f(x) +,- f(x)
                // Equals: f'(x) +,- f'(x)
                BaseFunction d1 = baseFunction1.getDerivative();
                BaseFunction d2 = baseFunction2.getDerivative();
                if (d1 instanceof Number) {
                    if (((Number) d1).getNumber() == 0) {
                        derivative = baseFunction2.getDerivative();
                        break;
                    }
                }
                if (d2 instanceof Number) {
                    if (((Number) d2).getNumber() == 0) {
                        derivative = baseFunction1.getDerivative();
                        break;
                    }
                }
                derivative = new OperatorFunction(operator,
                        baseFunction1.getDerivative(), baseFunction2.getDerivative());
                break;
            case OPERATOR_MULTIPLY:
                // Derivative from C * f(x)
                // Equals: C * f'(x)
                if (baseFunction1 instanceof Number) {
                    d2 = baseFunction2.getDerivative();
                    if (d2 instanceof Number) {
                        if (((Number) d2).getNumber() == 1) {
                            derivative = baseFunction1.clone();
                            break;
                        }
                    }
                    derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                            baseFunction1.clone(), baseFunction2.getDerivative());
                    // Derivative from f(x) * C
                    // Equals: C * f'(x)
                } else if (baseFunction2 instanceof Number) {
                    d1 = baseFunction1.getDerivative();
                    if (d1 instanceof Number) {
                        if (((Number) d1).getNumber() == 1) {
                            derivative = baseFunction2.clone();
                            break;
                        }
                    }
                    derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                            baseFunction2.clone(), baseFunction1.getDerivative());
                    // Derivative from f(x) * g(x)
                    // Equals: from f(x) * g'(x) + g(x) * f'(x)
                } else {
                    // f(x) * g'(x)
                    OperatorFunction func1 = new OperatorFunction(OPERATOR_MULTIPLY);
                    func1.setBaseFunction1(baseFunction1.clone());
                    func1.setBaseFunction2(baseFunction2.getDerivative());

                    //g(x) * f'(x)
                    OperatorFunction func2 = new OperatorFunction(OPERATOR_MULTIPLY);
                    func2.setBaseFunction1(baseFunction2.clone());
                    func2.setBaseFunction2(baseFunction1.getDerivative());

                    //f(x) * g'(x) + g(x) * f'(x)
                    derivative = new OperatorFunction(OPERATOR_PLUS, func1, func2);
                }
                break;
            // Derivative from f(x) / g(x)
            // Equals: (g(x) * f'(x) - f(x) * g'(x)) / g(x)^2
            case OPERATOR_DIVIDE:
                // Derivative from f(x) / C
                if (baseFunction1 instanceof Number) {
                    // Derivative from C / g(x)
                    // Equals: (g(x)^-1)'
                    if (((Number) baseFunction1).getNumber() == 1) {
                        OperatorFunction pow = new OperatorFunction(OPERATOR_POW, baseFunction2.clone(), Number.getConstant(Number.C_NEG_1));
                        derivative = pow.getDerivative();
                        break;
                        // Derivative from C / g(x)
                        // Equals: (C*g(x)^-1)'
                    } else {
                        OperatorFunction pow = new OperatorFunction(OPERATOR_POW, baseFunction2.clone(), Number.getConstant(Number.C_NEG_1));
                        OperatorFunction mul = new OperatorFunction(OPERATOR_MULTIPLY, baseFunction1.clone(), pow);
                        derivative = mul.getDerivative();
                        break;
                    }
                }
                // Derivative from f(x) / C
                if (baseFunction2 instanceof Number) {
                    // Derivative from f(x) / 1
                    // Equals:  f'(x)
                    if (((Number) baseFunction2).getNumber() == 1) {
                        derivative = baseFunction1.getDerivative();
                        break;
                        // Derivative from f(x) / C
                        // Equals: 1/C* f'(x)
                    } else {
                        double constant = ((Number) baseFunction2).getNumber();
                        if (constant % 1 == 0 && constant != 0 && !((Number) baseFunction2).isKnownConstant()) {
                            Fraction fraction = new Fraction(1, (int) constant);
                            OperatorFunction mul = new OperatorFunction(OPERATOR_MULTIPLY, fraction, baseFunction1.clone());
                            derivative = mul.getDerivative();
                            break;
                        } else {
                            OperatorFunction div = new OperatorFunction(OPERATOR_DIVIDE, Number.getConstant(Number.C_1), baseFunction2.clone());
                            OperatorFunction mul = new OperatorFunction(OPERATOR_MULTIPLY, div, baseFunction1.clone());
                            derivative = mul.getDerivative();
                            break;
                        }
                    }
                }
                //g(x) * f'(x)
                OperatorFunction func1 = new OperatorFunction(OPERATOR_MULTIPLY,
                        baseFunction2.clone(), baseFunction1.getDerivative());
                debug("func1: " + func1.toString());

                //f(x) * g'(x)
                OperatorFunction func2 = new OperatorFunction(OPERATOR_MULTIPLY,
                        baseFunction1.clone(), baseFunction2.getDerivative());
                debug("func2: " + func2.toString());

                //(g(x) * f'(x) - f(x) * g'(x))
                OperatorFunction sub1 = new OperatorFunction(OPERATOR_MINUS, func1, func2);
                debug("sub1: " + sub1.toString());

                //g(x)^2
                OperatorFunction sub2 = new OperatorFunction(OPERATOR_POW,
                        baseFunction2.clone(), Number.getConstant(Number.C_2));
                debug("sub2: " + sub2.toString());

                //(g(x) * f'(x) - f(x) * g'(x)) / g(x)^2
                derivative = new OperatorFunction(OPERATOR_DIVIDE, sub1, sub2);
                debug("derivative: " + derivative.toString());
                break;
            case OPERATOR_POW:
                // Derivative from x^C
                // Equals: x^C * (C*ln(x))'
                // Equals: C * x^(C-1)
                if (baseFunction1 instanceof VariableFunction && baseFunction2 instanceof Number) {
                    if (((Number) baseFunction2).isKnownConstant()) {
                        // C-1
                        OperatorFunction funcMin = new OperatorFunction(OPERATOR_MINUS,
                                baseFunction2.clone(), Number.getConstant(Number.C_1));
                        debug("funcMin: " + funcMin.toString());

                        // x ^ (C-1)
                        OperatorFunction funPow = new OperatorFunction(OPERATOR_POW, baseFunction1.clone(),
                                funcMin);
                        debug("funPow: " + funPow.toString());

                        // C * x^(C-1)
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                baseFunction2.clone(), funPow);
                        debug("derivative: " + derivative.toString());
                    } else {
                        // x ^ (C-1 = C)
                        OperatorFunction funPow = new OperatorFunction(OPERATOR_POW, baseFunction1.clone(),
                                new Number(((Number) baseFunction2).getNumber() - 1));
                        debug("funPow: " + funPow.toString());

                        // C * x^(C-1 = C)
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                baseFunction2.clone(), funPow);
                        debug("derivative: " + derivative.toString());
                    }

                    // Derivative from f(x)^C
                    // Equals: f(x)^C * (C*ln(f(x)))'
                    // Equals: C * f(x)^(C-1) * (f(x))'
                } else if ((baseFunction1 instanceof MathFunction || baseFunction1 instanceof OperatorFunction)
                        && baseFunction2 instanceof Number) {
                    if (((Number) baseFunction2).isKnownConstant()) {
                        // C-1
                        OperatorFunction funcMin = new OperatorFunction(OPERATOR_MINUS,
                                baseFunction2.clone(), Number.getConstant(Number.C_1));
                        debug("funcMin: " + funcMin.toString());

                        // f(x) ^ (C-1)
                        OperatorFunction funPow = new OperatorFunction(OPERATOR_POW, baseFunction1.clone(),
                                funcMin);
                        debug("funPow: " + funPow.toString());

                        // C * x^(C-1)
                        OperatorFunction funMul = new OperatorFunction(OPERATOR_MULTIPLY,
                                baseFunction2.clone(), funPow);
                        debug("funMul: " + funMul.toString());

                        // C * f(x)^(C-1) * (f(x))'
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                funMul, baseFunction1.getDerivative());
                        debug("derivative: " + derivative.toString());
                    } else {
                        // f(x) ^ (C-1 = C)
                        OperatorFunction funPow = new OperatorFunction(OPERATOR_POW, baseFunction1.clone(),
                                new Number(((Number) baseFunction2).getNumber() - 1));
                        debug("funPow: " + funPow.toString());

                        // C * x^(C-1)
                        OperatorFunction funMul = new OperatorFunction(OPERATOR_MULTIPLY,
                                baseFunction2.clone(), funPow);
                        debug("funMul: " + funMul.toString());

                        // C * f(x)^(C-1) * (f(x))'
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                funMul, baseFunction1.getDerivative());
                        debug("derivative: " + derivative.toString());
                    }

                    // Derivative from C^x
                    // Equals: C^x * (x*ln(C))'
                    // Equals: C^x * ln(C)
                } else if (baseFunction1 instanceof Number && baseFunction2 instanceof VariableFunction) {
                    if (((Number) baseFunction1).getNumber() == Math.E) {
                        // Derivative from e^x
                        // Equals: e^x
                        derivative = (OperatorFunction) clone();
                    } else {
                        // ln(C)
                        MathFunction funcLn = new MathFunction(Ln.TAG);
                        funcLn.setBaseFunction(baseFunction1.clone());

                        // C^x * ln(C)
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY, clone(), funcLn);
                    }

                    // Derivative from C^f(x)
                    // Equals: C^f(x) * (f(x)*ln(C))'
                    // Equals: C^f(x) * ln(C) * f(x)'
                } else if (baseFunction1 instanceof Number &&
                        (baseFunction2 instanceof MathFunction || baseFunction2 instanceof OperatorFunction)) {
                    if (((Number) baseFunction1).getNumber() == Math.E) {
                        // Derivative from e^f(x)
                        // Equals: e^f(x) * (f(x))'
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                clone(), baseFunction2.getDerivative());

                    } else {
                        // ln(C)
                        MathFunction funcLn = new MathFunction(Ln.TAG);
                        funcLn.setBaseFunction(baseFunction1.clone());

                        // C^f(x) * ln(C)
                        OperatorFunction funMul = new OperatorFunction(OPERATOR_MULTIPLY);
                        funMul.setBaseFunction1(clone());
                        funMul.setBaseFunction2(funcLn);

                        // C^f(x) * ln(C) * f(x)'
                        derivative = new OperatorFunction(OPERATOR_MULTIPLY,
                                funMul, baseFunction2.getDerivative());
                    }

                    // Derivative from x^x
                    // Equals: x^x * (x*ln(x))'
                    // Equals: x^x * (1+ln(x))
                } else if (baseFunction1 instanceof VariableFunction && baseFunction2 instanceof VariableFunction) {

                    // ln(x)
                    MathFunction funcLn = new MathFunction(Ln.TAG);
                    funcLn.setBaseFunction(baseFunction1.clone());

                    // 1+ln(x)
                    OperatorFunction funPl = new OperatorFunction(OPERATOR_PLUS);
                    funPl.setBaseFunction1(Number.getConstant(Number.C_1));
                    funPl.setBaseFunction2(funcLn);

                    // x^x * (1+ln(x))
                    derivative = new OperatorFunction(OPERATOR_MULTIPLY, clone(), funPl);

                    // Derivative from x^f(x)
                    // Equals: x^f(x) * (f(x)*ln(x))'

                    // Derivative from f(x)^x
                    // Equals: f(x)^x * (x*ln(f(x)))'

                    // Derivative from f(x)^g(x)
                    // Equals: f(x)^g(x) * (g(x)*ln(f(x)))'
                } else {

                    // ln(f(x))
                    MathFunction funcLn = new MathFunction(Ln.TAG);
                    funcLn.setBaseFunction(baseFunction1.clone());

                    //g(x)* ln(f(x))
                    OperatorFunction funMul = new OperatorFunction(OPERATOR_MULTIPLY);
                    funMul.setBaseFunction1(baseFunction2.clone());
                    funMul.setBaseFunction2(funcLn);

                    //f(x)^g(x) * (g(x)*ln(f(x)))'
                    derivative = new OperatorFunction(OPERATOR_MULTIPLY, clone(), funMul.getDerivative());
                }
                break;
        }
        if (derivative == null) {
            return Number.getConstant(Number.C_0);
        }
//        addNextStep(derivative.toString());

        debug("getDerivative, derivative: " + derivative.toString());
        return derivative;
    }

    public static boolean isOperation(String s) {
        return s.equals(OPERATOR_PLUS) || s.equals(OPERATOR_MINUS) || s.equals(OPERATOR_MULTIPLY)
                || s.equals(OPERATOR_DIVIDE) || s.equals(OPERATOR_POW);
    }

    public static void debug(String message) {
        if (Logger.DEBUG && DEBUG) {
            Log.d(TAG, message);
        }
    }
}
