package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;
import com.aling.function.functions.VariableFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Cosh {
    public static final String TAG = "cosh";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (cosh(x))' = sinh(x)
        // Derivative from (cosh(f(x)))' = sinh(x) * f(x)'
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            //sinh(x)
            MathFunction func = new MathFunction(Sinh.TAG, baseFunction.clone());

            //sinh(x) * f(x)'
            return new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY, func,
                    baseFunction.getDerivative());
        } else if (baseFunction instanceof VariableFunction) {
            //sinh(x)
            return new MathFunction(Sinh.TAG, baseFunction.clone());
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}

