package com.aling.function.functions.numbers;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.aling.function.functions.numbers.interfaces.MathInterface;
import com.aling.function.functions.numbers.interfaces.OperatorInterface;
import com.aling.function.utils.Logger;

/**
 * Created bm mcekic on 12/7/17.
 */

public class Euler implements Parcelable, MathInterface<Euler>, OperatorInterface<Euler> {
    public static final String TAG = "Euler";

    public static final boolean DEBUG = true;

    public static final Euler I = new Euler(1, Number.PI / 2);

    private double moduo;
    private double argument;

    public Euler(double moduo, double argument) {
        this.moduo = moduo;
        this.argument = argument;
    }

    public Euler(Euler e) {
        this.moduo = e.moduo;
        this.argument = e.argument;
    }

    /**
     * @param a double
     * @param m double
     * @return
     */
    public final boolean equals(double m, double a) {
        return this.moduo == m && this.argument == a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Euler number = (Euler) o;

        if (this.moduo != number.moduo) return false;
        if (this.argument != number.argument) return false;

        return true;
    }

    /**
     * @return Converts to Complex number.
     */
    public final Complex toComplex() {
        double x = moduo * Math.cos(argument);
        double y = moduo * Math.sin(argument);
        return new Complex(x, y);
    }

    @Override
    public Euler plus(Euler object) {
        Complex complex = toComplex();
        Complex plus = complex.plus(object.toComplex());
        return plus.toEuler();
    }

    @Override
    public Euler minus(Euler object) {
        Complex complex = toComplex();
        Complex minus = complex.minus(object.toComplex());
        return minus.toEuler();
    }

    @Override
    public Euler multiply(Euler object) {
        return new Euler(moduo * object.moduo,
                argument + object.argument);
    }

    @Override
    public Euler divide(Euler object) {
        return new Euler(moduo / object.moduo,
                argument - object.argument);
    }

    @Override
    public Euler pow(Euler object) {
        Complex complex = object.toComplex();
        return new Euler(moduo / (Math.pow(Number.E, complex.y * argument)),
                complex.x * argument);
    }

    @Override
    public Euler neg() {
        double arg = argument + Number.PI;
        return new Euler(moduo, arg);
    }

    @Override
    public Euler ln() {
        Complex complex = new Complex(Math.log(moduo), argument);
        return complex.toEuler();
    }

    @Override
    public Euler log10() {
        Complex complex = new Complex(Math.log10(moduo), argument / Math.log(10));
        return complex.toEuler();
    }

    @Override
    public Euler sin() {
        Complex complex = toComplex();
        Complex sin = complex.sin();
        return sin.toEuler();
    }

    @Override
    public Euler cos() {
        Complex complex = toComplex();
        Complex cos = complex.cos();
        return cos.toEuler();
    }

    @Override
    public Euler tan() {
        Complex complex = toComplex();
        Complex tan = complex.tan();
        return tan.toEuler();
    }

    @Override
    public Euler cot() {
        Complex complex = toComplex();
        Complex cot = complex.cot();
        return cot.toEuler();
    }

    @Override
    public Euler arcsin() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler arccos() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler arctan() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler arccot() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler sinh() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler cosh() {
        //TODO:
        return new Euler(moduo, argument);
    }

    @Override
    public Euler abs() {
        return new Euler(moduo, 0);
    }

    @Override
    public Euler sqrt() {
        return new Euler(Math.sqrt(moduo), argument / 2);
    }

    @Override
    public Euler eu() {
        Complex complex = toComplex();
        return new Euler(Math.pow(Number.E, -complex.y), complex.x);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.moduo);
        dest.writeDouble(this.argument);
    }

    protected Euler(Parcel in) {
        this.moduo = in.readDouble();
        this.argument = in.readDouble();
    }

    public static final Creator<Complex> CREATOR = new Creator<Complex>() {
        @Override
        public Complex createFromParcel(Parcel source) {
            return new Complex(source);
        }

        @Override
        public Complex[] newArray(int size) {
            return new Complex[size];
        }
    };

    public static void debug(String message) {
        if (Logger.DEBUG && DEBUG) {
            Log.d(TAG, message);
        }
    }
}
