package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

/**
 * Created by maja on 10/19/16.
 */
public class ArcCot {
    public static final String TAG = "arccot";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (arcctg(x))' = -1/(1+x^2)
        // Derivative from (arcctg(f(x)))' = -f(x)'/(1+x^2)
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // x^2
            OperatorFunction pow = new OperatorFunction(OperatorFunction.OPERATOR_POW, baseFunction.clone(), Number.getConstant(Number.C_2));
            // 1+x^2
            OperatorFunction plus = new OperatorFunction(OperatorFunction.OPERATOR_PLUS, Number.getConstant(Number.C_1), pow);
            // f(x)'/(1+x^2)
            OperatorFunction div = new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE, baseFunction.getDerivative(), plus);
            // - f(x)'/(1+x^2)
            return new MathFunction(Neg.TAG, div);
        } else if (baseFunction instanceof VariableFunction) {
            // x^2
            OperatorFunction pow = new OperatorFunction(OperatorFunction.OPERATOR_POW, VariableFunction.getFunction(), Number.getConstant(Number.C_2));
            // 1+x^2
            OperatorFunction plus = new OperatorFunction(OperatorFunction.OPERATOR_PLUS, Number.getConstant(Number.C_1), pow);
            // ((1+x^2))^-1
            OperatorFunction pow2 = new OperatorFunction(OperatorFunction.OPERATOR_POW, plus, Number.getConstant(Number.C_NEG_1));
            // -((1+x^2))^-1
            return new MathFunction(Neg.TAG, pow2);
        } else {
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
