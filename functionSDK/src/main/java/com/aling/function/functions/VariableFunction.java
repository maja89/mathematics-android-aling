package com.aling.function.functions;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.numbers.Complex;
import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.numbers.Number;

import java.util.ArrayList;

public class VariableFunction extends BaseFunction {
    public static final String TAG = "VariableFunction";

    public static final String X = "x";
    public static final String Z = "z";

    private static BaseFunction sFunction;

    private static String sVariable;

    private VariableFunction() {

    }

    /**
     * @return singleton instance of VariableFunction.
     */
    public static BaseFunction getFunction() {
        if (sFunction == null) {
            sFunction = new VariableFunction();
        }
        return sFunction;
    }

    /**
     * Sets variable to x or z
     *
     * @param variable String
     */
    public static void setVariable(String variable) {
        if (X.equals(variable) || Z.equals(variable)) {
            sVariable = variable;
        }
    }

    @Override
    public String toString() {
        return sVariable;
    }

    @Override
    public String showNode() {
        return toString();
    }

    @Override
    public FunctionArray getFunctionArray() {
        ArrayList<String> function = new ArrayList<>();
        function.add(toString());
        return new FunctionArray(function);
    }

    @Override
    public BaseFunction clone() {
        return sFunction;
    }

    @Override
    public double getValue(double x) {
        return x;
    }

    @Override
    public Euler getValue(Euler euler) {
        return euler;
    }

    @Override
    public Complex getValue(Complex complex) {
        return complex;
    }

    @Override
    public Interval getDomain() {
        return new DefInterval();
    }

    @Override
    public boolean equals(BaseFunction baseFunction) {
        // variable can only be x or z, no need for checking
        return baseFunction instanceof VariableFunction;
    }

    @Override
    public BaseFunction getDerivative() {
        return Number.getConstant(Number.C_1);
    }
}
