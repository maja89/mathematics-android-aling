package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Tan {
    public static final String TAG = "tan";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (tg(x))' = 1/(cos(x))^2 = (cos(x))^-2
        // Derivative from (tg(f(x)))' =  f(x)'/(cos(x))^2
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // (cos(x)) ^ 2
            OperatorFunction func = new OperatorFunction(OperatorFunction.OPERATOR_POW,
                    new MathFunction(Cos.TAG, baseFunction.clone()), Number.getConstant(Number.C_2));

            // f(x)'/(cos(x))^2
            return new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE,
                    baseFunction.getDerivative(), func);
        } else if (baseFunction instanceof VariableFunction) {
            // (cos(x)) ^ -2
            return new OperatorFunction(OperatorFunction.OPERATOR_POW,
                    new MathFunction(Cos.TAG, baseFunction.clone()), Number.getConstant(Number.C_NEG_2));
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
