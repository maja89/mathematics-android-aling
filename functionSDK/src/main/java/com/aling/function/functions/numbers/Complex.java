package com.aling.function.functions.numbers;

import android.os.Parcel;
import android.os.Parcelable;

import com.aling.function.functions.numbers.interfaces.MathInterface;
import com.aling.function.functions.numbers.interfaces.OperatorInterface;

/**
 * Created by mcekic on 12/7/17.
 */

public class Complex implements Parcelable, MathInterface<Complex>, OperatorInterface<Complex> {
    public static final String TAG = "Complex";

    public static final boolean DEBUG = true;

    public double x;
    public double y;

    public Complex(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Complex(Complex p) {
        this.x = p.x;
        this.y = p.y;
    }

    /**
     * @param x double
     * @param y double
     * @return true if the number's coordinates equal (x,y)
     */
    public final boolean equals(double x, double y) {
        return this.x == x && this.y == y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Complex number = (Complex) o;

        if (Double.compare(number.x, x) != 0) return false;
        if (Double.compare(number.y, y) != 0) return false;

        return true;
    }

    @Override
    public String toString() {
        return "(" + x + "+ i " + y + ")";
    }

    /**
     * @return Converts to Complex number.
     */
    public final Euler toEuler() {
        double m = r();

        if (x != 0 && y != 0) {
            double a = Math.atan(y / x);
            return new Euler(m, a);
        } else if (x == 0 && y != 0) {
            double a;
            if (y >= 0) {
                a = Number.PI / 2;
            } else {
                a = -Number.PI / 2;
            }
            return new Euler(m, a);
        } else {
            return new Euler(0, 0);
        }
    }

    /**
     * @return distance from (0,0) to the number
     */
    public final double r() {
        return length(x, y);
    }

    /**
     * @param x double
     * @param y double
     * @return distance from (0,0) to (x,y)
     */
    private static double length(double x, double y) {
        return Math.hypot(x, y);
    }

    @Override
    public Complex plus(Complex object) {
        return new Complex(x + object.x, y + object.y);
    }

    @Override
    public Complex minus(Complex object) {
        return new Complex(x - object.x, y - object.y);
    }

    @Override
    public Complex multiply(Complex object) {
        return new Complex(x * object.x - y * object.y, x * object.y + y * object.x);
    }

    @Override
    public Complex divide(Complex object) {
        double r = Math.pow(object.x, 2) + Math.pow(object.y, 2);
        return new Complex((x * object.x + y * object.y) / r, (y * object.x - x * object.y) / r);
    }

    @Override
    public Complex pow(Complex object) {
        Euler euler1 = toEuler();
        Euler pow = euler1.pow(object.toEuler());
        return pow.toComplex();
    }

    @Override
    public Complex neg() {
        return new Complex(-x, -y);
    }

    @Override
    public Complex ln() {
        Euler euler = toEuler();
        Euler ln = euler.ln();
        return ln.toComplex();
    }

    @Override
    public Complex log10() {
        Euler euler = toEuler();
        Euler log10 = euler.log10();
        return log10.toComplex();
    }

    @Override
    public Complex sin() {
        return new Complex(Math.sin(x) * Math.cosh(y), -Math.sinh(y) * Math.cos(x));
    }

    @Override
    public Complex cos() {
        return new Complex(Math.cos(x) * Math.cosh(y), -Math.sinh(y) * Math.sin(x));
    }

    @Override
    public Complex tan() {
        double r = Math.pow(Math.cos(x), 2) * Math.pow(Math.cosh(y), 2) + Math.pow(Math.sin(x), 2) * Math.pow(Math.sinh(y), 2);
        return new Complex(Math.cos(x) * Math.sin(x) / r, -Math.cosh(y) * Math.sinh(y) / r);
    }

    @Override
    public Complex cot() {
        double r = Math.pow(Math.sin(x), 2) * Math.pow(Math.cosh(y), 2) + Math.pow(Math.cos(x), 2) * Math.pow(Math.sinh(y), 2);
        return new Complex(Math.cos(x) * Math.sin(x) / r, Math.cosh(y) * Math.sinh(y) / r);
    }

    @Override
    public Complex arcsin() {
        Euler euler = toEuler();
        Euler arcsin = euler.arcsin();
        return arcsin.toComplex();
    }

    @Override
    public Complex arccos() {
        Euler euler = toEuler();
        Euler arccos = euler.arccos();
        return arccos.toComplex();
    }

    @Override
    public Complex arctan() {
        Euler euler = toEuler();
        Euler arctan = euler.arctan();
        return arctan.toComplex();
    }

    @Override
    public Complex arccot() {
        Euler euler = toEuler();
        Euler arccot = euler.arccot();
        return arccot.toComplex();
    }

    @Override
    public Complex sinh() {
        Euler euler = toEuler();
        Euler sinh = euler.sinh();
        return sinh.toComplex();
    }

    @Override
    public Complex cosh() {
        Euler euler = toEuler();
        Euler cosh = euler.cosh();
        return cosh.toComplex();
    }

    @Override
    public Complex abs() {
        Euler euler = toEuler();
        Euler abs = euler.abs();
        return abs.toComplex();
    }

    @Override
    public Complex sqrt() {
        Euler euler = toEuler();
        Euler sqrt = euler.sqrt();
        return sqrt.toComplex();
    }

    @Override
    public Complex eu() {
        return new Euler(Math.pow(Number.E, -y), x).toComplex();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.x);
        dest.writeDouble(this.y);
    }

    protected Complex(Parcel in) {
        this.x = in.readDouble();
        this.y = in.readDouble();
    }

    public static final Creator<Complex> CREATOR = new Creator<Complex>() {
        @Override
        public Complex createFromParcel(Parcel source) {
            return new Complex(source);
        }

        @Override
        public Complex[] newArray(int size) {
            return new Complex[size];
        }
    };
}
