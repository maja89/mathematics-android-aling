package com.aling.function.functions.numbers.interfaces;

/**
 * Created by mcekic on 12/8/17.
 */

public interface OperatorInterface<T> {
    T plus(T object);

    T minus(T object);

    T multiply(T object);

    T divide(T object);

    T pow(T object);
}

