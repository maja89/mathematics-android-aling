package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Sinh {
    public static final String TAG = "sinh";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (sinh(x))' = cosh(x)
        // Derivative from (sinh(f(x)))' = cosh(x) * f(x)'
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            //cosh(x)
            MathFunction func = new MathFunction(Cosh.TAG, baseFunction.clone());

            //cosh(x) * f(x)'
            return new OperatorFunction(OperatorFunction.OPERATOR_MULTIPLY, func,
                    baseFunction.getDerivative());
        } else if (baseFunction instanceof VariableFunction) {
            //cosh(x)
            return new MathFunction(Cosh.TAG, baseFunction.clone());
        } else {
            // 0
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
