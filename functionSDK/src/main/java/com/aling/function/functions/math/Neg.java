package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;

/**
 * Created by maja on 10/19/16.
 */
public class Neg {
    public static final String TAG = "neg";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from -f(x)
        // Equals: -f(x)'
        if (!(baseFunction instanceof Number)) {
            return new MathFunction(Neg.TAG, baseFunction.getDerivative());
        } else {
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  (-infinity,+infinity)
        return new DefInterval();
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}
