package com.aling.function.functions;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.Idiom;
import com.aling.function.beans.Point;
import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.numbers.Complex;
import com.aling.function.functions.numbers.Euler;
import com.aling.function.functions.math.Neg;
import com.aling.function.functions.numbers.Number;

import java.util.ArrayList;

public abstract class BaseFunction {
    public static final String TAG = "BaseFunction";

    private static ArrayList<String> sSolvingSteps = new ArrayList<String>();

    public static ArrayList<String> getSolvingSteps() {
        return sSolvingSteps;
    }

    protected static void addNextStep(String s) {
        sSolvingSteps.add(s);
    }

    public static void clearAllSteps() {
        sSolvingSteps.clear();
    }

    public BaseFunction() {
    }

    public abstract BaseFunction clone();

    public abstract boolean equals(BaseFunction baseFunction);


    public abstract String toString();

    public abstract String showNode();

    public abstract FunctionArray getFunctionArray();

    /**
     * y = f(x)
     *
     * @return y
     */
    public abstract double getValue(double x);

    /**
     * @return f(z)
     */
    public abstract Euler getValue(Euler euler);

    /**
     * @return f(z)
     */
    public abstract Complex getValue(Complex complex);

    /**
     * @return Interval (a, b) Interval [a, b] Interval (-infinity, +infinity)
     */
    public abstract Interval getDomain();

    /**
     * f(x) = 0
     *
     * @return x0, x1, x2, x3...
     */
    public ArrayList<Point> getZeroesOfFunction() {
        return new ArrayList<>();
    }

    /**
     * f(x) > 0
     *
     * @return Interval (a, b)
     */
    public Interval getPositive() {
        return new DefInterval();
    }

    /**
     * f(x) < 0
     *
     * @return Interval (a, b)
     */
    public Interval getNegative() {
        return new DefInterval();
    }

    /**
     * lim (x-> a+-0) (f(x))=+-infinity
     *
     * @return x = a
     */
    public ArrayList<Double> getAsymptoteVertical() {
        return new ArrayList<>();
    }

    /**
     * k = lim(x->+-infinity)( (f(x)) / x) n = lim(x->+-infinity)( f(x) - k*x)
     *
     * @return y = k*x + n
     */
    public ArrayList<Double> getAsymptoteOblique() {
        return new ArrayList<>();
    }

    /**
     * k = lim(x->+-infinity)( (f(x)) / x) = 0 n = lim(x->+-infinity)( f(x) -
     * k*x)
     *
     * @return y = n
     */
    public ArrayList<Double> getAsymptoteHorizontal() {
        return new ArrayList<>();
    }

    /**
     * lim(x-> -infinity)(f(x))
     *
     * @return Idiom: -a, +a, -infinity, +infinity
     */
    public Idiom getBehaviorMinusInfinity() {
        return new Idiom();
    }

    /**
     * lim(x-> +infinity)(f(x))
     *
     * @return Idiom: -a, +a, -infinity, +infinity
     */
    public Idiom getBehaviorPlusInfinity() {
        return new Idiom();
    }

    /**
     * lim(x-> x0-0)(f(x))
     *
     * @return Idiom: -a, +a, -infinity, +infinity
     */
    public Idiom getBehaviorBreakPointPlusZero(Double point) {
        return new Idiom();
    }

    /**
     * lim(x-> x0+0)(f(x))
     *
     * @return Idiom: -a, +a, -infinity, +infinity
     */
    public Idiom getBehaviorBreakPointMinusZero(Double point) {
        return new Idiom();
    }

    /**
     * (f(x))'
     *
     * @return f'(x)
     */
    public abstract BaseFunction getDerivative();

    /**
     * f(x) = -f(-x)
     *
     * @return -f(-x)
     */
    public BaseFunction getOdd() {
        return new MathFunction(Neg.TAG, getEven());
    }

    /**
     * f(x) = f(-x)
     *
     * @return f(-x)
     */
    public BaseFunction getEven() {
        if (this instanceof Number) {
            return new MathFunction(Neg.TAG, this.clone());
        } else if (this instanceof VariableFunction) {
            return new MathFunction(Neg.TAG, VariableFunction.getFunction());
        } else if (this instanceof MathFunction) {
            MathFunction func = (MathFunction) this.clone();
            func.setBaseFunction(func.getBaseFunction().getEven());
            return func;
        } else if (this instanceof OperatorFunction) {
            OperatorFunction func = (OperatorFunction) this.clone();
            func.setBaseFunction1(func.getBaseFunction1().getEven());
            func.setBaseFunction2(func.getBaseFunction2().getEven());
            return func;
        }
        return null;
    }

    /**
     * f(x) = -f(-x)
     *
     * @return true, false
     */
    public boolean isOdd() {
        return equals(getOdd());
    }

    /**
     * f(x) = f(-x)
     *
     * @return true, false
     */
    public boolean isEven() {
        return equals(getEven());
    }

    public BaseFunction replaceXWithValue(double value) {
        if (this instanceof Number) {
            return new Number(value);
        } else if (this instanceof VariableFunction) {
            return new Number(value);
        } else if (this instanceof MathFunction) {
            MathFunction func = (MathFunction) this.clone();
            func.setBaseFunction(func.getBaseFunction().replaceXWithValue(value));
            return func;
        } else if (this instanceof OperatorFunction) {
            OperatorFunction func = (OperatorFunction) this.clone();
            func.setBaseFunction1(func.getBaseFunction1().replaceXWithValue(value));
            func.setBaseFunction2(func.getBaseFunction1().replaceXWithValue(value));
            return func;
        }
        return null;
    }
}
