package com.aling.function.functions.math;

import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.VariableFunction;
import com.aling.function.functions.BaseFunction;
import com.aling.function.functions.numbers.Number;
import com.aling.function.functions.MathFunction;
import com.aling.function.functions.OperatorFunction;

import com.aling.function.beans.interval.DefInterval.IntervalType;

/**
 * Created by maja on 10/19/16.
 */
public class ArcSin {
    public static final String TAG = "arcsin";

    public static BaseFunction getDerivative(BaseFunction baseFunction) {
        // Derivative from (arcsin(x))' = 1/sqrt(1-x^2)
        // Derivative from (arcsin(f(x)))' = f(x)'/sqrt(1-x^2)
        if (baseFunction instanceof OperatorFunction || baseFunction instanceof MathFunction) {
            // x^2
            OperatorFunction pow = new OperatorFunction(OperatorFunction.OPERATOR_POW, baseFunction.clone(), Number.getConstant(Number.C_2));
            // 1-x^2
            OperatorFunction min = new OperatorFunction(OperatorFunction.OPERATOR_MINUS, Number.getConstant(Number.C_1), pow);
            // sqrt(1-x^2)
            MathFunction sqrt = new MathFunction(Sqrt.TAG, min);
            // f(x)'/sqrt(1-x^2)
            return new OperatorFunction(OperatorFunction.OPERATOR_DIVIDE, baseFunction.getDerivative(), sqrt);
        } else if (baseFunction instanceof VariableFunction) {
            // x^2
            OperatorFunction pow = new OperatorFunction(OperatorFunction.OPERATOR_POW, VariableFunction.getFunction(), Number.getConstant(Number.C_2));
            // 1-x^2
            OperatorFunction min = new OperatorFunction(OperatorFunction.OPERATOR_MINUS, Number.getConstant(Number.C_1), pow);
            // sqrt(1-x^2)
            MathFunction sqrt = new MathFunction(Sqrt.TAG, min);
            // (sqrt(1-x^2))^
            return new OperatorFunction(OperatorFunction.OPERATOR_POW, sqrt, Number.getConstant(Number.C_NEG_1));
        } else {
            return Number.getConstant(Number.C_0);
        }
    }

    public static DefInterval getDomain() {
        //  [-1,+1]
        return new DefInterval(IntervalType.CLOSED, -1, 1, IntervalType.CLOSED);
    }

    public static Interval getDomain(BaseFunction baseFunction) {
        if (baseFunction instanceof VariableFunction) {
            return getDomain();
        } else if (baseFunction instanceof MathFunction) {
        } else if (baseFunction instanceof Number) {

        }
        return new DefInterval();
    }
}

