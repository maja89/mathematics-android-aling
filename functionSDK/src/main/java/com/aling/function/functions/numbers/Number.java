package com.aling.function.functions.numbers;

import android.os.Parcel;
import android.os.Parcelable;

import com.aling.function.beans.FunctionArray;
import com.aling.function.beans.interval.DefInterval;
import com.aling.function.beans.interval.Interval;
import com.aling.function.functions.BaseFunction;

import java.util.ArrayList;
import java.util.HashMap;

public class Number extends BaseFunction implements Parcelable {
    public static final String TAG = "Number";

    public static final double E = 2.718281828459045D;
    public static final double PI = 3.141592653589793D;

    public static final String STR_E = "e";
    public static final String STR_PI = "\u03C0";
    public static final String STR_INFINITY = "\u221e";
    public static final String STR_PLUS_MINUS = "\u00b1";
    public static final String STR_PLUS_MINUS_INFINITY = STR_PLUS_MINUS + STR_INFINITY;
    public static final String STR_PLUS_INFINITY = "+" + STR_INFINITY;
    public static final String STR_MINUS_INFINITY = "-" + STR_INFINITY;

    public static final int C_NEG_2 = -2;
    public static final int C_NEG_1 = -1;
    public static final int C_0 = 0;
    public static final int C_1 = 1;
    public static final int C_2 = 2;
    public static final int C_10 = 10;

    public static final int C_E = 27;
    public static final int C_PI = 31;

    public static final double PLUS_INFINITY = Double.MAX_VALUE;
    public static final double MINUS_INFINITY = Double.MIN_VALUE;

    private static HashMap<Integer, Number> sHasMapConstants = new HashMap<>();

    static {
        sHasMapConstants.put(C_NEG_2, new Number(C_NEG_2));
        sHasMapConstants.put(C_NEG_1, new Number(C_NEG_1));
        sHasMapConstants.put(C_0, new Number(C_0));
        sHasMapConstants.put(C_1, new Number(C_1));
        sHasMapConstants.put(C_2, new Number(C_2));
        /*sHasMapConstants.put(3, new Number(3));
        sHasMapConstants.put(4, new Number(4));
        sHasMapConstants.put(5, new Number(5));
        sHasMapConstants.put(6, new Number(6));
        sHasMapConstants.put(7, new Number(7));
        sHasMapConstants.put(8, new Number(8));
        sHasMapConstants.put(9, new Number(9));*/
        sHasMapConstants.put(C_10, new Number(C_10));
        sHasMapConstants.put(C_E, new Number(E));
        sHasMapConstants.put(C_PI, new Number(PI));
    }

    public static Number getConstant(int key) {
        return sHasMapConstants.get(key);
    }

    private double number;

    public Number(double number) {
        this.number = number;
    }

    public double getNumber() {
        return number;
    }

    public void addDividend(double dividend) {
        number *= dividend;
    }

    public void addDivisor(double divisor) {
        number /= divisor;
    }

    /**
     * @return
     */
    public boolean isKnownConstant() {
        return E == this.number || PI == this.number;
    }

    /**
     * @param number double
     * @return
     */
    public static boolean isKnownConstant(double number) {
        return E == number || PI == number;
    }

    @Override
    public BaseFunction clone() {
        return new Number(this.number);
    }

    @Override
    public boolean equals(BaseFunction baseFunction) {
        if (baseFunction instanceof Number) {
            Number function = (Number) baseFunction;
            return function.number == this.number;
        }
        return false;
    }

    @Override
    public String toString() {
        if (number == E) {
            return STR_E;
        } else if (number == PI) {
            return STR_PI;
        } else if (number == PLUS_INFINITY) {
            return STR_PLUS_INFINITY;
        } else if (number == MINUS_INFINITY) {
            return STR_MINUS_INFINITY;
        } else if (number % 1 == 0) {
            return Integer.toString((int) number);
        } else {
            return Double.toString(number);
        }
    }

    @Override
    public String showNode() {
        return toString();
    }

    @Override
    public FunctionArray getFunctionArray() {
        ArrayList<String> function = new ArrayList<>();
        function.add(toString());
        return new FunctionArray(function);
    }

    @Override
    public double getValue(double point) {
        return number;
    }

    @Override
    public Euler getValue(Euler euler) {
        return new Euler(number, 0);
    }

    @Override
    public Complex getValue(Complex complex) {
        return new Complex(number, 0);
    }

    @Override
    public Interval getDomain() {
        return new DefInterval();
    }

    @Override
    public BaseFunction getDerivative() {
        return Number.getConstant(Number.C_0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.number);
    }

    protected Number(Parcel in) {
        this.number = in.readDouble();
    }

    public static final Creator<Number> CREATOR = new Creator<Number>() {
        @Override
        public Number createFromParcel(Parcel source) {
            return new Number(source);
        }

        @Override
        public Number[] newArray(int size) {
            return new Number[size];
        }
    };
}
