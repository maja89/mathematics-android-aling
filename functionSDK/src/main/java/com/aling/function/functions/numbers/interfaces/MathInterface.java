package com.aling.function.functions.numbers.interfaces;

/**
 * Created by mcekic on 12/8/17.
 */

public interface MathInterface<T> {
    T neg();

    T ln();

    T log10();

    T sin();

    T cos();

    T tan();

    T cot();

    T arcsin();

    T arccos();

    T arctan();

    T arccot();

    T sinh();

    T cosh();

    T abs();

    T sqrt();

    T eu();
}
